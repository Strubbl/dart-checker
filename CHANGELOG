Version: 0.6.5

fixed:
- unable to install on Android 12 and above


Version: 0.6.5

added:
- main menu: button for random game selection

Version: 0.6.4

added:
- elimination: undo unlimited
- elimination: visualisation of score for the next elimination

fixed:
- elimination: 2 Player: not active player was eliminated in the overview, no matter what the active player scored
- elimination: eliminations were not undone when clicking undo

Version: 0.6.3

added:
- new game typ: ELIMINATION (look up the help section for the game rules)
- help: added pending entries

fixed:
- main menu: reduced game variants to 301 and 501 only (want to play a different score? use the FREE game typ)

Version: 0.6.2

fixed:
- X01 / cricket: exchanged alignment position of "continue" and "missed" button

Version: 0.6.1

added:
- X01 / cricket: "continue" button - switches to next player and counts nothing for the pending darts

Version: 0.6

added:
- preferences: start of crazy cricket range

fixed:
- cricket: app freeze at end of match, when option "the game continues after the first player has won" is enabled
- cricket: display of wrong information when denying question on last throw

Version: 0.5.9

added:
- preferences: enable/disable undo safety check question

fixed:
- SET/LEG: layout of notification message after end of leg
- X01: layout of suggestions
- X01: layout of space for player name enhanced

Version: 0.5.8

- cricket: tiny but essential code cleanup

added:
- cricket: question after last input

fixed:
- app crash on startup and settings
- Preferences: language setting should work now in all Android versions
- tiny layout problems

Version: 0.5.7

fixed:
- main menu: missing statistic button and score input for FREE mode
- cricket: response speed

Version: 0.5.6

added:
- Preferences: language selection
- X01/Cricket: undo safety question
- X01: improved layout for big screens and landscape mode - variable textsize of score

fixed:
- SET/LEG: overview at end of game: wrong text overlays were visible
- main menu: layout of player names especially for small screens and lots of players
- help: light theme wasn't applied

Version: 0.5.5
added:
- support for up to 8 player (except set/leg mode)
- Help: general dart rules

fixed:
- Main: selected player names changed, when changing player count
- Help: tiny language mixture


Version: 0.5.4

added:
- X01: undo message in single dart input mode
- cricket: enhanced undo messages
- preferences: optionally cricket can be played over the "whole distance" (game continues after first player won)

fixed:
- X01 + one dart input: when input of score, was exact the same as the score left and no double or masterout was hit, but needed, the game didn't goes on
- cricket: player ranking at end of game
- crazy cricket: when undo button was clicked and player changed, the segments were selected randomly


Version: 0.5.3

added:
- spanish translation (thanks to Diego)
- cricket game variant: crazy
- help

fixed:
- landscape screen orientation in "preferences" and "about"
- input of player names

Version: 0.5.2

fixed:
- cricket cut throat mode: score was added, even if an opponent has closed the field (thanks to Heinrich)

Version: 0.5.1

fixed:
- bug in Sudden Death mode

Version: 0.5

added:
- cricket mode: cut throat
- master out
- preferences: alternative score input mode for all non cricket matches
- preferences: Sudden Death mode for all non cricket matches

fixed:
- X01: undo bug if only one player and no hit
- Cricket: screen standby

Version: 0.4.4

added:
- support of landscape screen orientation in match mode

fixed:
- sometimes wrong display of playing time
- unwanted screen standby in cricket mode
- layout of names in cricket mode

Version: 0.4.3

added:
- display elapsed time at end of match

fixed:
- bug in match mode - "undo" changed order of player, if no dart was entered yet
- missing english translation in cricket mode

Version: 0.4.2

fixed:
- bug in cricket: player had to score to win


Version: 0.4.1

- layout improvements in cricket mode

added:
- new preference item: FIRST TO or BEST OF
- set/leg status popup after leg finish

fixed:
- bug in display of "highest checkout" after SET/LEG match
- bug in cricket: (re)activation of buttons, while numbers being closed


Version: 0.4

- tiny improvements in layout
- bugfix in overview after SET/LEG match
- added new game typ: cricket


Version: 0.3

- tiny improvements in layout
- bugfix in playerlist (problem if adding new player after canceling the change of a playername)
- added delete buttons in playerlist 