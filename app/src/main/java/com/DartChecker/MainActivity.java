package com.DartChecker;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private static final String fileName = "spieler.dat";
    public static ArrayList<spieler> allespieler;
    public String startsprache;
    public static boolean themeauswahl = true,
            setlegmodus = true;
    private ArrayAdapter<spieler> arrayAdapter;




    public static void speichern(Context ctx, Boolean speichern_aussetzen) {
        if (speichern_aussetzen) return;
        try {
            FileOutputStream fout = ctx.openFileOutput(fileName, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(allespieler);
            oos.flush();
            oos.close();  //fileoutputstream wird automatisch mitgeschlossen
        } catch (FileNotFoundException e) {
            Toast.makeText(ctx, e + "Error saving: file not found", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        } catch (IOException i) {
            Toast.makeText(ctx, i + "Error saving: Input/Output", Toast.LENGTH_LONG).show();
            i.printStackTrace();
        }
    }

    private void laden() {
        try {
            FileInputStream fis = openFileInput(fileName);
            ObjectInputStream is = new ObjectInputStream(fis);
            @SuppressWarnings("unchecked") ArrayList<spieler> returnlist = (ArrayList<spieler>) is.readObject();
            is.close();
            fis.close();
            allespieler = returnlist;

        } catch (FileNotFoundException e) {
            Toast.makeText(this, e + "Error loading: file not found", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        } catch (ClassNotFoundException b) {
            Toast.makeText(this, b + "Error loading: class not found", Toast.LENGTH_LONG).show();
            b.printStackTrace();
        } catch (IOException i) {
            Toast.makeText(this, i + "Error loading: Input/Output", Toast.LENGTH_LONG).show();
            i.printStackTrace();
        }

    }

    private int maxlegs = 5;
    private int maxsets = 13;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(updateBaseContextLocale(base));
    }

    private Context updateBaseContextLocale(Context context) {
        final SharedPreferences settings = context.getSharedPreferences("Einstellungen", 0);
        String language = Locale.getDefault().getLanguage();
        if (settings.contains("Sprache")) {
            language = settings.getString("Sprache", "en");
        }
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return updateResourcesLocale(context, locale);
        }

        return updateResourcesLocaleLegacy(context, locale);
    }

    @TargetApi(Build.VERSION_CODES.N_MR1)
    private Context updateResourcesLocale(Context context, Locale locale) {
        Configuration configuration = new Configuration(context.getResources().getConfiguration());
        configuration.setLocale(locale);
        return context.createConfigurationContext(configuration);
    }

    @SuppressWarnings("deprecation")
    private Context updateResourcesLocaleLegacy(Context context, Locale locale) {
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        return context;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
        if (settings.contains("Sprache")) {
            startsprache = settings.getString("Sprache", "en");
        } else {
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("SystemSprache", Locale.getDefault().getLanguage());
            editor.apply();
        }
        if (settings.contains("Theme")) themeauswahl = settings.getBoolean("Theme", true);
        if (themeauswahl) setTheme(R.style.AppTheme);
        else setTheme(R.style.Theme_AppCompat_Light_NoActionBar);
        setContentView(R.layout.activity_main);


        final CharSequence[] spielmode = {"301", "501"};
        final CharSequence[] spieltyptext = {"X01", "FREE", "SET/LEG", "CRICKET","ELIMINATION"};
        final CharSequence[] spielmode2 = {"15-Bull", "14-Bull", "13-Bull", "12-Bull", "11-Bull", "10-Bull", "Crazy"};
        final CharSequence[] cricketmodes = {"Classic", "Cut Throat"};

        if (settings.contains("setlegmodus"))
            setlegmodus = settings.getBoolean("setlegmodus", true);
        if (setlegmodus) {
            maxlegs = 3;
            maxsets = 7;
        }

        allespieler = new ArrayList<>();
        // wenn Speicherdatei vorhanden, laden! ansonsten 8 Spieler erstellen und speichern
        File file = getBaseContext().getFileStreamPath(fileName);
        if (file.exists()) {
            laden();

        } else {
            spieler gast1 = new spieler();
            spieler gast2 = new spieler();
            spieler gast3 = new spieler();
            spieler gast4 = new spieler();
            spieler gast5 = new spieler();
            spieler gast6 = new spieler();
            spieler gast7 = new spieler();
            spieler gast8 = new spieler();
            gast1.spielerName = getResources().getString(R.string.gast1);
            gast2.spielerName = getResources().getString(R.string.gast2);
            gast3.spielerName = getResources().getString(R.string.gast3);
            gast4.spielerName = getResources().getString(R.string.gast4);
            gast5.spielerName = getResources().getString(R.string.gast5);
            gast6.spielerName = getResources().getString(R.string.gast6);
            gast7.spielerName = getResources().getString(R.string.gast7);
            gast8.spielerName = getResources().getString(R.string.gast8);
            allespieler.add(gast1);
            allespieler.add(gast2);
            allespieler.add(gast3);
            allespieler.add(gast4);
            allespieler.add(gast5);
            allespieler.add(gast6);
            allespieler.add(gast7);
            allespieler.add(gast8);
            speichern(this,true);
        }
        // spiellistenAuswahllisten "bestücken"
        arrayAdapter = new ArrayAdapter<>(
                MainActivity.this,
                R.layout.spinner_item_head, allespieler);
        arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        final Spinner spin1 = findViewById(R.id.spinner);
        final Spinner spin2 = findViewById(R.id.spinner2);
        final Spinner spin3 = findViewById(R.id.spinner3);
        final Spinner spin4 = findViewById(R.id.spinner4);
        final Spinner spin5 = findViewById(R.id.spinner5);
        final Spinner spin6 = findViewById(R.id.spinner6);
        final Spinner spin7 = findViewById(R.id.spinner7);
        final Spinner spin8 = findViewById(R.id.spinner8);
        spin1.setAdapter(arrayAdapter);
        spin2.setAdapter(arrayAdapter);
        spin3.setAdapter(arrayAdapter);
        spin4.setAdapter(arrayAdapter);
        spin5.setAdapter(arrayAdapter);
        spin6.setAdapter(arrayAdapter);
        spin7.setAdapter(arrayAdapter);
        spin8.setAdapter(arrayAdapter);
        final Button sliste = findViewById(R.id.spielerlisteb);
        final Button spielmodeknopf = findViewById(R.id.spielmodus);
        final int x01variants = spielmode.length;
        final int cricketvariants = spielmode2.length;
        final Button spieltypeknopf = findViewById(R.id.spieltyp);
        final Switch doubleout = findViewById(R.id.doubleout);
        final Switch masterout = findViewById(R.id.masterout);
        final EditText eingabe = findViewById(R.id.eingabe);
        final Button spieleranzahl = findViewById(R.id.spieleranzahl);
        final Button startbutton = findViewById(R.id.startbutton);
        final ImageButton statistikb = findViewById(R.id.statistikbutton);
        final ImageButton einstellungsbutton = findViewById(R.id.einstellungsbutton);
        final TextView settext = findViewById(R.id.settext);
        final TextView legtext = findViewById(R.id.legtext);
        final Button sets = findViewById(R.id.sets);
        final Button legs = findViewById(R.id.legs);
        final TextView spiele2text = findViewById(R.id.spieler2text);
        final TextView spiele3text = findViewById(R.id.spieler3text);
        final TextView spiele4text = findViewById(R.id.spieler4text);
        final TextView spiele5text = findViewById(R.id.spieler5text);
        final TextView spiele6text = findViewById(R.id.spieler6text);
        final TextView spiele7text = findViewById(R.id.spieler7text);
        final TextView spiele8text = findViewById(R.id.spieler8text);
        final Button cricketmodeknopf = findViewById(R.id.cricketmodeknopf);
        final TextView cricketmodetext = findViewById(R.id.cricketmodetext);


        //setanzahl wechseln bei touch
        sets.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int aktuell = Integer.parseInt(sets.getText().toString());
                if (aktuell == maxsets)
                    aktuell = 1;
                else {
                    aktuell++;
                    if (!setlegmodus && aktuell % 2 == 0) aktuell++;
                    if (aktuell > maxsets) aktuell = 1;
                }
                sets.setText(String.valueOf(aktuell));
            }
        });


        //leganzahl wechseln bei touch
        legs.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int aktuell = Integer.parseInt(legs.getText().toString());
                if (aktuell == maxlegs)
                    aktuell = 1;
                else {
                    aktuell++;
                    if (!setlegmodus && aktuell % 2 == 0) aktuell++;
                    if (aktuell > maxlegs) aktuell = 1;
                }
                legs.setText(String.valueOf(aktuell));
            }
        });




        // Spielmodus wechseln bei touch
        spielmodeknopf.setOnClickListener(new OnClickListener() {
                                              public void onClick(View v) {
                                                  CharSequence aktuell = spielmodeknopf.getText();
                                                  if (!(spieltypeknopf.getText().equals("FREE"))) {
                                                      if (aktuell.equals(spielmode[x01variants - 1]))
                                                          spielmodeknopf.setText(spielmode[0]);
                                                      else
                                                          for (int i = 0; i < x01variants - 1; i++) {
                                                              if (aktuell.equals(spielmode[i])) {
                                                                  spielmodeknopf.setText(spielmode[i + 1]);
                                                                  break;
                                                              }
                                                          }

                                                      if (aktuell.equals(spielmode2[cricketvariants - 1]))
                                                          spielmodeknopf.setText(spielmode2[0]);
                                                      else
                                                          for (int i = 0; i < cricketvariants - 1; i++) {
                                                              if (aktuell.equals(spielmode2[i])) {
                                                                  spielmodeknopf.setText(spielmode2[i + 1]);
                                                                  break;
                                                              }
                                                          }


                                                  } else if (spieltypeknopf.getText().equals("FREE")) {
                                                      if (eingabe.getVisibility() == View.INVISIBLE) {
                                                          eingabe.setText(spielmodeknopf.getText());
                                                          spieleranzahl.setEnabled(false);
                                                          spielmodeknopf.setEnabled(false);
                                                          spieltypeknopf.setEnabled(false);
                                                          spin1.setEnabled(false);
                                                          spin2.setEnabled(false);
                                                          spin3.setEnabled(false);
                                                          spin4.setEnabled(false);
                                                          spin5.setEnabled(false);
                                                          spin6.setEnabled(false);
                                                          spin7.setEnabled(false);
                                                          spin8.setEnabled(false);
                                                          startbutton.setEnabled(false);
                                                          statistikb.setEnabled(false);
                                                          doubleout.setVisibility(View.INVISIBLE);
                                                          masterout.setVisibility(View.INVISIBLE);
                                                          einstellungsbutton.setEnabled(false);
                                                          eingabe.setVisibility(View.VISIBLE);
                                                          eingabe.selectAll();
                                                          // tastatur einblenden
                                                          if (eingabe.requestFocus()) {
                                                              InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                                              assert imm != null;
                                                              imm.showSoftInput(eingabe, InputMethodManager.SHOW_IMPLICIT);

                                                          }
                                                      }
                                                  }
                                              }
                                          }
        );


        // Spieltyp wechseln bei touch
        final int max2 = spieltyptext.length;

        spieltypeknopf.setOnClickListener(new OnClickListener() {
                                              public void onClick(View v) {
                                                  CharSequence aktuell = spieltypeknopf.getText();
                                                  if (aktuell.equals(spieltyptext[max2 - 1])) spieltypeknopf.setText(spieltyptext[0]);
                                                  else
                                                      for (int i = 0; i < max2 - 1; i++) {
                                                          if (aktuell.equals(spieltyptext[i])) {
                                                              spieltypeknopf.setText(spieltyptext[i + 1]);
                                                              break;
                                                          }
                                                      }
                                                  if (spieltypeknopf.getText().toString().equals("FREE"))
                                                      spielmodeknopf.setText("170");
                                                  else spielmodeknopf.setText("301");
                                                  if (spieltypeknopf.getText().toString().equals("SET/LEG")) {
                                                      spieleranzahl.setText("2");
                                                      spieleranzahl.setEnabled(false);
                                                      settext.setVisibility(View.VISIBLE);
                                                      sets.setVisibility(View.VISIBLE);
                                                      legtext.setVisibility(View.VISIBLE);
                                                      legs.setVisibility(View.VISIBLE);
                                                      spin2.setVisibility(View.VISIBLE);
                                                      spiele2text.setVisibility(View.VISIBLE);
                                                      spiele3text.setVisibility(View.GONE);
                                                      spiele4text.setVisibility(View.GONE);
                                                      spiele5text.setVisibility(View.GONE);
                                                      spiele6text.setVisibility(View.GONE);
                                                      spiele7text.setVisibility(View.GONE);
                                                      spiele8text.setVisibility(View.GONE);
                                                      spin3.setVisibility(View.GONE);
                                                      spin4.setVisibility(View.GONE);
                                                      spin5.setVisibility(View.GONE);
                                                      spin6.setVisibility(View.GONE);
                                                      spin7.setVisibility(View.GONE);
                                                      spin8.setVisibility(View.GONE);
                                                  } else {
                                                      spieleranzahl.setEnabled(true);
                                                      settext.setVisibility(View.GONE);
                                                      sets.setVisibility(View.GONE);
                                                      legtext.setVisibility(View.GONE);
                                                      legs.setVisibility(View.GONE);


                                                  }
                                                  if (spieltypeknopf.getText().toString().equals("CRICKET")) {
                                                      doubleout.setEnabled(false);
                                                      masterout.setEnabled(false);
                                                      spielmodeknopf.setText(spielmode2[0]);
                                                      cricketmodeknopf.setVisibility(View.VISIBLE);
                                                      cricketmodetext.setVisibility(View.VISIBLE);
                                                  } else {
                                                      doubleout.setEnabled(true);
                                                      masterout.setEnabled(true);
                                                      cricketmodeknopf.setVisibility(View.GONE);
                                                      cricketmodetext.setVisibility(View.GONE);
                                                  }
                                              }
                                          }
        );

        // cricketvarianten wechseln bei touch
        final int cricketmodevariants = cricketmodes.length;

        cricketmodeknopf.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence aktuell = cricketmodeknopf.getText();
                if (aktuell.equals(cricketmodes[cricketmodevariants - 1])) cricketmodeknopf.setText(cricketmodes[0]);
                else
                    for (int i = 0; i < cricketmodevariants - 1; i++) {
                        if (aktuell.equals(cricketmodes[i])) {
                            cricketmodeknopf.setText(cricketmodes[i + 1]);
                            break;
                        }
                    }
            }
        });


        // Spieleranzahl wechseln bei touch

        spieleranzahl.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                CharSequence aktuell = spieleranzahl.getText();
                int i = Integer.parseInt(aktuell.toString());
                if ((allespieler.size() == i) && i != 8) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.fehlendespieler), Toast.LENGTH_LONG).show();
                } else {
                    if (allespieler.size() < i) i = 8;
                    switch (i) {
                        case 1:
                            spieleranzahl.setText("2");
                            spiele2text.setVisibility(View.VISIBLE);
                            if (spin2.getSelectedItemPosition()==0) spin2.setSelection(i);
                            spin2.setVisibility(View.VISIBLE);
                            break;
                        case 2:
                            spieleranzahl.setText("3");
                            spiele3text.setVisibility(View.VISIBLE);
                            if (spin3.getSelectedItemPosition()==0) spin3.setSelection(i);
                            spin3.setVisibility(View.VISIBLE);
                            break;
                        case 3:
                            spieleranzahl.setText("4");
                            spiele4text.setVisibility(View.VISIBLE);
                            if (spin4.getSelectedItemPosition()==0) spin4.setSelection(i);
                            spin4.setVisibility(View.VISIBLE);
                            break;
                        case 4:
                            spieleranzahl.setText("5");
                            spiele5text.setVisibility(View.VISIBLE);
                            if (spin5.getSelectedItemPosition()==0) spin5.setSelection(i);
                            spin5.setVisibility(View.VISIBLE);
                            break;
                        case 5:
                            spieleranzahl.setText("6");
                            spiele6text.setVisibility(View.VISIBLE);
                            if (spin6.getSelectedItemPosition()==0) spin6.setSelection(i);
                            spin6.setVisibility(View.VISIBLE);
                            break;
                        case 6:
                            spieleranzahl.setText("7");
                            spiele7text.setVisibility(View.VISIBLE);
                            if (spin7.getSelectedItemPosition()==0) spin7.setSelection(i);
                            spin7.setVisibility(View.VISIBLE);
                            break;
                        case 7:
                            spieleranzahl.setText("8");
                            spiele8text.setVisibility(View.VISIBLE);
                            if (spin7.getSelectedItemPosition()==0) spin8.setSelection(i);
                            spin8.setVisibility(View.VISIBLE);
                            break;
                            case 8:
                            spieleranzahl.setText("1");
                            spiele2text.setVisibility(View.GONE);
                            spin2.setVisibility(View.GONE);
                            spiele3text.setVisibility(View.GONE);
                            spin3.setVisibility(View.GONE);
                            spiele4text.setVisibility(View.GONE);
                            spin4.setVisibility(View.GONE);
                            spiele5text.setVisibility(View.GONE);
                            spin5.setVisibility(View.GONE);
                            spiele6text.setVisibility(View.GONE);
                            spin6.setVisibility(View.GONE);
                            spiele7text.setVisibility(View.GONE);
                            spin7.setVisibility(View.GONE);
                            spiele8text.setVisibility(View.GONE);
                            spin8.setVisibility(View.GONE);
                            break;
                    }
                }

            }
        });

        // startknopf-aktion
        startbutton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                int i = Integer.parseInt(spieleranzahl.getText().toString());
                if (i == 2) {
                    if (spin1.getSelectedItem().toString().equals(spin2.getSelectedItem().toString())) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.spielerdoppelt), Toast.LENGTH_LONG).show();
                        return;
                    }
                }

                if (i == 3) {
                    if (spin1.getSelectedItem().toString().equals(spin2.getSelectedItem().toString())
                            || spin1.getSelectedItem().toString().equals(spin3.getSelectedItem().toString())
                            || spin2.getSelectedItem().toString().equals(spin3.getSelectedItem().toString())) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.spielerdoppelt), Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                if (i == 4) {
                    if (spin1.getSelectedItem().toString().equals(spin2.getSelectedItem().toString())
                            || spin1.getSelectedItem().toString().equals(spin3.getSelectedItem().toString())
                            || spin1.getSelectedItem().toString().equals(spin4.getSelectedItem().toString())

                            || spin2.getSelectedItem().toString().equals(spin3.getSelectedItem().toString())
                            || spin2.getSelectedItem().toString().equals(spin4.getSelectedItem().toString())

                            || spin3.getSelectedItem().toString().equals(spin4.getSelectedItem().toString())
                            ) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.spielerdoppelt), Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                if (i == 5) {
                    if (spin1.getSelectedItem().toString().equals(spin2.getSelectedItem().toString())
                            || spin1.getSelectedItem().toString().equals(spin3.getSelectedItem().toString())
                            || spin1.getSelectedItem().toString().equals(spin4.getSelectedItem().toString())
                            || spin1.getSelectedItem().toString().equals(spin5.getSelectedItem().toString())

                            || spin2.getSelectedItem().toString().equals(spin3.getSelectedItem().toString())
                            || spin2.getSelectedItem().toString().equals(spin4.getSelectedItem().toString())
                            || spin2.getSelectedItem().toString().equals(spin5.getSelectedItem().toString())

                            || spin3.getSelectedItem().toString().equals(spin4.getSelectedItem().toString())
                            || spin3.getSelectedItem().toString().equals(spin5.getSelectedItem().toString())

                            || spin4.getSelectedItem().toString().equals(spin5.getSelectedItem().toString())


                    ) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.spielerdoppelt), Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                if (i == 6) {
                    if (spin1.getSelectedItem().toString().equals(spin2.getSelectedItem().toString())
                            || spin1.getSelectedItem().toString().equals(spin3.getSelectedItem().toString())
                            || spin1.getSelectedItem().toString().equals(spin4.getSelectedItem().toString())
                            || spin1.getSelectedItem().toString().equals(spin5.getSelectedItem().toString())
                            || spin1.getSelectedItem().toString().equals(spin6.getSelectedItem().toString())

                            || spin2.getSelectedItem().toString().equals(spin3.getSelectedItem().toString())
                            || spin2.getSelectedItem().toString().equals(spin4.getSelectedItem().toString())
                            || spin2.getSelectedItem().toString().equals(spin5.getSelectedItem().toString())
                            || spin2.getSelectedItem().toString().equals(spin6.getSelectedItem().toString())

                            || spin3.getSelectedItem().toString().equals(spin4.getSelectedItem().toString())
                            || spin3.getSelectedItem().toString().equals(spin5.getSelectedItem().toString())
                            || spin3.getSelectedItem().toString().equals(spin6.getSelectedItem().toString())

                            || spin4.getSelectedItem().toString().equals(spin5.getSelectedItem().toString())
                            || spin4.getSelectedItem().toString().equals(spin6.getSelectedItem().toString())

                            || spin5.getSelectedItem().toString().equals(spin6.getSelectedItem().toString())
                    ) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.spielerdoppelt), Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                if (i == 7) {
                    if (spin1.getSelectedItem().toString().equals(spin2.getSelectedItem().toString())
                            || spin1.getSelectedItem().toString().equals(spin3.getSelectedItem().toString())
                            || spin1.getSelectedItem().toString().equals(spin4.getSelectedItem().toString())
                            || spin1.getSelectedItem().toString().equals(spin5.getSelectedItem().toString())
                            || spin1.getSelectedItem().toString().equals(spin6.getSelectedItem().toString())
                            || spin1.getSelectedItem().toString().equals(spin7.getSelectedItem().toString())

                            || spin2.getSelectedItem().toString().equals(spin3.getSelectedItem().toString())
                            || spin2.getSelectedItem().toString().equals(spin4.getSelectedItem().toString())
                            || spin2.getSelectedItem().toString().equals(spin5.getSelectedItem().toString())
                            || spin2.getSelectedItem().toString().equals(spin6.getSelectedItem().toString())
                            || spin2.getSelectedItem().toString().equals(spin7.getSelectedItem().toString())

                            || spin3.getSelectedItem().toString().equals(spin4.getSelectedItem().toString())
                            || spin3.getSelectedItem().toString().equals(spin5.getSelectedItem().toString())
                            || spin3.getSelectedItem().toString().equals(spin6.getSelectedItem().toString())
                            || spin3.getSelectedItem().toString().equals(spin7.getSelectedItem().toString())

                            || spin4.getSelectedItem().toString().equals(spin5.getSelectedItem().toString())
                            || spin4.getSelectedItem().toString().equals(spin6.getSelectedItem().toString())
                            || spin4.getSelectedItem().toString().equals(spin7.getSelectedItem().toString())

                            || spin5.getSelectedItem().toString().equals(spin6.getSelectedItem().toString())
                            || spin5.getSelectedItem().toString().equals(spin7.getSelectedItem().toString())

                            || spin6.getSelectedItem().toString().equals(spin7.getSelectedItem().toString())

                    ) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.spielerdoppelt), Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                if (i == 8) {
                    if (spin1.getSelectedItem().toString().equals(spin2.getSelectedItem().toString())
                            || spin1.getSelectedItem().toString().equals(spin3.getSelectedItem().toString())
                            || spin1.getSelectedItem().toString().equals(spin4.getSelectedItem().toString())
                            || spin1.getSelectedItem().toString().equals(spin5.getSelectedItem().toString())
                            || spin1.getSelectedItem().toString().equals(spin6.getSelectedItem().toString())
                            || spin1.getSelectedItem().toString().equals(spin7.getSelectedItem().toString())
                            || spin1.getSelectedItem().toString().equals(spin8.getSelectedItem().toString())

                            || spin2.getSelectedItem().toString().equals(spin3.getSelectedItem().toString())
                            || spin2.getSelectedItem().toString().equals(spin4.getSelectedItem().toString())
                            || spin2.getSelectedItem().toString().equals(spin5.getSelectedItem().toString())
                            || spin2.getSelectedItem().toString().equals(spin6.getSelectedItem().toString())
                            || spin2.getSelectedItem().toString().equals(spin7.getSelectedItem().toString())
                            || spin2.getSelectedItem().toString().equals(spin8.getSelectedItem().toString())

                            || spin3.getSelectedItem().toString().equals(spin4.getSelectedItem().toString())
                            || spin3.getSelectedItem().toString().equals(spin5.getSelectedItem().toString())
                            || spin3.getSelectedItem().toString().equals(spin6.getSelectedItem().toString())
                            || spin3.getSelectedItem().toString().equals(spin7.getSelectedItem().toString())
                            || spin3.getSelectedItem().toString().equals(spin8.getSelectedItem().toString())

                            || spin4.getSelectedItem().toString().equals(spin5.getSelectedItem().toString())
                            || spin4.getSelectedItem().toString().equals(spin6.getSelectedItem().toString())
                            || spin4.getSelectedItem().toString().equals(spin7.getSelectedItem().toString())
                            || spin4.getSelectedItem().toString().equals(spin8.getSelectedItem().toString())

                            || spin5.getSelectedItem().toString().equals(spin6.getSelectedItem().toString())
                            || spin5.getSelectedItem().toString().equals(spin7.getSelectedItem().toString())
                            || spin5.getSelectedItem().toString().equals(spin8.getSelectedItem().toString())

                            || spin6.getSelectedItem().toString().equals(spin7.getSelectedItem().toString())
                            || spin6.getSelectedItem().toString().equals(spin8.getSelectedItem().toString())

                            || spin7.getSelectedItem().toString().equals(spin8.getSelectedItem().toString())
                    ) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.spielerdoppelt), Toast.LENGTH_LONG).show();
                        return;
                    }
                }

                if (spieltypeknopf.getText().equals("CRICKET")) {
                    Intent intent = new Intent(MainActivity.this, cricket.class);
                    intent.putExtra("spielmodus", spielmodeknopf.getText());
                    intent.putExtra("spieler1", spin1.getSelectedItem().toString());
                    intent.putExtra("spieler2", spin2.getSelectedItem().toString());
                    intent.putExtra("spieler3", spin3.getSelectedItem().toString());
                    intent.putExtra("spieler4", spin4.getSelectedItem().toString());
                    intent.putExtra("spieler5", spin5.getSelectedItem().toString());
                    intent.putExtra("spieler6", spin6.getSelectedItem().toString());
                    intent.putExtra("spieler7", spin7.getSelectedItem().toString());
                    intent.putExtra("spieler8", spin8.getSelectedItem().toString());
                    intent.putExtra("spieleranzahl", spieleranzahl.getText());
                    intent.putExtra("spielvariante",cricketmodeknopf.getText());
                    startActivity(intent);

                } else if (spieltypeknopf.getText().equals("ELIMINATION")) {
                    Intent intent;
                    boolean inputmode=false;
                    if (settings.contains("inputmethode")) inputmode=settings.getBoolean("inputmethode",false);
                    intent = new Intent(MainActivity.this, elimination.class);
                    intent.putExtra("spielmodus", spielmodeknopf.getText());
                    intent.putExtra("spieler1", spin1.getSelectedItem().toString());
                    intent.putExtra("spieler2", spin2.getSelectedItem().toString());
                    intent.putExtra("spieler3", spin3.getSelectedItem().toString());
                    intent.putExtra("spieler4", spin4.getSelectedItem().toString());
                    intent.putExtra("spieler5", spin5.getSelectedItem().toString());
                    intent.putExtra("spieler6", spin6.getSelectedItem().toString());
                    intent.putExtra("spieler7", spin7.getSelectedItem().toString());
                    intent.putExtra("spieler8", spin8.getSelectedItem().toString());
                    intent.putExtra("spieleranzahl", spieleranzahl.getText());
                    intent.putExtra("doubleout", doubleout.isChecked());
                    intent.putExtra("masterout", masterout.isChecked());
                    startActivity(intent);
                }

                else {
                    Intent intent;
                    boolean inputmode=false;
                    if (settings.contains("inputmethode")) inputmode=settings.getBoolean("inputmethode",false);
                    if (inputmode) { intent = new Intent(MainActivity.this, matchcalc.class);  }
                    else { intent = new Intent(MainActivity.this, match.class);}
                    intent.putExtra("spielmodus", spielmodeknopf.getText());
                    intent.putExtra("spieler1", spin1.getSelectedItem().toString());
                    intent.putExtra("spieler2", spin2.getSelectedItem().toString());
                    intent.putExtra("spieler3", spin3.getSelectedItem().toString());
                    intent.putExtra("spieler4", spin4.getSelectedItem().toString());
                    intent.putExtra("spieler5", spin5.getSelectedItem().toString());
                    intent.putExtra("spieler6", spin6.getSelectedItem().toString());
                    intent.putExtra("spieler7", spin7.getSelectedItem().toString());
                    intent.putExtra("spieler8", spin8.getSelectedItem().toString());
                    intent.putExtra("spieleranzahl", spieleranzahl.getText());
                    intent.putExtra("doubleout", doubleout.isChecked());
                    intent.putExtra("masterout", masterout.isChecked());
                    if (spieltypeknopf.getText().equals("SET/LEG")) {
                        intent.putExtra("sets", Integer.parseInt(sets.getText().toString()));
                        intent.putExtra("legs", Integer.parseInt(legs.getText().toString()));
                    } else {
                        intent.putExtra("sets", 0);
                        intent.putExtra("legs", 0);
                    }

                    startActivity(intent);
                }
            }
        });


        // freemodeeingabe übernehmen
        eingabe.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // eingabe kleiner 2?
                    try {
                        if ((eingabe.getText().toString().isEmpty()) || (Integer.parseInt(eingabe.getText().toString()) <= 1)) {
                            Toast.makeText(MainActivity.this, getResources().getString(R.string.minimum), Toast.LENGTH_LONG).show();
                            return true;
                        }
                    } catch (Exception e)   //größere eingabe als integer erlaubt
                    {
                        Toast.makeText(MainActivity.this, getResources().getString(R.string.maximum), Toast.LENGTH_LONG).show();
                        return true;
                    }
                    //willkürliche eingabegrenze überschritten?
                    if (Integer.parseInt(eingabe.getText().toString()) > 1000000) {
                        Toast.makeText(MainActivity.this, getResources().getString(R.string.maximum), Toast.LENGTH_LONG).show();
                        return true;
                    }

                    // edittext ausblenden
                    eingabe.setVisibility(View.INVISIBLE);
                    // doubleout und co wieder einblenden
                    doubleout.setVisibility(View.VISIBLE);
                    masterout.setVisibility(View.VISIBLE);
                    spieleranzahl.setEnabled(true);
                    spielmodeknopf.setEnabled(true);
                    spieltypeknopf.setEnabled(true);
                    spin1.setEnabled(true);
                    spin2.setEnabled(true);
                    spin3.setEnabled(true);
                    spin4.setEnabled(true);
                    spin5.setEnabled(true);
                    spin6.setEnabled(true);
                    spin7.setEnabled(true);
                    spin8.setEnabled(true);
                    startbutton.setEnabled(true);
                    statistikb.setEnabled(true);
                    einstellungsbutton.setEnabled(true);

                    // keyboard ausblenden
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    assert imm != null;
                    imm.hideSoftInputFromWindow(eingabe.getWindowToken(), 0);
                    spielmodeknopf.setText(eingabe.getText());
                    return true;
                }
                return false;
            }
        });


        final ScrollView sv = findViewById(R.id.menu);
       // ImageButton einstell = findViewById(R.id.einstellungsbutton);

        //menü einblenden bei klick auf einstellungsbutton
        einstellungsbutton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                GUIan(false);
            }

        });


        // menü ausblenden, wenn click ausserhalb desselben liegt
        // oder bei free_mode_eingabe tastatur einblenden
        ConstraintLayout cl = findViewById(R.id.startlayout);
        cl.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sv.getVisibility() == View.VISIBLE) {
                   GUIan(true);
                }

                if (eingabe.getVisibility() == View.VISIBLE) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    assert imm != null;
                    imm.showSoftInput(eingabe, InputMethodManager.SHOW_IMPLICIT);

                }
            }
        });

        statistikb.setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(MainActivity.this, statistik.class));
                    }
                }
        );

        final Button einstellungen = findViewById(R.id.buttoneinstellungen);

        einstellungen.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, einstellungen.class));
                GUIan(true);
            }
        });


        final Button infob = findViewById(R.id.infobutton);
        infob.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, info.class));
                GUIan(true);
            }
        });

        sliste.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, spielerliste.class));
                GUIan(true);
            }
        });

        final Button hilfe = findViewById(R.id.hilfebutton);
        hilfe.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, hilfe.class));
                GUIan(true);
            }
        });

        doubleout.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    masterout.setChecked(false);
                }

            }
        });

        masterout.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    doubleout.setChecked(true);
                }
            }
        });



        final Button zufall = findViewById(R.id.zufall);
        zufall.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                // zufällige spieltypwahl
                int i=0;
                Random rand = new Random();
                for (i=0;i<=rand.nextInt(spieltyptext.length);i++) {
                    spieltypeknopf.performClick();
                }

                // zufällige spielmodewahl in abhängikeit vom spieltyp

                // zufall sagt "cricket"
                int i2=0;
                if (spieltypeknopf.getText().equals("CRICKET")) {
                    for (i=0;i<=rand.nextInt(spielmode2.length);i++) {
                        spielmodeknopf.performClick();
                    }
                    for (i=0;i<=rand.nextInt(cricketmodes.length);i++) {
                        cricketmodeknopf.performClick();
                    }
                }

                else {
                    // zufall sagt "FREE"
                    if (spieltypeknopf.getText().equals("FREE")) {
                        spielmodeknopf.setText(Integer.toString(rand.nextInt(501)+1));
                    }
                    // zufall sagt "x01" oder "elimination" oder "set/leg"
                    else{
                        for (i = 0; i<=rand.nextInt(spielmode.length); i++) {
                            spielmodeknopf.performClick();
                        }
                    }
                        // setze single, double oder master out

                    i2 = rand.nextInt(3);
                    switch (i2) {
                        case 0:
                            doubleout.setChecked(false);
                            masterout.setChecked(false);
                            break;
                            case 1:
                                doubleout.setChecked(true);
                                masterout.setChecked(false);
                                break;
                                case 2:
                                    // doubleout.setChecked(true);
                                    masterout.setChecked(true);
                                    break;
                        }
                    if (spieltypeknopf.getText().equals("SET/LEG")) {
                        for (i=0;i<=rand.nextInt(maxsets);i++) sets.performClick();
                        for (i=0;i<=rand.nextInt(maxlegs);i++) legs.performClick();
                    }

                }
            }
        });


    }


    public void GUIan(boolean anaus) {
        final Spinner spin1 = findViewById(R.id.spinner);
        final Spinner spin2 = findViewById(R.id.spinner2);
        final Spinner spin3 = findViewById(R.id.spinner3);
        final Spinner spin4 = findViewById(R.id.spinner4);
        final Spinner spin5 = findViewById(R.id.spinner5);
        final Spinner spin6 = findViewById(R.id.spinner6);
        final Spinner spin7 = findViewById(R.id.spinner7);
        final Spinner spin8 = findViewById(R.id.spinner8);
        final Button spielmodeknopf = findViewById(R.id.spielmodus);
        final Button spieltypeknopf = findViewById(R.id.spieltyp);
        final Switch doubleout = findViewById(R.id.doubleout);
        final Switch masterout = findViewById(R.id.masterout);
        final Button spieleranzahl = findViewById(R.id.spieleranzahl);
        final Button startbutton = findViewById(R.id.startbutton);
        final ImageButton statistikb = findViewById(R.id.statistikbutton);
        final Button sets = findViewById(R.id.sets);
        final Button legs = findViewById(R.id.legs);
        final ScrollView sv = findViewById(R.id.menu);
        final Button crkmode =findViewById(R.id.cricketmodeknopf);
        final Button zufall =  findViewById(R.id.zufall);

        if (anaus) sv.setVisibility(View.INVISIBLE);
        else sv.setVisibility(View.VISIBLE);
        doubleout.setEnabled(anaus);
        masterout.setEnabled(anaus);
        if (crkmode.getVisibility()==View.VISIBLE) {
            doubleout.setEnabled(false);
            masterout.setEnabled(false);
        }
        spieleranzahl.setEnabled(anaus);
        spielmodeknopf.setEnabled(anaus);
        spieltypeknopf.setEnabled(anaus);
        spin1.setEnabled(anaus);
        spin2.setEnabled(anaus);
        spin3.setEnabled(anaus);
        spin4.setEnabled(anaus);
        spin5.setEnabled(anaus);
        spin6.setEnabled(anaus);
        spin7.setEnabled(anaus);
        spin8.setEnabled(anaus);
        startbutton.setEnabled(anaus);
        statistikb.setEnabled(anaus);
        sets.setEnabled(anaus);
        legs.setEnabled(anaus);
        crkmode.setEnabled(anaus);
        zufall.setEnabled(anaus);
    }


    @Override
    public void onResume() {
        super.onResume();

        arrayAdapter.notifyDataSetChanged();

        SharedPreferences settings = getSharedPreferences("Einstellungen", 0);

        // falls in den einstellungen die Sprache geändert wurde, neustart der activity
        if (settings.contains("Sprache")) {
           if (!(settings.getString("Sprache","en") == startsprache)) {
               Intent intent = getIntent();
               finish();
               startActivity(intent);
           }
        }

        //falls in den einstellungen das theme geändert wurde, neustart der activity

        if (settings.contains("Theme")) {
            if (!(settings.getBoolean("Theme", true) == themeauswahl)) {
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        }
        if (settings.contains("setlegmodus")) {
            if (!settings.getBoolean("setlegmodus", true)) {
                maxlegs = 5;
                maxsets = 13;
                setlegmodus = false;
            } else {
                maxlegs = 3;
                maxsets = 7;
                setlegmodus = true;
                Button legsb = findViewById(R.id.legs);
                Button setsb = findViewById(R.id.sets);
                if (Integer.parseInt(setsb.getText().toString()) > maxsets)
                    setsb.setText(String.valueOf(maxsets));
                if (Integer.parseInt(legsb.getText().toString()) > maxlegs)
                    legsb.setText(String.valueOf(maxlegs));
            }
        }



    }

    public void onBackPressed() {
        final EditText eingabe = findViewById(R.id.eingabe);
        if (eingabe.getVisibility() == View.VISIBLE) {
            Switch doubleout = findViewById(R.id.doubleout);
            Switch masterout = findViewById(R.id.masterout);
            Button spieleranzahl = findViewById(R.id.spieleranzahl);
            Button spieltypeknopf = findViewById(R.id.spieltyp);
            Button spielmodeknopf = findViewById(R.id.spielmodus);
            Button startbutton = findViewById(R.id.startbutton);
            Button sets = findViewById(R.id.sets);
            Button legs = findViewById(R.id.legs);
            Button zufall = findViewById(R.id.zufall);
            ImageButton statistikb = findViewById(R.id.statistikbutton);
            ImageButton einstellungsbutton = findViewById(R.id.einstellungsbutton);
            Spinner spin1 = findViewById(R.id.spinner);
            Spinner spin2 = findViewById(R.id.spinner2);
            Spinner spin3 = findViewById(R.id.spinner3);
            Spinner spin4 = findViewById(R.id.spinner4);
            Spinner spin5 = findViewById(R.id.spinner5);
            Spinner spin6 = findViewById(R.id.spinner6);
            Spinner spin7 = findViewById(R.id.spinner7);
            Spinner spin8 = findViewById(R.id.spinner8);
            eingabe.setVisibility(View.INVISIBLE);
            doubleout.setVisibility(View.VISIBLE);
            masterout.setVisibility(View.VISIBLE);
            spieleranzahl.setEnabled(true);
            spielmodeknopf.setEnabled(true);
            spieltypeknopf.setEnabled(true);
            spin1.setEnabled(true);
            spin2.setEnabled(true);
            spin3.setEnabled(true);
            spin4.setEnabled(true);
            spin5.setEnabled(true);
            spin6.setEnabled(true);
            spin7.setEnabled(true);
            spin8.setEnabled(true);
            startbutton.setEnabled(true);
            statistikb.setEnabled(true);
            einstellungsbutton.setEnabled(true);
            sets.setEnabled(true);
            legs.setEnabled(true);
            zufall.setEnabled(true);


        } else {
            finish();
        }
    }

    public static class spieler implements Serializable {
        private static final long serialVersionUID = 19L;
        String spielerName;
        int AnzahlSpiele;
        int AnzahlEinzelspiele;
        int anzahlSiege;
        int besterWurf;
        int zweitbesterWurf;
        float durchschnitt;
        int geworfenePfeile;
        int anzahluber60;
        int anzahluber100;
        int anzahluber140;
        int anzahl180;
        int score;
        int legs;
        int sets = 0;
        int matcheswon;
        int matcheslost;
        int checkoutmax;
        int legswon;

        @Override
        public String toString() {
            return this.spielerName;
        }

    }
}
