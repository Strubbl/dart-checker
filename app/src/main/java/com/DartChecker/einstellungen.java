package com.DartChecker;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Locale;

public class einstellungen extends AppCompatActivity {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(updateBaseContextLocale(base));
    }

    private Context updateBaseContextLocale(Context context) {
        final SharedPreferences settings = context.getSharedPreferences("Einstellungen", 0);
        String language = Locale.getDefault().getLanguage();
        if (settings.contains("Sprache")) {
            language = settings.getString("Sprache", "en");
        }
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return updateResourcesLocale(context, locale);
        }

        return updateResourcesLocaleLegacy(context, locale);
    }

    @TargetApi(Build.VERSION_CODES.N_MR1)
    private Context updateResourcesLocale(Context context, Locale locale) {
        Configuration configuration = new Configuration(context.getResources().getConfiguration());
        configuration.setLocale(locale);
        return context.createConfigurationContext(configuration);
    }

    @SuppressWarnings("deprecation")
    private Context updateResourcesLocaleLegacy(Context context, Locale locale) {
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        return context;
    }


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
        boolean darktheme = true,
                slmode = true,
                inputmode = false,
                suddendeathmode = false,
                spielgehtweiter = false;
        if (settings.contains("Theme")) darktheme = settings.getBoolean("Theme", true);
        if (darktheme) setTheme(R.style.AppTheme);
        else setTheme(R.style.Theme_AppCompat_Light_NoActionBar);
        setContentView(R.layout.activity_einstellungen);

        final Switch setmode = findViewById(R.id.slmode);
        final Switch themes = findViewById(R.id.themeswitch);
        TextView umschaltzeit = findViewById(R.id.millisekunden);
        final TextView dartanzahl = findViewById(R.id.dartanzahl);
        TextView crazystart = findViewById(R.id.crazystartsegment);
        final Switch inputmethode = findViewById(R.id.inputmethode);
        final Switch suddendeath = findViewById(R.id.suddendeath);
        final Switch spielgehtweiterswitch = findViewById(R.id.cricketkeinstop);
        final Switch undosicherheitsabfrage = findViewById(R.id.undosicherheit);
        int anzahldarts = 0;

        if (settings.contains("crazystart"))
            crazystart.setText(Integer.toString(settings.getInt("crazystart", 6)));

        if (settings.contains("changetime"))
            umschaltzeit.setText(Integer.toString(settings.getInt("changetime", 1500)));

        if (settings.contains("setlegmodus")) slmode = settings.getBoolean("setlegmodus", true);
        setmode.setChecked(slmode);
        if (slmode) {
            setmode.setText(getResources().getString(R.string.setlegmodusfirstto));
        } else {
            setmode.setText(getResources().getString(R.string.setlegmodusbestof));
        }

        themes.setChecked(darktheme);
        if (darktheme) {
            themes.setText(getResources().getString(R.string.design_dunkel));
        } else {
            themes.setText(getResources().getString(R.string.design_hell));
        }

        if (settings.contains("inputmethode"))
            inputmode = settings.getBoolean("inputmethode", false);
        inputmethode.setChecked(inputmode);
        if (inputmode) {
            inputmethode.setText(getResources().getString(R.string.inputmode3dart));
        } else {
            inputmethode.setText(getResources().getString(R.string.inputmode1dart));
        }

        if (settings.contains("suddendeath"))
            suddendeathmode = settings.getBoolean("suddendeath", false);
        suddendeath.setChecked(suddendeathmode);
        if (settings.contains("sddarts")) anzahldarts = settings.getInt("sddarts", 20);
        dartanzahl.setText(Integer.toString(anzahldarts));
        dartanzahl.setEnabled(suddendeath.isChecked());
        if (settings.contains("spielgehtweiter"))
            spielgehtweiter = settings.getBoolean("spielgehtweiter", false);
        spielgehtweiterswitch.setChecked(spielgehtweiter);

        ArrayList sprachen = new ArrayList<>();
        sprachen.add(getResources().getString(R.string.system));
        sprachen.add("Deutsch");
        sprachen.add("English");
        sprachen.add("Español");
        ArrayAdapter arrayAdapter = new ArrayAdapter<>(
                einstellungen.this,
                R.layout.spinner_item_head, sprachen);
        arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        final Spinner spin1 = findViewById(R.id.sprache);
        spin1.setAdapter(arrayAdapter);
        if (settings.contains("SprachZahl")) spin1.setSelection(settings.getInt("SprachZahl", 0));
        if (settings.contains("Undosicherheitsabfrage"))
            undosicherheitsabfrage.setChecked(settings.getBoolean("Undosicherheitsabfrage", false));

        spin1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                String language_code = "en";
                switch (position) {
                    case 0:
                        language_code = settings.getString("SystemSprache", language_code);
                        break;
                    case 1:
                        language_code = "de";
                        break;
                    case 2:
                        language_code = "en";
                        break;
                    case 3:
                        language_code = "es";
                        break;
                }

                SharedPreferences.Editor editor = settings.edit();
                editor.putString("Sprache", language_code);
                editor.putInt("SprachZahl", position);
                editor.apply();
                if (settings.contains("Sprache")) {
                    if (!(language_code.equals(Locale.getDefault().getLanguage()))) {
                        // Sprache aktualisieren durch Neustart der activity
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> AdapterView) {
            }

        });

        undosicherheitsabfrage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Switch undoabfrage = findViewById(R.id.undosicherheit);
                SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("Undosicherheitsabfrage", undoabfrage.isChecked());
                editor.apply();
            }
        });

        themes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    themes.setText(getResources().getString(R.string.design_dunkel));
                } else {
                    themes.setText(getResources().getString(R.string.design_hell));

                }
                Switch theme = findViewById(R.id.themeswitch);
                SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("Theme", theme.isChecked());
                editor.apply();
                //einstellungsactivity neu starten
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        });
        findViewById(R.id.appBarLayout).requestFocus();


        setmode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    setmode.setText(getResources().getString(R.string.setlegmodusfirstto));
                } else setmode.setText(getResources().getString(R.string.setlegmodusbestof));

                SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("setlegmodus", setmode.isChecked());
                editor.apply();
            }
        });

        inputmethode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    inputmethode.setText(getResources().getString(R.string.inputmode3dart));
                } else inputmethode.setText(getResources().getString(R.string.inputmode1dart));

                SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("inputmethode", inputmethode.isChecked());
                editor.apply();
            }
        });

        suddendeath.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    dartanzahl.setEnabled(true);
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.suddendeathHinweis), Toast.LENGTH_LONG).show();
                } else dartanzahl.setEnabled(false);
                SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("suddendeath", suddendeath.isChecked());
                editor.apply();
            }
        });

        spielgehtweiterswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("spielgehtweiter", spielgehtweiterswitch.isChecked());
                editor.apply();
            }
        });

        final Button ok = findViewById(R.id.okb2);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        crazystart.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // eingabe zwischen 1 und 13?
                    try {
                        if ((crazystart.getText().toString().isEmpty()) || (Integer.parseInt(crazystart.getText().toString()) < 1) || (Integer.parseInt(crazystart.getText().toString()) > 13) ) {
                            Toast.makeText(einstellungen.this, getResources().getString(R.string.range), Toast.LENGTH_LONG).show();
                            return true;
                        }
                    } catch (Exception e) {
                        //größere eingabe als integer erlaubt
                    }
                }
                return false;
            }
        });



    }

    public void onBackPressed() {
        //Zahleneingaben-Einstellungen speichern
        TextView millisekunden = findViewById(R.id.millisekunden);
        TextView dartanzahl = findViewById(R.id.dartanzahl);
        TextView crazystart = findViewById(R.id.crazystartsegment);
        SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("changetime", Integer.valueOf(millisekunden.getText().toString()));
        editor.putInt("sddarts", Integer.valueOf(dartanzahl.getText().toString()));
        editor.putInt("crazystart", Integer.valueOf(crazystart.getText().toString()));
        editor.apply();
        finish();
    }


}
