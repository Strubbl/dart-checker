package com.DartChecker;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.Guideline;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;

public class elimination extends AppCompatActivity {

    private static class pfeil {
        int zahl;
        int faktor;
        int addpunkte;
        int spielerindex;
    }

    private static class eliminationscore {
        int wurfindex;
        String spielername;
        int score_eliminated;
    }


    private final MainActivity.spieler spieler1 = new MainActivity.spieler();
    private final MainActivity.spieler spieler2 = new MainActivity.spieler();
    private final MainActivity.spieler spieler3 = new MainActivity.spieler();
    private final MainActivity.spieler spieler4 = new MainActivity.spieler();
    private final MainActivity.spieler spieler5 = new MainActivity.spieler();
    private final MainActivity.spieler spieler6 = new MainActivity.spieler();
    private final MainActivity.spieler spieler7 = new MainActivity.spieler();
    private final MainActivity.spieler spieler8 = new MainActivity.spieler();

    private final MainActivity.spieler[] spieler = new MainActivity.spieler[9];
    private final ArrayList<elimination.pfeil> wuerfe = new ArrayList<elimination.pfeil>();
    private final ArrayList<elimination.eliminationscore> eliminations = new ArrayList<eliminationscore>();

    private int aktiverSpieler = 1,       //index für namensarray
                ii = -1;                   //index für undo-speicher



    private final int maxundo = 3;
    private final int[] last3 = new int[(6 * maxundo) + 1];    //speicher für undo (+1 um den start des feldes bei 0 zu kompensieren)
    private final String[][] checkouts = new String[171][4];
    private final DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US);
    private final DecimalFormat formater = new DecimalFormat("###.##", symbols);
    private Integer spieleranzahl;
    private boolean doubleout,
                    masterout,
                    suddendeath;
    private int maxPunkte,
                suddendeathdarts;
    private TextView score;
    private TextView d1;
    private TextView d2;
    private TextView d3;
    private TextView v1;
    private TextView v2;
    private TextView v3;
    private TextView name;
    private TextView durchschnitt;
    private TextView pfeile;
    private TextView listenName1;
    private TextView listenName2;
    private TextView listenName3;
    private TextView listenName4;
    private TextView listenName5;
    private TextView listenName6;
    private TextView listenName7;
    private TextView listenPunkte1;
    private TextView listenPunkte2;
    private TextView listenPunkte3;
    private TextView listenPunkte4;
    private TextView listenPunkte5;
    private TextView listenPunkte6;
    private TextView listenPunkte7;
    private TextView listenvorschlag1;
    private TextView listenvorschlag2;
    private TextView listenvorschlag3;
    private TextView listenvorschlag4;
    private TextView listenvorschlag5;
    private TextView listenvorschlag6;
    private TextView listenvorschlag7;
    private TextView scoretoeliminate;
    private TextView toeliminate;

    private int changetime = 1500;
    private float fdummy;
    private int last3zeiger = 1;
    private boolean d = false;
    private boolean t = false;
    private int xdart = 0;           //geworfene darts in der aktuellen runde
    private int dummy;
    private int dummy2 = 0;
    private long startTime = 0;
    private long spielzeit = 0;
    private final View.OnClickListener doubletriple = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Button doubleb = findViewById(R.id.doublebutton);
            Button tripleb = findViewById(R.id.triple);
            doubleb.setBackgroundColor(bcolorn);        // workaround for low api version (15)
            tripleb.setBackgroundColor(bcolorn);        // workaround for low api version (15)

            if (v.getId() == R.id.doublebutton) {
                if (t) {
                    t = false;
                    tripleb.setBackgroundColor(bcolorn);
                }
                if (!d) {
                    d = true;
                    doubleb.setBackgroundColor(bcolor);
                } else {
                    d = false;
                    doubleb.setBackgroundColor(bcolorn);
                }
            } else {
                if (d) {
                    d = false;
                    doubleb.setBackgroundColor(bcolorn);
                }
                if (!t) {
                    t = true;
                    tripleb.setBackgroundColor(bcolor);
                } else {
                    t = false;
                    tripleb.setBackgroundColor(bcolorn);
                }
            }
        }
    };
    private String rundenname;
    private String dot = "";             // inhalt "", "D" oder "T" für anzeige
    private int bcolor;
    private int bcolorn;
    private int dummy3 = 0;
    private boolean matchgame = false;
    private boolean letzterwurf = false;

    private final View.OnClickListener undoclick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (wuerfe.size() == 0) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.keinundo), Toast.LENGTH_LONG).show();
                return;
            }

            if (d) //double oder triple gedrückt? dann setz die tasten zurück und mach gar nichts
            {
                d = false;
                Button doubleb = findViewById(R.id.doublebutton);
                doubleb.setBackgroundColor(bcolorn);
                return;
            } else if (t)  //triple?
            {
                t = false;
                Button tripleb = findViewById(R.id.triple);
                tripleb.setBackgroundColor(bcolorn);
                return;
            }

            // sicherheitsabfrage
            AlertDialog alertDialog = new AlertDialog.Builder(elimination.this).create();
            alertDialog.setTitle(getResources().getString(R.string.achtung));
            alertDialog.setMessage(getResources().getString(R.string.willstduUndo));
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.jaichw),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            String dartwert;
                            rundenname = name.getText().toString();
                            dartwert = "0";
                            dummy = Integer.parseInt(score.getText().toString());
                            int xdartsave=xdart;
                            switch (xdartsave) {
                                case 0:            //vorherigen spieler wieder auf anzeige holen und von diesem den letzten dartwurf entfernen
                                {
                                    // daten sichern falls schon zuvor 3x undo gedrückt wurde 
                                    if (rundenname == spieler1.spielerName) {
                                        spieler1.score = Integer.parseInt((score.getText().toString()));
                                        spieler1.geworfenePfeile = Integer.parseInt(pfeile.getText().toString());
                                        if (spieler1.geworfenePfeile < 3)
                                            spieler1.durchschnitt = 0;
                                        else
                                        spieler1.durchschnitt = spieler1.score / (spieler1.geworfenePfeile * 3);
                                    }
                                    else if (rundenname == spieler2.spielerName) {
                                        spieler2.score = Integer.parseInt((score.getText().toString()));
                                        spieler2.geworfenePfeile = Integer.parseInt(pfeile.getText().toString());
                                        if (spieler2.geworfenePfeile < 3)
                                            spieler2.durchschnitt = 0;
                                        else
                                        spieler2.durchschnitt = spieler2.score / (spieler2.geworfenePfeile * 3);
                                    }
                                    else if (rundenname == spieler3.spielerName) {
                                        spieler3.score = Integer.parseInt((score.getText().toString()));
                                        spieler3.geworfenePfeile = Integer.parseInt(pfeile.getText().toString());
                                        if (spieler3.geworfenePfeile < 3)
                                            spieler3.durchschnitt = 0;
                                        else
                                        spieler3.durchschnitt = spieler3.score / (spieler3.geworfenePfeile * 3);
                                    }
                                    else if (rundenname == spieler4.spielerName) {
                                        spieler4.score = Integer.parseInt((score.getText().toString()));
                                        spieler4.geworfenePfeile = Integer.parseInt(pfeile.getText().toString());
                                        if (spieler4.geworfenePfeile < 3)
                                            spieler4.durchschnitt = 0;
                                        else
                                        spieler4.durchschnitt = spieler4.score / (spieler4.geworfenePfeile * 3);
                                    }
                                    else if (rundenname == spieler5.spielerName) {
                                        spieler5.score = Integer.parseInt((score.getText().toString()));
                                        spieler5.geworfenePfeile = Integer.parseInt(pfeile.getText().toString());
                                        if (spieler5.geworfenePfeile < 3)
                                            spieler5.durchschnitt = 0;
                                        else
                                        spieler5.durchschnitt = spieler5.score / (spieler5.geworfenePfeile * 3);
                                    }
                                    else if (rundenname == spieler6.spielerName) {
                                        spieler6.score = Integer.parseInt((score.getText().toString()));
                                        spieler6.geworfenePfeile = Integer.parseInt(pfeile.getText().toString());
                                        if (spieler6.geworfenePfeile < 3)
                                            spieler6.durchschnitt = 0;
                                        else
                                        spieler6.durchschnitt = spieler6.score / (spieler6.geworfenePfeile * 3);
                                    }
                                    else if (rundenname == spieler7.spielerName) {
                                        spieler7.score = Integer.parseInt((score.getText().toString()));
                                        spieler7.geworfenePfeile = Integer.parseInt(pfeile.getText().toString());
                                        if (spieler7.geworfenePfeile < 3)
                                            spieler7.durchschnitt = 0;
                                        else
                                        spieler7.durchschnitt = spieler7.score / (spieler7.geworfenePfeile * 3);
                                    }
                                    else if (rundenname == spieler8.spielerName) {
                                        spieler8.score = Integer.parseInt((score.getText().toString()));
                                        spieler8.geworfenePfeile = Integer.parseInt(pfeile.getText().toString());
                                        if (spieler8.geworfenePfeile < 3)
                                            spieler8.durchschnitt = 0;
                                        else
                                        spieler8.durchschnitt = spieler8.score / (spieler8.geworfenePfeile * 3);
                                    }
                                    lastplayer();  //stellt anzeige her und bereinigt bei bedarf die statistik
                                /*    if (Integer.parseInt(pfeile.getText().toString()) <= 0) {
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.keinundo), Toast.LENGTH_SHORT).show();
                                        return;
                                    }

                                 */
                                    dummy = Integer.parseInt(score.getText().toString());
                                    dartwert = d3.getText().toString();
                                    d3.setText("-");
                                    xdart = 2;

                                    int fak3,fak2,fak1,dart3,dart2,dart1,punkte3,punkte2,punkte1;
                                    fak3=wuerfe.get(wuerfe.size()-1).faktor;
                                    fak2=wuerfe.get(wuerfe.size()-2).faktor;
                                    fak1=wuerfe.get(wuerfe.size()-3).faktor;
                                    dart3=wuerfe.get(wuerfe.size()-1).zahl;
                                    dart2=wuerfe.get(wuerfe.size()-2).zahl;
                                    dart1=wuerfe.get(wuerfe.size()-3).zahl;
                                    punkte3 =wuerfe.get(wuerfe.size()-1).addpunkte;
                                    punkte2 =wuerfe.get(wuerfe.size()-2).addpunkte;
                                    punkte1 =wuerfe.get(wuerfe.size()-3).addpunkte;

                                    dummy -= punkte3;
                                    undo_eliminations();
                                    wuerfe.remove(wuerfe.size()-1);


                                    // wenn 1. und/oder 2. dart nicht zählte (weil überworfen mit 3.), dann zählen sie jetzt
                                     if (fak2!=0 && punkte2==0) {
                                         punkte2 = dart2*fak2;
                                         dummy += punkte2;
                                         pfeil neu = wuerfe.get(wuerfe.size()-1);
                                         neu.addpunkte=neu.faktor*neu.zahl;
                                         wuerfe.set(wuerfe.size()-1,neu);
                                         neu=null;
                                     }
                                     if (fak1!=0 && punkte1==0) {
                                         punkte1 = dart1*fak1;
                                         dummy += punkte1;
                                         pfeil neu = wuerfe.get(wuerfe.size()-2);
                                         neu.addpunkte=neu.faktor*neu.zahl;
                                         wuerfe.set(wuerfe.size()-2,neu);
                                         neu=null;
                                     }

                                 /* alte version
                                    setLast3zeiger(false);
                                    if (!(last3[last3zeiger + 2] == 111))          //3. dart ungültig? (addieren oder ignorieren)
                                    {
                                        dummy = dummy - (last3[last3zeiger] * last3[last3zeiger + 1]);
                                        last3[last3zeiger] = 0;
                                        last3[last3zeiger + 1] = 0;
                                        last3[last3zeiger + 2] = 0;
                                    } else {   //wenn überworfen, sollten die vorherigen darts den score verringern
                                        //sofern sie angerechnet werden dürfen

                                        last3[last3zeiger] = 0;
                                        last3[last3zeiger + 1] = 0;
                                        last3[last3zeiger + 2] = 0;

                                        setLast3zeiger(false);
                                        if (!(last3[last3zeiger + 2] == 111))       //2. dart gültig?
                                        {                                   // -> ja: also 2.und 1. dart abziehen
                                            dummy = dummy + (last3[last3zeiger] * last3[last3zeiger + 1]); //ist gültiger wurf, hätte der überwurf mit dem 3. dart nicht stattgefunden
                                            setLast3zeiger(false);
                                            dummy = dummy + (last3[last3zeiger] * last3[last3zeiger + 1]);
                                            setLast3zeiger(true);       //und stop; zeiger auf 3. dart stehen lassen
                                            setLast3zeiger(true);


                                        } else //wenn der 2. auch nicht angerechnet wurde bzw. darf, dann fliegt die anzeige raus und zeiger einen zurück
                                        {
                                            last3[last3zeiger] = 0;
                                            last3[last3zeiger + 1] = 0;
                                            last3[last3zeiger + 2] = 0;
                                            dartwert=d2.getText().toString();
                                            d2.setText("-");
                                            xdart = 1;
                                            pfeile.setText(Integer.toString(Integer.parseInt(pfeile.getText().toString()) - 1));
                                            setLast3zeiger(false);
                                            if (!(last3[last3zeiger + 2] == 111))           //1. dart überworfen?
                                            {
                                                dummy = dummy + (last3[last3zeiger] * last3[last3zeiger + 1]);
                                                setLast3zeiger(true); //und stop
                                            } else {
                                                last3[last3zeiger] = 0;
                                                last3[last3zeiger + 1] = 0;
                                                last3[last3zeiger + 2] = 0;
                                                dartwert=d1.getText().toString();
                                                d1.setText("-");
                                                xdart = 0;
                                                pfeile.setText(Integer.toString(Integer.parseInt(pfeile.getText().toString()) - 1));

                                            }

                                        }

                                    }

*/
                                    break;
                                }

                                case 1: {
                                    // zeiger auf vorherigen dart stellen, der nach dem auslesen auch wieder überschrieben wird
                                    if (wuerfe.get(wuerfe.size()-1).faktor != 0) {
                                        dummy -= wuerfe.get(wuerfe.size()-1).addpunkte;
                                    }

                                    /* alte version
                                    setLast3zeiger(false);
                                    if (!(last3[last3zeiger + 2] == 111)) {
                                        dummy = dummy - (last3[last3zeiger] * last3[last3zeiger + 1]);
                                        last3[last3zeiger] = 0;
                                        last3[last3zeiger + 1] = 0;
                                        last3[last3zeiger + 2] = 0;
                                    }
                                  */
                                    undo_eliminations();
                                    wuerfe.remove(wuerfe.size()-1);
                                    dartwert=d1.getText().toString();
                                    d1.setText("-");
                                    xdart--;
                                    break;
                                }
                                case 2: {
                                    if (wuerfe.get(wuerfe.size()-1).faktor != 0) {
                                        dummy -= wuerfe.get(wuerfe.size()-1).addpunkte;
                                    }

                                                                       /* alte version

                                    setLast3zeiger(false);
                                    if (!(last3[last3zeiger + 2] == 111)) {
                                        dummy = dummy - (last3[last3zeiger] * last3[last3zeiger + 1]);
                                        last3[last3zeiger] = 0;
                                        last3[last3zeiger + 1] = 0;
                                        last3[last3zeiger + 2] = 0;
                                    }

                                                                        */
                                    undo_eliminations();
                                    wuerfe.remove(wuerfe.size()-1);
                                    dartwert=d2.getText().toString();
                                    d2.setText("-");
                                    xdart--;
                                    break;
                                }
                            }

                            score.setText(String.valueOf(dummy));
                            pfeile.setText(Integer.toString(Integer.parseInt(pfeile.getText().toString()) - 1));
                            fdummy = dummy;
                            if ((fdummy > 0) && (Integer.parseInt(pfeile.getText().toString()) >= 3))
                                durchschnitt.setText(formater.format((fdummy / Integer.parseInt(pfeile.getText().toString()) * 3)));
                            else
                                durchschnitt.setText("0");
                            vorschlagsetzen(dummy);
                            eliminationsvorschlagsetzen();
                            Toast.makeText(getApplicationContext(),"undo: "+dartwert+" "+getResources().getString(R.string.von_spieler)+" "+rundenname,Toast.LENGTH_SHORT).show();
                        }
                    });
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.nein), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    return;
                }
            });
            alertDialog.show();
            final SharedPreferences settings = getApplicationContext().getSharedPreferences("Einstellungen", 0);
            if (settings.contains("Undosicherheitsabfrage")) {
                if (!settings.getBoolean("Undosicherheitsabfrage",false)) {
                    alertDialog.hide();
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).performClick();
                }
            }
        }
    };

    private final View.OnClickListener setleganzeigeclick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            TextView aufnahmetv = findViewById(R.id.aufnahmetv);
            buttonfreeze(false);
            aufnahmetv.setVisibility(View.INVISIBLE);
        }
    };


    private final View.OnClickListener buttonclick = new View.OnClickListener() {
        @SuppressLint("NonConstantResourceId")
        @Override
        public void onClick(View v) {
            int sender = v.getId();
            Button eingabe = findViewById(sender);
            rundenname = name.getText().toString();  // aktueller spieler
            int ergebnis;
            ergebnis = Integer.parseInt(score.getText().toString());


            // welche taste wurde gedrückt?
            int dart;
            switch (sender) {
                case R.id.bull:
                    dart = 25;
                    break;
                case R.id.daneben:
                    dart = 0;
                    break;
                default:
                    try {
                        dart = Integer.parseInt(eingabe.getText().toString());
                    } catch (Exception e) {
                        dart = 255;         //fehlerausgabe - sollte niemals eintreten
                    }
                    break;
            }

            int faktor;
            if (d) //double?
            {
                faktor = 2;
                dot = "D";
                d = false;
                Button doubleb = findViewById(R.id.doublebutton);
                doubleb.setBackgroundColor(bcolorn);
            } else if (t)  //triple?
            {
                faktor = 3;
                dot = "T";
                t = false;
                Button tripleb = findViewById(R.id.triple);
                tripleb.setBackgroundColor(bcolorn);
                if (dart == 25) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.triplewirklich), Toast.LENGTH_LONG).show();
                    return;
                }
            } else //single!
            {
                faktor = 1;
                dot = "";
            }
            //damit kein "D0" oder "T0" in der anzeige erscheint
            if (dart == 0) {
                faktor = 1;
                dot = "";
            }

            dummy = ergebnis + (faktor * dart);

            if ((doubleout && dummy == maxPunkte - 1) || dummy > maxPunkte || ((dummy == maxPunkte && doubleout && !(faktor == 2)) && !(masterout && (faktor == 3)))) {

                if (doubleout && dummy == maxPunkte - 1)
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.keindoubleo), Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.uberworfen), Toast.LENGTH_SHORT).show();

                //wurf speichern für undo
                pfeil aktuellerWurf = new pfeil();
                aktuellerWurf.faktor=0;
                aktuellerWurf.zahl=dart;
                aktuellerWurf.spielerindex=aktiverSpieler;
                aktuellerWurf.addpunkte=0;                      //keine Punkte zum addieren, weil überworfen wurde
                wuerfe.add(aktuellerWurf);
// alte version
/*                last3[last3zeiger] = faktor;
                last3[last3zeiger + 1] = dart;
                last3[last3zeiger + 2] = 111;
*/
                // bereits geworfene Punkte wieder addieren und punktewerte im speicher auf 0 setzen
                switch (xdart) {
                    // überworfen mit 1. dart -> die nächsten 2 darts auf 0 setzen.
                    // score bleibt unverändert
                    case 0: {
                        d1.setText(dot + dart);         //anzeige für die umschaltzeit aktualisieren
                        aktuellerWurf.faktor=0;
                        aktuellerWurf.zahl=0;
                        wuerfe.add(aktuellerWurf);      //2x nüscht speichern
                        wuerfe.add(aktuellerWurf);
// alte version
                        /*
                            setLast3zeiger(true);
                            last3[last3zeiger] = 1;
                            last3[last3zeiger + 1] = 0;
                            last3[last3zeiger + 2] = 111;
                            setLast3zeiger(true);
                            last3[last3zeiger] = 1;
                            last3[last3zeiger + 1] = 0;
                            last3[last3zeiger + 2] = 111;
                        */
                        break;
                    }
                    // überworfen mit 2. dart - subtraktion des 1. darts und nullen des 3.
                    case 1: {
                        d2.setText(dot + dart);         //anzeige für die umschaltzeit aktualisieren
                        score.setText(String.valueOf(ergebnis - wuerfe.get(wuerfe.size()-2).addpunkte));   //vorletzter Wurf (letzter gespeicherter Wurf wäre size-1)
                        pfeil vorletzterpfeil = new pfeil();
                        vorletzterpfeil = wuerfe.get(wuerfe.size()-2);
                        vorletzterpfeil.addpunkte=0;
                        wuerfe.set(wuerfe.size()-2,vorletzterpfeil); //addpunkte vom 1. dart als nicht gewertet speichern, falls ein undo auf diesen wurf zugreift
                        aktuellerWurf.faktor=0;
                        aktuellerWurf.zahl=0;
                        wuerfe.add(aktuellerWurf);      //nüscht speichern
                        vorletzterpfeil = null;
//alte version
               /*         setLast3zeiger(false);
                        if (!(last3[last3zeiger + 2] == 111))
                            score.setText(String.valueOf(ergebnis - (last3[last3zeiger] * last3[last3zeiger + 1])));
                        setLast3zeiger(true);
                        setLast3zeiger(true);
                        last3[last3zeiger] = 1;
                        last3[last3zeiger + 1] = 0;
                        last3[last3zeiger + 2] = 111;
*/
                        break;
                    }
                    // überworfen mit 3. dart - subtraktion der beiden ersten darts und zurücksetzen deren gezählter Punkte
                    case 2: {
                        d3.setText(dot + dart);         //anzeige für die umschaltzeit aktualisieren
                        dummy2 = ergebnis;
                        dummy2 -= wuerfe.get(wuerfe.size()-2).addpunkte;    //vorletzter
                        dummy2 -= wuerfe.get(wuerfe.size()-3).addpunkte;    //vorvorletzter
                        score.setText(String.valueOf(dummy2));
                        pfeil vorletzterpfeil = new pfeil();
                        vorletzterpfeil = wuerfe.get(wuerfe.size()-2);
                        vorletzterpfeil.addpunkte=0;
                        wuerfe.set(wuerfe.size()-2,vorletzterpfeil);

                        vorletzterpfeil = wuerfe.get(wuerfe.size()-3);
                        vorletzterpfeil.addpunkte=0;
                        wuerfe.set(wuerfe.size()-3,vorletzterpfeil);
                        vorletzterpfeil=null;


        /*  alte version
                        setLast3zeiger(false);
                        if (!(last3[last3zeiger + 2] == 111))
                            dummy2 -= last3[last3zeiger] * last3[last3zeiger + 1];

                        setLast3zeiger(false);
                        if (!(last3[last3zeiger + 2] == 111))
                            dummy2 -= last3[last3zeiger] * last3[last3zeiger + 1];

                        score.setText(String.valueOf(dummy2));
                        setLast3zeiger(true);
                        setLast3zeiger(true);
                        break;
          */
                    }
                }
                eliminationsvorschlagsetzen();
                pfeile.setText(Integer.toString(Integer.parseInt(pfeile.getText().toString()) + 3 - xdart));
                fdummy = Integer.parseInt(score.getText().toString());
                if (Integer.parseInt(pfeile.getText().toString()) < 3)
                    durchschnitt.setText(String.valueOf("0"));
                else
                    durchschnitt.setText(formater.format((fdummy / Integer.parseInt(pfeile.getText().toString()) * 3)));

         //alte version
         //       setLast3zeiger(true);
                nextplayer_with_freeze();
            }
            //spieler gewinnt!
            else if ((dummy == maxPunkte && !doubleout) || (dummy == maxPunkte && doubleout && faktor == 2) || (dummy == maxPunkte && masterout && faktor == 3)) {
                letzterwurf = true;
                Toast.makeText(getApplicationContext(), rundenname + " " + getResources().getString(R.string.gewinnt), Toast.LENGTH_SHORT).show();
                switch (xdart) {
                    case 0:
                        d1.setText(dot + dart);
                        break;
                    case 1:
                        d2.setText(dot + dart);
                        break;
                    case 2:
                        d3.setText(dot + dart);
                        break;
                }

                pfeile.setText(Integer.toString(Integer.parseInt(pfeile.getText().toString()) + 1));
                score.setText(String.valueOf(dummy));
                eliminationsvorschlagsetzen();
                fdummy = Integer.parseInt(score.getText().toString());
                if (Integer.parseInt(pfeile.getText().toString()) < 3)
                    durchschnitt.setText("0");
                else
                    durchschnitt.setText(formater.format((fdummy / Integer.parseInt(pfeile.getText().toString()) * 3)));
                // ergebnisübersicht aufrufen - gewinnerreihenfolge bestimmen
                pfeil aktuellerWurf = new pfeil();
                aktuellerWurf.faktor=faktor;
                aktuellerWurf.zahl=dart;
                aktuellerWurf.spielerindex=aktiverSpieler;
                aktuellerWurf.addpunkte=faktor*dart;                      //keine Punkte zum addieren, weil überworfen wurde
                wuerfe.add(aktuellerWurf);
                aktuellerWurf=null;
                eliminate(dummy,rundenname,wuerfe.size()-1);

                // nextplayerx
          /* alte version
                last3[last3zeiger] = faktor;
                last3[last3zeiger + 1] = dart;
                last3[last3zeiger + 2] = 0;
                setLast3zeiger(true);
*/
                spielzeit = (System.currentTimeMillis() - startTime) / 1000;
                Intent intent = new Intent(elimination.this, spielende.class);
                intent.putExtra("anzahl", spieleranzahl);
                intent.putExtra("erster", rundenname);
                intent.putExtra("ersterschnitt", Float.valueOf(durchschnitt.getText().toString()));
                intent.putExtra("ersterpfeile", Integer.parseInt(pfeile.getText().toString()));
                intent.putExtra("spielzeit", spielzeit);
                MainActivity.spieler saf, s2, s3, s4, s5, s6, s7, s8;
                boolean changed;
                switch (spieleranzahl) {
                    case 1:
                        break;
                    case 2:
                        if (rundenname.equals(spieler1.spielerName))
                            s2 = spieler2;
                        else
                            s2 = spieler1;
                        intent.putExtra("zweiter", s2.spielerName);
                        intent.putExtra("zweiterschnitt", s2.durchschnitt);
                        intent.putExtra("zweiterpfeile", s2.geworfenePfeile);
                        intent.putExtra("zweiterrest", maxPunkte-s2.score);
                        break;
                    case 3:
                        if (rundenname.equals(spieler1.spielerName) && spieler2.score >= spieler3.score) {
                            s2 = spieler2;
                            s3 = spieler3;
                        } else if (rundenname.equals(spieler1.spielerName) && spieler2.score < spieler3.score) {
                            s2 = spieler3;
                            s3 = spieler2;
                        } else if (rundenname.equals(spieler2.spielerName) && spieler1.score >= spieler3.score) {
                            s2 = spieler1;
                            s3 = spieler3;
                        } else if (rundenname.equals(spieler2.spielerName) && spieler1.score < spieler3.score) {
                            s2 = spieler3;
                            s3 = spieler1;
                        } else if (rundenname.equals(spieler3.spielerName) && spieler1.score >= spieler2.score) {
                            s2 = spieler1;
                            s3 = spieler2;
                        } else // if (rundenname.equals(spieler3.spielerName) && spieler1.score < spieler2.score)
                        {
                            s2 = spieler2;
                            s3 = spieler1;
                        }
                        intent.putExtra("zweiter", s2.spielerName);
                        intent.putExtra("zweiterschnitt", s2.durchschnitt);
                        intent.putExtra("zweiterpfeile", s2.geworfenePfeile);
                        intent.putExtra("zweiterrest", maxPunkte-s2.score);
                        intent.putExtra("dritter", s3.spielerName);
                        intent.putExtra("dritterschnitt", s3.durchschnitt);
                        intent.putExtra("dritterpfeile", s3.geworfenePfeile);
                        intent.putExtra("dritterrest", maxPunkte-s3.score);
                        break;

                    case 4:
                        if (rundenname.equals(spieler1.spielerName)) {
                            s2 = spieler2;
                            s3 = spieler3;
                            s4 = spieler4;
                        } else if (rundenname.equals(spieler2.spielerName)) {
                            s2 = spieler1;
                            s3 = spieler3;
                            s4 = spieler4;
                        } else if (rundenname.equals(spieler3.spielerName)) {
                            s2 = spieler1;
                            s3 = spieler2;
                            s4 = spieler4;
                        } else {
                            s2 = spieler1;
                            s3 = spieler2;
                            s4 = spieler3;
                        }

                        changed = true;
                        while (changed)             //kleines bubblesort für die spielerreihenfolge nach punkten
                        {
                            changed = false;
                            if (s2.score < s3.score) {
                                saf = s2;
                                s2 = s3;
                                s3 = saf;
                                changed = true;
                            }
                            if (s3.score < s4.score) {
                                saf = s3;
                                s3 = s4;
                                s4 = saf;
                                changed = true;
                            }
                        }

                        intent.putExtra("zweiter", s2.spielerName);
                        intent.putExtra("zweiterschnitt", s2.durchschnitt);
                        intent.putExtra("zweiterpfeile", s2.geworfenePfeile);
                        intent.putExtra("zweiterrest", maxPunkte-s2.score);
                        intent.putExtra("dritter", s3.spielerName);
                        intent.putExtra("dritterschnitt", s3.durchschnitt);
                        intent.putExtra("dritterpfeile", s3.geworfenePfeile);
                        intent.putExtra("dritterrest", maxPunkte-s3.score);
                        intent.putExtra("vierter", s4.spielerName);
                        intent.putExtra("vierterschnitt", s4.durchschnitt);
                        intent.putExtra("vierterpfeile", s4.geworfenePfeile);
                        intent.putExtra("vierterrest", maxPunkte-s4.score);
                        break;
                    case 5:
                        if (rundenname.equals(spieler2.spielerName)) s2 = spieler1;
                        else s2 = spieler2;
                        if (rundenname.equals(spieler3.spielerName)) s3 = spieler1;
                        else s3 = spieler3;
                        if (rundenname.equals(spieler4.spielerName)) s4 = spieler1;
                        else s4 = spieler4;
                        if (rundenname.equals(spieler5.spielerName)) s5 = spieler1;
                        else s5 = spieler5;
                        changed = true;
                        while (changed)             //kleines bubblesort für die spielerreihenfolge nach punkten
                        {
                            changed = false;
                            if (s2.score < s3.score) {
                                saf = s2;
                                s2 = s3;
                                s3 = saf;
                                changed = true;
                            }
                            if (s3.score < s4.score) {
                                saf = s3;
                                s3 = s4;
                                s4 = saf;
                                changed = true;
                            }
                            if (s4.score < s5.score) {
                                saf = s4;
                                s4 = s5;
                                s5 = saf;
                                changed = true;
                            }
                        }
                        intent.putExtra("zweiter", s2.spielerName);
                        intent.putExtra("zweiterschnitt", s2.durchschnitt);
                        intent.putExtra("zweiterpfeile", s2.geworfenePfeile);
                        intent.putExtra("zweiterrest", maxPunkte-s2.score);
                        intent.putExtra("dritter", s3.spielerName);
                        intent.putExtra("dritterschnitt", s3.durchschnitt);
                        intent.putExtra("dritterpfeile", s3.geworfenePfeile);
                        intent.putExtra("dritterrest", maxPunkte-s3.score);
                        intent.putExtra("vierter", s4.spielerName);
                        intent.putExtra("vierterschnitt", s4.durchschnitt);
                        intent.putExtra("vierterpfeile", s4.geworfenePfeile);
                        intent.putExtra("vierterrest", maxPunkte-s4.score);
                        intent.putExtra("fuenfter", s5.spielerName);
                        intent.putExtra("fuenfterschnitt", s5.durchschnitt);
                        intent.putExtra("fuenfterpfeile", s5.geworfenePfeile);
                        intent.putExtra("fuenfterrest", maxPunkte-s5.score);
                        break;
                    case 6:
                        if (rundenname.equals(spieler2.spielerName)) s2 = spieler1;
                        else s2 = spieler2;
                        if (rundenname.equals(spieler3.spielerName)) s3 = spieler1;
                        else s3 = spieler3;
                        if (rundenname.equals(spieler4.spielerName)) s4 = spieler1;
                        else s4 = spieler4;
                        if (rundenname.equals(spieler5.spielerName)) s5 = spieler1;
                        else s5 = spieler5;
                        if (rundenname.equals(spieler6.spielerName)) s6 = spieler1;
                        else s6 = spieler6;

                        changed = true;
                        while (changed)             //kleines bubblesort für die spielerreihenfolge nach punkten
                        {
                            changed = false;
                            if (s2.score < s3.score) {
                                saf = s2;
                                s2 = s3;
                                s3 = saf;
                                changed = true;
                            }
                            if (s3.score < s4.score) {
                                saf = s3;
                                s3 = s4;
                                s4 = saf;
                                changed = true;
                            }
                            if (s4.score < s5.score) {
                                saf = s4;
                                s4 = s5;
                                s5 = saf;
                                changed = true;
                            }
                            if (s5.score < s6.score) {
                                saf = s5;
                                s5 = s6;
                                s6 = saf;
                                changed = true;
                            }
                        }
                        intent.putExtra("zweiter", s2.spielerName);
                        intent.putExtra("zweiterschnitt", s2.durchschnitt);
                        intent.putExtra("zweiterpfeile", s2.geworfenePfeile);
                        intent.putExtra("zweiterrest", maxPunkte-s2.score);
                        intent.putExtra("dritter", s3.spielerName);
                        intent.putExtra("dritterschnitt", s3.durchschnitt);
                        intent.putExtra("dritterpfeile", s3.geworfenePfeile);
                        intent.putExtra("dritterrest", maxPunkte-s3.score);
                        intent.putExtra("vierter", s4.spielerName);
                        intent.putExtra("vierterschnitt", s4.durchschnitt);
                        intent.putExtra("vierterpfeile", s4.geworfenePfeile);
                        intent.putExtra("vierterrest", maxPunkte-s4.score);
                        intent.putExtra("fuenfter", s5.spielerName);
                        intent.putExtra("fuenfterschnitt", s5.durchschnitt);
                        intent.putExtra("fuenfterpfeile", s5.geworfenePfeile);
                        intent.putExtra("fuenfterrest", maxPunkte-s5.score);
                        intent.putExtra("sechster", s6.spielerName);
                        intent.putExtra("sechsterschnitt", s6.durchschnitt);
                        intent.putExtra("sechsterpfeile", s6.geworfenePfeile);
                        intent.putExtra("sechsterrest", maxPunkte-s6.score);
                        break;
                    case 7:
                        if (rundenname.equals(spieler2.spielerName)) s2 = spieler1;
                        else s2 = spieler2;
                        if (rundenname.equals(spieler3.spielerName)) s3 = spieler1;
                        else s3 = spieler3;
                        if (rundenname.equals(spieler4.spielerName)) s4 = spieler1;
                        else s4 = spieler4;
                        if (rundenname.equals(spieler5.spielerName)) s5 = spieler1;
                        else s5 = spieler5;
                        if (rundenname.equals(spieler6.spielerName)) s6 = spieler1;
                        else s6 = spieler6;
                        if (rundenname.equals(spieler7.spielerName)) s7 = spieler1;
                        else s7 = spieler7;

                        changed = true;
                        while (changed)             //kleines bubblesort für die spielerreihenfolge nach punkten
                        {
                            changed = false;
                            if (s2.score < s3.score) {
                                saf = s2;
                                s2 = s3;
                                s3 = saf;
                                changed = true;
                            }
                            if (s3.score < s4.score) {
                                saf = s3;
                                s3 = s4;
                                s4 = saf;
                                changed = true;
                            }
                            if (s4.score < s5.score) {
                                saf = s4;
                                s4 = s5;
                                s5 = saf;
                                changed = true;
                            }
                            if (s5.score < s6.score) {
                                saf = s5;
                                s5 = s6;
                                s6 = saf;
                                changed = true;
                            }
                            if (s6.score < s7.score) {
                                saf = s6;
                                s6 = s7;
                                s7 = saf;
                                changed = true;
                            }
                        }
                        intent.putExtra("zweiter", s2.spielerName);
                        intent.putExtra("zweiterschnitt", s2.durchschnitt);
                        intent.putExtra("zweiterpfeile", s2.geworfenePfeile);
                        intent.putExtra("zweiterrest", maxPunkte-s2.score);
                        intent.putExtra("dritter", s3.spielerName);
                        intent.putExtra("dritterschnitt", s3.durchschnitt);
                        intent.putExtra("dritterpfeile", s3.geworfenePfeile);
                        intent.putExtra("dritterrest", maxPunkte-s3.score);
                        intent.putExtra("vierter", s4.spielerName);
                        intent.putExtra("vierterschnitt", s4.durchschnitt);
                        intent.putExtra("vierterpfeile", s4.geworfenePfeile);
                        intent.putExtra("vierterrest", maxPunkte-s4.score);
                        intent.putExtra("fuenfter", s5.spielerName);
                        intent.putExtra("fuenfterschnitt", s5.durchschnitt);
                        intent.putExtra("fuenfterpfeile", s5.geworfenePfeile);
                        intent.putExtra("fuenfterrest", maxPunkte-s5.score);
                        intent.putExtra("sechster", s6.spielerName);
                        intent.putExtra("sechsterschnitt", s6.durchschnitt);
                        intent.putExtra("sechsterpfeile", s6.geworfenePfeile);
                        intent.putExtra("sechsterrest", maxPunkte-s6.score);
                        intent.putExtra("siebenter", s7.spielerName);
                        intent.putExtra("siebenterschnitt", s7.durchschnitt);
                        intent.putExtra("siebenterpfeile", s7.geworfenePfeile);
                        intent.putExtra("siebenterrest", maxPunkte-s7.score);
                        break;
                    case 8:
                        if (rundenname.equals(spieler2.spielerName)) s2 = spieler1;
                        else s2 = spieler2;
                        if (rundenname.equals(spieler3.spielerName)) s3 = spieler1;
                        else s3 = spieler3;
                        if (rundenname.equals(spieler4.spielerName)) s4 = spieler1;
                        else s4 = spieler4;
                        if (rundenname.equals(spieler5.spielerName)) s5 = spieler1;
                        else s5 = spieler5;
                        if (rundenname.equals(spieler6.spielerName)) s6 = spieler1;
                        else s6 = spieler6;
                        if (rundenname.equals(spieler7.spielerName)) s7 = spieler1;
                        else s7 = spieler7;
                        if (rundenname.equals(spieler8.spielerName)) s8 = spieler1;
                        else s8 = spieler8;

                        changed = true;
                        while (changed)             //kleines bubblesort für die spielerreihenfolge nach punkten
                        {
                            changed = false;
                            if (s2.score < s3.score) {
                                saf = s2;
                                s2 = s3;
                                s3 = saf;
                                changed = true;
                            }
                            if (s3.score < s4.score) {
                                saf = s3;
                                s3 = s4;
                                s4 = saf;
                                changed = true;
                            }
                            if (s4.score < s5.score) {
                                saf = s4;
                                s4 = s5;
                                s5 = saf;
                                changed = true;
                            }
                            if (s5.score < s6.score) {
                                saf = s5;
                                s5 = s6;
                                s6 = saf;
                                changed = true;
                            }
                            if (s6.score < s7.score) {
                                saf = s6;
                                s6 = s7;
                                s7 = saf;
                                changed = true;
                            }
                            if (s7.score < s8.score) {
                                saf = s7;
                                s7 = s8;
                                s8 = saf;
                                changed = true;
                            }
                        }
                        intent.putExtra("zweiter", s2.spielerName);
                        intent.putExtra("zweiterschnitt", s2.durchschnitt);
                        intent.putExtra("zweiterpfeile", s2.geworfenePfeile);
                        intent.putExtra("zweiterrest", maxPunkte-s2.score);
                        intent.putExtra("dritter", s3.spielerName);
                        intent.putExtra("dritterschnitt", s3.durchschnitt);
                        intent.putExtra("dritterpfeile", s3.geworfenePfeile);
                        intent.putExtra("dritterrest", maxPunkte-s3.score);
                        intent.putExtra("vierter", s4.spielerName);
                        intent.putExtra("vierterschnitt", s4.durchschnitt);
                        intent.putExtra("vierterpfeile", s4.geworfenePfeile);
                        intent.putExtra("vierterrest", maxPunkte-s4.score);
                        intent.putExtra("fuenfter", s5.spielerName);
                        intent.putExtra("fuenfterschnitt", s5.durchschnitt);
                        intent.putExtra("fuenfterpfeile", s5.geworfenePfeile);
                        intent.putExtra("fuenfterrest", maxPunkte-s5.score);
                        intent.putExtra("sechster", s6.spielerName);
                        intent.putExtra("sechsterschnitt", s6.durchschnitt);
                        intent.putExtra("sechsterpfeile", s6.geworfenePfeile);
                        intent.putExtra("sechsterrest", maxPunkte-s6.score);
                        intent.putExtra("siebenter", s7.spielerName);
                        intent.putExtra("siebenterschnitt", s7.durchschnitt);
                        intent.putExtra("siebenterpfeile", s7.geworfenePfeile);
                        intent.putExtra("siebenterrest", maxPunkte-s7.score);
                        intent.putExtra("achter", s8.spielerName);
                        intent.putExtra("achterschnitt", s8.durchschnitt);
                        intent.putExtra("achterpfeile", s8.geworfenePfeile);
                        intent.putExtra("achterrest", maxPunkte-s8.score);
                        break;
                }
                spielerwertespeichern();
                startActivity(intent);
                finish();

            }

            //regelfall!
            else {
                letzterwurf = false;
                //vorschläge?!
                vorschlagsetzen(maxPunkte-dummy);
                ergebnis = dummy;
                score.setText(Integer.toString(ergebnis));
                eliminationsvorschlagsetzen();
                pfeile.setText(Integer.toString(Integer.parseInt(pfeile.getText().toString()) + 1));
                switch (xdart) {
                    case 0: {
                        d1.setText(dot + dart);
                        break;
                    }
                    case 1: {
                        d2.setText(dot + dart);
                        break;
                    }
                    case 2: {
                        d3.setText(dot + dart);
                        fdummy = ergebnis;
                        durchschnitt.setText(formater.format((fdummy / Integer.parseInt(pfeile.getText().toString()) * 3)));
                        break;
                    }
                }

                //eingabe sichern für undo
                pfeil aktuellerWurf = new pfeil();
                aktuellerWurf.faktor=faktor;
                aktuellerWurf.zahl=dart;
                aktuellerWurf.spielerindex=aktiverSpieler;
                aktuellerWurf.addpunkte=faktor*dart;
                wuerfe.add(aktuellerWurf);
                aktuellerWurf=null;
                eliminate(dummy,rundenname,wuerfe.size()-1);


           /*alte version
                last3[last3zeiger] = faktor;
                last3[last3zeiger + 1] = dart;
                last3[last3zeiger + 2] = 0;
                //speicherzeiger auf nächstes,"leeres" feld setzen
                setLast3zeiger(true);

            */
                if (xdart < 2) xdart++;
                else {
                  nextplayer_with_freeze();
                     }
            }
        }

    };

    private void undo_eliminations(){
        if ((eliminations.size()<=0) || (wuerfe.size()<=0)) return;
        // solange es eine elimination mit dem aktuellen wurfindex gibt, eliminierung rückgängig machen
        while ((eliminations.get(eliminations.size()-1).wurfindex == wuerfe.size()-1)) {
            // if eliminated, restore score
            switch (spieleranzahl) {
                case 8:
                    if (rundenname != spieler8.spielerName && (eliminations.get(eliminations.size() - 1).spielername == spieler8.spielerName)) {
                        spieler8.score = eliminations.get(eliminations.size() - 1).score_eliminated;
                        spieler8.durchschnitt = spieler8.score / (spieler8.geworfenePfeile * 3);
                        if (listenName1.getText().toString().equals(spieler8.spielerName))
                            listenPunkte1.setText(spieler8.score + "  Ø: " + spieler8.durchschnitt);
                        else if (listenName2.getText().toString().equals(spieler8.spielerName))
                            listenPunkte2.setText(spieler8.score + "  Ø: " + spieler8.durchschnitt);
                        else if (listenName3.getText().toString().equals(spieler8.spielerName))
                            listenPunkte3.setText(spieler8.score + "  Ø: " + spieler8.durchschnitt);
                        else if (listenName4.getText().toString().equals(spieler8.spielerName))
                            listenPunkte4.setText(spieler8.score + "  Ø: " + spieler8.durchschnitt);
                        else if (listenName5.getText().toString().equals(spieler8.spielerName))
                            listenPunkte5.setText(spieler8.score + "  Ø: " + spieler8.durchschnitt);
                        else if (listenName6.getText().toString().equals(spieler8.spielerName))
                            listenPunkte6.setText(spieler8.score + "  Ø: " + spieler8.durchschnitt);
                        else
                            listenPunkte7.setText(spieler8.score + "  Ø: " + spieler8.durchschnitt);
                    }
                case 7:
                    if (rundenname != spieler7.spielerName && (eliminations.get(eliminations.size() - 1).spielername == spieler7.spielerName)) {
                        spieler7.score = eliminations.get(eliminations.size() - 1).score_eliminated;
                        spieler7.durchschnitt = spieler7.score / (spieler7.geworfenePfeile * 3);
                        if (listenName1.getText().toString().equals(spieler7.spielerName))
                            listenPunkte1.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                        else if (listenName2.getText().toString().equals(spieler7.spielerName))
                            listenPunkte2.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                        else if (listenName3.getText().toString().equals(spieler7.spielerName))
                            listenPunkte3.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                        else if (listenName4.getText().toString().equals(spieler7.spielerName))
                            listenPunkte4.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                        else if (listenName5.getText().toString().equals(spieler7.spielerName))
                            listenPunkte5.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                        else if (listenName6.getText().toString().equals(spieler7.spielerName))
                            listenPunkte6.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                        else
                            listenPunkte7.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    }
                case 6:
                    if (rundenname != spieler6.spielerName && (eliminations.get(eliminations.size() - 1).spielername == spieler6.spielerName)) {
                        spieler6.score = eliminations.get(eliminations.size() - 1).score_eliminated;
                        spieler6.durchschnitt = spieler6.score / (spieler6.geworfenePfeile * 3);
                        if (listenName1.getText().toString().equals(spieler6.spielerName))
                            listenPunkte1.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                        else if (listenName2.getText().toString().equals(spieler6.spielerName))
                            listenPunkte2.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                        else if (listenName3.getText().toString().equals(spieler6.spielerName))
                            listenPunkte3.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                        else if (listenName4.getText().toString().equals(spieler6.spielerName))
                            listenPunkte4.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                        else if (listenName5.getText().toString().equals(spieler6.spielerName))
                            listenPunkte5.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                        else if (listenName6.getText().toString().equals(spieler7.spielerName))
                            listenPunkte6.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                        else
                            listenPunkte7.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    }                   
                case 5:
                    if (rundenname != spieler5.spielerName && (eliminations.get(eliminations.size() - 1).spielername == spieler5.spielerName)) {
                        spieler5.score = eliminations.get(eliminations.size() - 1).score_eliminated;
                        spieler5.durchschnitt = spieler5.score / (spieler5.geworfenePfeile * 3);
                        if (listenName1.getText().toString().equals(spieler5.spielerName))
                            listenPunkte1.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                        else if (listenName2.getText().toString().equals(spieler5.spielerName))
                            listenPunkte2.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                        else if (listenName3.getText().toString().equals(spieler5.spielerName))
                            listenPunkte3.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                        else if (listenName4.getText().toString().equals(spieler5.spielerName))
                            listenPunkte4.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                        else if (listenName5.getText().toString().equals(spieler5.spielerName))
                            listenPunkte5.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                        else if (listenName6.getText().toString().equals(spieler5.spielerName))
                            listenPunkte6.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                        else
                            listenPunkte7.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    }
                case 4:
                    if (rundenname != spieler4.spielerName && (eliminations.get(eliminations.size() - 1).spielername == spieler4.spielerName)) {
                        spieler4.score = eliminations.get(eliminations.size() - 1).score_eliminated;
                        spieler4.durchschnitt = spieler4.score / (spieler4.geworfenePfeile * 3);
                        if (listenName1.getText().toString().equals(spieler4.spielerName))
                            listenPunkte1.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                        else if (listenName2.getText().toString().equals(spieler4.spielerName))
                            listenPunkte2.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                        else if (listenName3.getText().toString().equals(spieler4.spielerName))
                            listenPunkte3.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                        else if (listenName4.getText().toString().equals(spieler4.spielerName))
                            listenPunkte4.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                        else if (listenName5.getText().toString().equals(spieler4.spielerName))
                            listenPunkte5.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                        else if (listenName6.getText().toString().equals(spieler4.spielerName))
                            listenPunkte6.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                        else
                            listenPunkte7.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    }
                case 3:
                    if (rundenname != spieler3.spielerName && (eliminations.get(eliminations.size() - 1).spielername == spieler3.spielerName)) {
                        spieler3.score = eliminations.get(eliminations.size() - 1).score_eliminated;
                        spieler3.durchschnitt = spieler3.score / (spieler3.geworfenePfeile * 3);
                        if (listenName1.getText().toString().equals(spieler3.spielerName))
                            listenPunkte1.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                        else if (listenName2.getText().toString().equals(spieler3.spielerName))
                            listenPunkte2.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                        else if (listenName3.getText().toString().equals(spieler3.spielerName))
                            listenPunkte3.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                        else if (listenName4.getText().toString().equals(spieler3.spielerName))
                            listenPunkte4.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                        else if (listenName5.getText().toString().equals(spieler3.spielerName))
                            listenPunkte5.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                        else if (listenName6.getText().toString().equals(spieler3.spielerName))
                            listenPunkte6.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                        else
                            listenPunkte7.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    }
                case 2:
                    if (rundenname != spieler2.spielerName && (eliminations.get(eliminations.size() - 1).spielername == spieler2.spielerName)) {
                        spieler2.score = eliminations.get(eliminations.size() - 1).score_eliminated;
                        spieler2.durchschnitt = spieler2.score / (spieler2.geworfenePfeile * 3);
                        if (listenName1.getText().toString().equals(spieler2.spielerName))
                            listenPunkte1.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                        else if (listenName2.getText().toString().equals(spieler2.spielerName))
                            listenPunkte2.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                        else if (listenName3.getText().toString().equals(spieler2.spielerName))
                            listenPunkte3.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                        else if (listenName4.getText().toString().equals(spieler2.spielerName))
                            listenPunkte4.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                        else if (listenName5.getText().toString().equals(spieler2.spielerName))
                            listenPunkte5.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                        else if (listenName6.getText().toString().equals(spieler2.spielerName))
                            listenPunkte6.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                        else
                            listenPunkte7.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    }
                case 1:
                    if (rundenname != spieler1.spielerName && (eliminations.get(eliminations.size() - 1).spielername == spieler1.spielerName)) {
                        spieler1.score = eliminations.get(eliminations.size() - 1).score_eliminated;
                        spieler1.durchschnitt = spieler1.score / (spieler1.geworfenePfeile * 3);
                        if (listenName1.getText().toString().equals(spieler1.spielerName))
                            listenPunkte1.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                        else if (listenName2.getText().toString().equals(spieler1.spielerName))
                            listenPunkte2.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                        else if (listenName3.getText().toString().equals(spieler1.spielerName))
                            listenPunkte3.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                        else if (listenName4.getText().toString().equals(spieler1.spielerName))
                            listenPunkte4.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                        else if (listenName5.getText().toString().equals(spieler1.spielerName))
                            listenPunkte5.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                        else if (listenName6.getText().toString().equals(spieler1.spielerName))
                            listenPunkte6.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                        else
                            listenPunkte7.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    }
            }
            eliminations.remove(eliminations.size()-1);
            if (eliminations.size() == 0) return;
        }
    }

    private void eliminate(int dummy, String rundenname,int eliminert_at_wurfindex) {
        // eliminates other player, if hit and set listenpunkte
        eliminationscore elimination_undo_save = new eliminationscore();
            elimination_undo_save.wurfindex = eliminert_at_wurfindex;//wuerfe.size()-1;
        switch (spieleranzahl) {
            case 1:
                break;
            case 2:
                if ((rundenname != spieler1.spielerName) && (dummy==spieler1.score)) {
                    elimination_undo_save.score_eliminated=spieler1.score;
                    elimination_undo_save.spielername=spieler1.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler1.score=0;
                    spieler1.durchschnitt=0;
                    Toast.makeText(getApplicationContext(), spieler1.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    listenPunkte1.setText("0  Ø: 0");
                }
                if ((rundenname != spieler2.spielerName) && (dummy==spieler2.score)) {
                    elimination_undo_save.score_eliminated=spieler2.score;
                    elimination_undo_save.spielername=spieler2.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler2.score=0;
                    spieler2.durchschnitt=0;
                    Toast.makeText(getApplicationContext(), spieler2.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    listenPunkte1.setText("0  Ø: 0");
                }
                break;
            case 3:
                if ((rundenname != spieler1.spielerName) && (dummy==spieler1.score)) {
                    elimination_undo_save.score_eliminated=spieler1.score;
                    elimination_undo_save.spielername=spieler1.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler1.durchschnitt=0;
                    spieler1.score=0;
                    Toast.makeText(getApplicationContext(), spieler1.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler1.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else {
                        listenPunkte2.setText("0  Ø: 0");
                    }
                }
                if ((rundenname != spieler2.spielerName) && (dummy==spieler2.score)) {
                    elimination_undo_save.score_eliminated=spieler2.score;
                    elimination_undo_save.spielername=spieler2.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler2.durchschnitt=0;
                    spieler2.score=0;
                    Toast.makeText(getApplicationContext(), spieler2.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler2.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else {
                        listenPunkte2.setText("0  Ø: 0");
                    }
                }
                if ((rundenname != spieler3.spielerName) && (dummy==spieler3.score)) {
                    elimination_undo_save.score_eliminated=spieler3.score;
                    elimination_undo_save.spielername=spieler3.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler3.durchschnitt=0;
                    spieler3.score=0;
                    Toast.makeText(getApplicationContext(), spieler3.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler3.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else {
                        listenPunkte2.setText("0  Ø: 0");
                    }
                }
                break;
            case 4:
                if ((rundenname != spieler1.spielerName) && (dummy==spieler1.score)) {
                    elimination_undo_save.score_eliminated=spieler1.score;
                    elimination_undo_save.spielername=spieler1.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler1.durchschnitt=0;
                    spieler1.score=0;
                    Toast.makeText(getApplicationContext(), spieler1.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler1.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else if (listenName2.getText().toString().equals(spieler1.spielerName)) {
                        listenPunkte2.setText("0  Ø: 0");
                    }
                    else {
                        listenPunkte3.setText("0  Ø: 0");
                    }
                }
                if ((rundenname != spieler2.spielerName) && (dummy==spieler2.score)) {
                    elimination_undo_save.score_eliminated=spieler2.score;
                    elimination_undo_save.spielername=spieler2.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler2.durchschnitt=0;
                    spieler2.score=0;
                    Toast.makeText(getApplicationContext(), spieler2.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler2.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else if (listenName2.getText().toString().equals(spieler2.spielerName)) {
                        listenPunkte2.setText("0  Ø: 0");
                    }
                    else {
                        listenPunkte3.setText("0  Ø: 0");
                    }
                }
                if ((rundenname != spieler3.spielerName) && (dummy==spieler3.score)) {
                    elimination_undo_save.score_eliminated=spieler3.score;
                    elimination_undo_save.spielername=spieler3.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler3.durchschnitt=0;
                    spieler3.score=0;
                    Toast.makeText(getApplicationContext(), spieler3.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler3.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else if (listenName2.getText().toString().equals(spieler3.spielerName)) {
                        listenPunkte2.setText("0  Ø: 0");
                    }
                    else {
                        listenPunkte3.setText("0  Ø: 0");
                    }
                }
                if ((rundenname != spieler4.spielerName) && (dummy==spieler4.score)) {
                    elimination_undo_save.score_eliminated=spieler4.score;
                    elimination_undo_save.spielername=spieler4.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler4.durchschnitt=0;
                    spieler4.score=0;
                    Toast.makeText(getApplicationContext(), spieler4.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler4.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else if (listenName2.getText().toString().equals(spieler4.spielerName)) {
                        listenPunkte2.setText("0  Ø: 0");
                    }
                    else {
                        listenPunkte3.setText("0  Ø: 0");
                    }
                }
                break;
            case 5:
                if ((rundenname != spieler1.spielerName) && (dummy==spieler1.score)) {
                    elimination_undo_save.score_eliminated=spieler1.score;
                    elimination_undo_save.spielername=spieler1.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler1.durchschnitt=0;
                    spieler1.score=0;
                    Toast.makeText(getApplicationContext(), spieler1.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler1.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else if (listenName2.getText().toString().equals(spieler1.spielerName)) {
                        listenPunkte2.setText("0  Ø: 0");
                    }
                    else if (listenName3.getText().toString().equals(spieler1.spielerName)) {
                        listenPunkte3.setText("0  Ø: 0");
                    }
                    else {
                        listenPunkte4.setText("0  Ø: 0");
                    }
                }
                if ((rundenname != spieler2.spielerName) && (dummy==spieler2.score)) {
                    elimination_undo_save.score_eliminated=spieler2.score;
                    elimination_undo_save.spielername=spieler2.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler2.durchschnitt=0;
                    spieler2.score=0;
                    Toast.makeText(getApplicationContext(), spieler2.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler2.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else if (listenName2.getText().toString().equals(spieler2.spielerName)) {
                        listenPunkte2.setText("0  Ø: 0");
                    }
                    else if (listenName3.getText().toString().equals(spieler2.spielerName)) {
                        listenPunkte3.setText("0  Ø: 0");
                    }
                    else {
                        listenPunkte4.setText("0  Ø: 0");
                    }
                }
                if ((rundenname != spieler3.spielerName) && (dummy==spieler3.score)) {
                    elimination_undo_save.score_eliminated=spieler3.score;
                    elimination_undo_save.spielername=spieler3.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler3.durchschnitt=0;
                    spieler3.score=0;
                    Toast.makeText(getApplicationContext(), spieler3.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler3.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else if (listenName2.getText().toString().equals(spieler3.spielerName)) {
                        listenPunkte2.setText("0  Ø: 0");
                    }
                    else if (listenName3.getText().toString().equals(spieler3.spielerName)) {
                        listenPunkte3.setText("0  Ø: 0");
                    }
                    else {
                        listenPunkte4.setText("0  Ø: 0");
                    }
                }
                if ((rundenname != spieler4.spielerName) && (dummy==spieler4.score)) {
                    elimination_undo_save.score_eliminated=spieler4.score;
                    elimination_undo_save.spielername=spieler4.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler4.durchschnitt=0;
                    spieler4.score=0;
                    Toast.makeText(getApplicationContext(), spieler4.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler4.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else if (listenName2.getText().toString().equals(spieler4.spielerName)) {
                        listenPunkte2.setText("0  Ø: 0");
                    }
                    else if (listenName3.getText().toString().equals(spieler4.spielerName)) {
                        listenPunkte3.setText("0  Ø: 0");
                    }
                    else {
                        listenPunkte4.setText("0  Ø: 0");
                    }
                }
                if ((rundenname != spieler5.spielerName) && (dummy==spieler5.score)) {
                    elimination_undo_save.score_eliminated=spieler5.score;
                    elimination_undo_save.spielername=spieler5.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler5.durchschnitt=0;
                    spieler5.score=0;
                    Toast.makeText(getApplicationContext(), spieler5.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler5.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else if (listenName2.getText().toString().equals(spieler5.spielerName)) {
                        listenPunkte2.setText("0  Ø: 0");
                    }
                    else if (listenName3.getText().toString().equals(spieler5.spielerName)) {
                        listenPunkte3.setText("0  Ø: 0");
                    }
                    else {
                        listenPunkte4.setText("0  Ø: 0");
                    }
                }
                break;
            case 6:
                if ((rundenname != spieler1.spielerName) && (dummy==spieler1.score)) {
                    elimination_undo_save.score_eliminated=spieler1.score;
                    elimination_undo_save.spielername=spieler1.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler1.durchschnitt=0;
                    spieler1.score=0;
                    Toast.makeText(getApplicationContext(), spieler1.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler1.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else if (listenName2.getText().toString().equals(spieler1.spielerName)) {
                        listenPunkte2.setText("0  Ø: 0");
                    }
                    else if (listenName3.getText().toString().equals(spieler1.spielerName)) {
                        listenPunkte3.setText("0  Ø: 0");
                    }
                    else if (listenName4.getText().toString().equals(spieler1.spielerName)) {
                        listenPunkte4.setText("0  Ø: 0");
                    }
                    else {
                        listenPunkte5.setText("0  Ø: 0");
                    }
                }
                if ((rundenname != spieler2.spielerName) && (dummy==spieler2.score)) {
                    elimination_undo_save.score_eliminated=spieler2.score;
                    elimination_undo_save.spielername=spieler2.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler2.durchschnitt=0;
                    spieler2.score=0;
                    Toast.makeText(getApplicationContext(), spieler2.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler2.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else if (listenName2.getText().toString().equals(spieler2.spielerName)) {
                        listenPunkte2.setText("0  Ø: 0");
                    }
                    else if (listenName3.getText().toString().equals(spieler2.spielerName)) {
                        listenPunkte3.setText("0  Ø: 0");
                    }
                    else if (listenName4.getText().toString().equals(spieler2.spielerName)) {
                        listenPunkte4.setText("0  Ø: 0");
                    }
                    else {
                        listenPunkte5.setText("0  Ø: 0");
                    }
                }
                if ((rundenname != spieler3.spielerName) && (dummy==spieler3.score)) {
                    elimination_undo_save.score_eliminated=spieler3.score;
                    elimination_undo_save.spielername=spieler3.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler3.durchschnitt=0;
                    spieler3.score=0;
                    Toast.makeText(getApplicationContext(), spieler3.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler3.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else if (listenName2.getText().toString().equals(spieler3.spielerName)) {
                        listenPunkte2.setText("0  Ø: 0");
                    }
                    else if (listenName3.getText().toString().equals(spieler3.spielerName)) {
                        listenPunkte3.setText("0  Ø: 0");
                    }
                    else if (listenName4.getText().toString().equals(spieler3.spielerName)) {
                        listenPunkte4.setText("0  Ø: 0");
                    }
                    else {
                        listenPunkte5.setText("0  Ø: 0");
                    }
                }
                if ((rundenname != spieler4.spielerName) && (dummy==spieler4.score)) {
                    elimination_undo_save.score_eliminated=spieler4.score;
                    elimination_undo_save.spielername=spieler4.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler4.durchschnitt=0;
                    spieler4.score=0;
                    Toast.makeText(getApplicationContext(), spieler4.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler4.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else if (listenName2.getText().toString().equals(spieler4.spielerName)) {
                        listenPunkte2.setText("0  Ø: 0");
                    }
                    else if (listenName3.getText().toString().equals(spieler4.spielerName)) {
                        listenPunkte3.setText("0  Ø: 0");
                    }
                    else if (listenName4.getText().toString().equals(spieler4.spielerName)) {
                        listenPunkte4.setText("0  Ø: 0");
                    }
                    else {
                        listenPunkte5.setText("0  Ø: 0");
                    }
                }
                if ((rundenname != spieler5.spielerName) && (dummy==spieler5.score)) {
                    elimination_undo_save.score_eliminated=spieler5.score;
                    elimination_undo_save.spielername=spieler5.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler5.durchschnitt=0;
                    spieler5.score=0;
                    Toast.makeText(getApplicationContext(), spieler5.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler5.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else if (listenName2.getText().toString().equals(spieler5.spielerName)) {
                        listenPunkte2.setText("0  Ø: 0");
                    }
                    else if (listenName3.getText().toString().equals(spieler5.spielerName)) {
                        listenPunkte3.setText("0  Ø: 0");
                    }
                    else if (listenName4.getText().toString().equals(spieler5.spielerName)) {
                        listenPunkte4.setText("0  Ø: 0");
                    }
                    else {
                        listenPunkte5.setText("0  Ø: 0");
                    }
                }
                if ((rundenname != spieler6.spielerName) && (dummy==spieler6.score)) {
                    elimination_undo_save.score_eliminated=spieler6.score;
                    elimination_undo_save.spielername=spieler6.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler6.durchschnitt=0;
                    spieler6.score=0;
                    Toast.makeText(getApplicationContext(), spieler6.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler6.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else if (listenName2.getText().toString().equals(spieler6.spielerName)) {
                        listenPunkte2.setText("0  Ø: 0");
                    }
                    else if (listenName3.getText().toString().equals(spieler6.spielerName)) {
                        listenPunkte3.setText("0  Ø: 0");
                    }
                    else if (listenName4.getText().toString().equals(spieler6.spielerName)) {
                        listenPunkte4.setText("0  Ø: 0");
                    }
                    else {
                        listenPunkte5.setText("0  Ø: 0");
                    }
                }
                break;
            case 7:
                if ((rundenname != spieler1.spielerName) && (dummy==spieler1.score)) {
                    elimination_undo_save.score_eliminated=spieler1.score;
                    elimination_undo_save.spielername=spieler1.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler1.durchschnitt=0;
                    spieler1.score=0;
                    Toast.makeText(getApplicationContext(), spieler1.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler1.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else if (listenName2.getText().toString().equals(spieler1.spielerName)) {
                        listenPunkte2.setText("0  Ø: 0");
                    }
                    else if (listenName3.getText().toString().equals(spieler1.spielerName)) {
                        listenPunkte3.setText("0  Ø: 0");
                    }
                    else if (listenName4.getText().toString().equals(spieler1.spielerName)) {
                        listenPunkte4.setText("0  Ø: 0");
                    }
                    else if (listenName5.getText().toString().equals(spieler1.spielerName)) {
                        listenPunkte5.setText("0  Ø: 0");
                    }
                    else {
                        listenPunkte6.setText("0  Ø: 0");
                    }
                }
                if ((rundenname != spieler2.spielerName) && (dummy==spieler2.score)) {
                    elimination_undo_save.score_eliminated=spieler2.score;
                    elimination_undo_save.spielername=spieler2.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler2.durchschnitt=0;
                    spieler2.score=0;
                    Toast.makeText(getApplicationContext(), spieler2.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler2.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else if (listenName2.getText().toString().equals(spieler2.spielerName)) {
                        listenvorschlag2.setVisibility(View.GONE);
                        listenPunkte2.setText("0  Ø: 0");
                    }
                    else if (listenName3.getText().toString().equals(spieler2.spielerName)) {
                        listenPunkte3.setText("0  Ø: 0");
                    }
                    else if (listenName4.getText().toString().equals(spieler2.spielerName)) {
                        listenPunkte4.setText("0  Ø: 0");
                    }
                    else if (listenName5.getText().toString().equals(spieler2.spielerName)) {
                        listenPunkte5.setText("0  Ø: 0");
                    }
                    else {
                        listenPunkte6.setText("0  Ø: 0");
                    }
                }
                if ((rundenname != spieler3.spielerName) && (dummy==spieler3.score)) {
                    elimination_undo_save.score_eliminated=spieler3.score;
                    elimination_undo_save.spielername=spieler3.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undospieler3.durchschnitt=0;
                    spieler3.durchschnitt=0;
                    spieler3.score=0;
                    Toast.makeText(getApplicationContext(), spieler3.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler3.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else if (listenName2.getText().toString().equals(spieler3.spielerName)) {
                        listenPunkte2.setText("0  Ø: 0");
                    }
                    else if (listenName3.getText().toString().equals(spieler3.spielerName)) {
                        listenPunkte3.setText("0  Ø: 0");
                    }
                    else if (listenName4.getText().toString().equals(spieler3.spielerName)) {
                        listenPunkte4.setText("0  Ø: 0");
                    }
                    else if (listenName5.getText().toString().equals(spieler3.spielerName)) {
                        listenPunkte5.setText("0  Ø: 0");
                    }
                    else {
                        listenPunkte6.setText("0  Ø: 0");
                    }
                }
                if ((rundenname != spieler4.spielerName) && (dummy==spieler4.score)) {
                    elimination_undo_save.score_eliminated=spieler4.score;
                    elimination_undo_save.spielername=spieler4.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo spieler4.durchschnitt=0;
                    spieler4.durchschnitt=0;
                    spieler4.score=0;
                    Toast.makeText(getApplicationContext(), spieler4.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler4.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else if (listenName2.getText().toString().equals(spieler4.spielerName)) {
                        listenPunkte2.setText("0  Ø: 0");
                    }
                    else if (listenName3.getText().toString().equals(spieler4.spielerName)) {
                        listenPunkte3.setText("0  Ø: 0");
                    }
                    else if (listenName4.getText().toString().equals(spieler4.spielerName)) {
                        listenPunkte4.setText("0  Ø: 0");
                    }
                    else if (listenName5.getText().toString().equals(spieler4.spielerName)) {
                        listenPunkte5.setText("0  Ø: 0");
                    }
                    else {
                        listenPunkte6.setText("0  Ø: 0");
                    }
                }
                if ((rundenname != spieler5.spielerName) && (dummy==spieler5.score)) {
                    elimination_undo_save.score_eliminated=spieler5.score;
                    elimination_undo_save.spielername=spieler5.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler5.durchschnitt=0;
                    spieler5.score=0;
                    Toast.makeText(getApplicationContext(), spieler5.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler5.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else if (listenName2.getText().toString().equals(spieler5.spielerName)) {
                        listenPunkte2.setText("0  Ø: 0");
                    }
                    else if (listenName3.getText().toString().equals(spieler5.spielerName)) {
                        listenPunkte3.setText("0  Ø: 0");
                    }
                    else if (listenName4.getText().toString().equals(spieler5.spielerName)) {
                        listenPunkte4.setText("0  Ø: 0");
                    }
                    else if (listenName5.getText().toString().equals(spieler5.spielerName)) {
                        listenPunkte5.setText("0  Ø: 0");
                    }
                    else {
                        listenPunkte6.setText("0  Ø: 0");
                    }
                }
                if ((rundenname != spieler6.spielerName) && (dummy==spieler6.score)) {
                    elimination_undo_save.score_eliminated=spieler6.score;
                    elimination_undo_save.spielername=spieler6.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler6.durchschnitt=0;
                    spieler6.score=0;
                    Toast.makeText(getApplicationContext(), spieler6.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler6.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else if (listenName2.getText().toString().equals(spieler6.spielerName)) {
                        listenPunkte2.setText("0  Ø: 0");
                    }
                    else if (listenName3.getText().toString().equals(spieler6.spielerName)) {
                        listenPunkte3.setText("0  Ø: 0");
                    }
                    else if (listenName4.getText().toString().equals(spieler6.spielerName)) {
                        listenPunkte4.setText("0  Ø: 0");
                    }
                    else if (listenName5.getText().toString().equals(spieler6.spielerName)) {
                        listenPunkte5.setText("0  Ø: 0");
                    }
                    else {
                        listenPunkte6.setText("0  Ø: 0");
                    }
                }
                if ((rundenname != spieler7.spielerName) && (dummy==spieler7.score)) {
                    elimination_undo_save.score_eliminated=spieler7.score;
                    elimination_undo_save.spielername=spieler7.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler7.durchschnitt=0;
                    spieler7.score=0;
                    Toast.makeText(getApplicationContext(), spieler7.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler7.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else if (listenName2.getText().toString().equals(spieler7.spielerName)) {
                        listenPunkte2.setText("0  Ø: 0");
                    }
                    else if (listenName3.getText().toString().equals(spieler7.spielerName)) {
                        listenPunkte3.setText("0  Ø: 0");
                    }
                    else if (listenName4.getText().toString().equals(spieler7.spielerName)) {
                        listenPunkte4.setText("0  Ø: 0");
                    }
                    else if (listenName5.getText().toString().equals(spieler7.spielerName)) {
                        listenPunkte5.setText("0  Ø: 0");
                    }
                    else {
                        listenPunkte6.setText("0  Ø: 0");
                    }
                }
                break;
            case 8:
                if ((rundenname != spieler1.spielerName) && (dummy==spieler1.score)) {
                    elimination_undo_save.score_eliminated=spieler1.score;
                    elimination_undo_save.spielername=spieler1.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler1.durchschnitt=0;
                    spieler1.score=0;
                    Toast.makeText(getApplicationContext(), spieler1.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler1.spielerName)) {
                        listenPunkte1.setText("0  Ø: 0");
                    }
                    else if (listenName2.getText().toString().equals(spieler1.spielerName)) listenPunkte2.setText("0  Ø: 0");
                    else if (listenName3.getText().toString().equals(spieler1.spielerName)) listenPunkte3.setText("0  Ø: 0");
                    else if (listenName4.getText().toString().equals(spieler1.spielerName)) listenPunkte4.setText("0  Ø: 0");
                    else if (listenName5.getText().toString().equals(spieler1.spielerName)) listenPunkte5.setText("0  Ø: 0");
                    else if (listenName6.getText().toString().equals(spieler1.spielerName)) listenPunkte6.setText("0  Ø: 0");
                    else listenPunkte7.setText("0  Ø: 0");
                }
                if ((rundenname != spieler2.spielerName) && (dummy==spieler2.score)) {
                    elimination_undo_save.score_eliminated=spieler2.score;
                    elimination_undo_save.spielername=spieler2.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler2.durchschnitt=0;
                    spieler2.score=0;
                    Toast.makeText(getApplicationContext(), spieler2.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler2.spielerName)) listenPunkte1.setText("0  Ø: 0");
                    else if (listenName2.getText().toString().equals(spieler2.spielerName)) listenPunkte2.setText("0  Ø: 0");
                    else if (listenName3.getText().toString().equals(spieler2.spielerName)) listenPunkte3.setText("0  Ø: 0");
                    else if (listenName4.getText().toString().equals(spieler2.spielerName)) listenPunkte4.setText("0  Ø: 0");
                    else if (listenName5.getText().toString().equals(spieler2.spielerName)) listenPunkte5.setText("0  Ø: 0");
                    else if (listenName6.getText().toString().equals(spieler2.spielerName)) listenPunkte6.setText("0  Ø: 0");
                    else listenPunkte7.setText("0  Ø: 0");
                }
                if ((rundenname != spieler3.spielerName) && (dummy==spieler3.score)) {
                    elimination_undo_save.score_eliminated=spieler3.score;
                    elimination_undo_save.spielername=spieler3.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler3.durchschnitt=0;
                    spieler3.score=0;
                    Toast.makeText(getApplicationContext(), spieler3.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler3.spielerName)) listenPunkte1.setText("0  Ø: 0");
                    else if (listenName2.getText().toString().equals(spieler3.spielerName)) listenPunkte2.setText("0  Ø: 0");
                    else if (listenName3.getText().toString().equals(spieler3.spielerName)) listenPunkte3.setText("0  Ø: 0");
                    else if (listenName4.getText().toString().equals(spieler3.spielerName)) listenPunkte4.setText("0  Ø: 0");
                    else if (listenName5.getText().toString().equals(spieler3.spielerName)) listenPunkte5.setText("0  Ø: 0");
                    else if (listenName6.getText().toString().equals(spieler3.spielerName)) listenPunkte6.setText("0  Ø: 0");
                    else listenPunkte7.setText("0  Ø: 0");
                }
                if ((rundenname != spieler4.spielerName) && (dummy==spieler4.score)) {
                    elimination_undo_save.score_eliminated=spieler4.score;
                    elimination_undo_save.spielername=spieler4.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler4.durchschnitt=0;
                    spieler4.score=0;
                    Toast.makeText(getApplicationContext(), spieler4.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler4.spielerName)) listenPunkte1.setText("0  Ø: 0");
                    else if (listenName2.getText().toString().equals(spieler4.spielerName)) listenPunkte2.setText("0  Ø: 0");
                    else if (listenName3.getText().toString().equals(spieler4.spielerName)) listenPunkte3.setText("0  Ø: 0");
                    else if (listenName4.getText().toString().equals(spieler4.spielerName)) listenPunkte4.setText("0  Ø: 0");
                    else if (listenName5.getText().toString().equals(spieler4.spielerName)) listenPunkte5.setText("0  Ø: 0");
                    else if (listenName6.getText().toString().equals(spieler4.spielerName)) listenPunkte6.setText("0  Ø: 0");
                    else listenPunkte7.setText("0  Ø: 0");
                }
                if ((rundenname != spieler5.spielerName) && (dummy==spieler5.score)) {
                    elimination_undo_save.score_eliminated=spieler5.score;
                    elimination_undo_save.spielername=spieler5.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler5.durchschnitt=0;
                    spieler5.score=0;
                    Toast.makeText(getApplicationContext(), spieler5.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler5.spielerName)) listenPunkte1.setText("0  Ø: 0");
                    else if (listenName2.getText().toString().equals(spieler5.spielerName)) listenPunkte2.setText("0  Ø: 0");
                    else if (listenName3.getText().toString().equals(spieler5.spielerName)) listenPunkte3.setText("0  Ø: 0");
                    else if (listenName4.getText().toString().equals(spieler5.spielerName)) listenPunkte4.setText("0  Ø: 0");
                    else if (listenName5.getText().toString().equals(spieler5.spielerName)) listenPunkte5.setText("0  Ø: 0");
                    else if (listenName6.getText().toString().equals(spieler5.spielerName)) listenPunkte6.setText("0  Ø: 0");
                    else listenPunkte7.setText("0  Ø: 0");
                }
                if ((rundenname != spieler6.spielerName) && (dummy==spieler6.score)) {
                    elimination_undo_save.score_eliminated=spieler6.score;
                    elimination_undo_save.spielername=spieler6.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler6.durchschnitt=0;
                    spieler6.score=0;
                    Toast.makeText(getApplicationContext(), spieler6.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler6.spielerName)) listenPunkte1.setText("0  Ø: 0");
                    else if (listenName2.getText().toString().equals(spieler6.spielerName)) listenPunkte2.setText("0  Ø: 0");
                    else if (listenName3.getText().toString().equals(spieler6.spielerName)) listenPunkte3.setText("0  Ø: 0");
                    else if (listenName4.getText().toString().equals(spieler6.spielerName)) listenPunkte4.setText("0  Ø: 0");
                    else if (listenName5.getText().toString().equals(spieler6.spielerName)) listenPunkte5.setText("0  Ø: 0");
                    else if (listenName6.getText().toString().equals(spieler6.spielerName)) listenPunkte6.setText("0  Ø: 0");
                    else listenPunkte7.setText("0  Ø: 0");
                }
                if ((rundenname != spieler7.spielerName) && (dummy==spieler7.score)) {
                    elimination_undo_save.score_eliminated=spieler7.score;
                    elimination_undo_save.spielername=spieler7.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler7.durchschnitt=0;
                    spieler7.score=0;
                    Toast.makeText(getApplicationContext(), spieler7.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler7.spielerName)) listenPunkte1.setText("0  Ø: 0");
                    else if (listenName2.getText().toString().equals(spieler7.spielerName)) listenPunkte2.setText("0  Ø: 0");
                    else if (listenName3.getText().toString().equals(spieler7.spielerName)) listenPunkte3.setText("0  Ø: 0");
                    else if (listenName4.getText().toString().equals(spieler7.spielerName)) listenPunkte4.setText("0  Ø: 0");
                    else if (listenName5.getText().toString().equals(spieler7.spielerName)) listenPunkte5.setText("0  Ø: 0");
                    else if (listenName6.getText().toString().equals(spieler7.spielerName)) listenPunkte6.setText("0  Ø: 0");
                    else listenPunkte7.setText("0  Ø: 0");
                }
                if ((rundenname != spieler8.spielerName) && (dummy==spieler8.score)) {
                    elimination_undo_save.score_eliminated=spieler8.score;
                    elimination_undo_save.spielername=spieler8.spielerName;
                    eliminations.add(elimination_undo_save);    //eliminierung speichern für undo
                    spieler8.durchschnitt=0;
                    spieler8.score=0;
                    Toast.makeText(getApplicationContext(), spieler8.spielerName + " " + getResources().getString(R.string.eliminiert), Toast.LENGTH_SHORT).show();
                    if (listenName1.getText().toString().equals(spieler8.spielerName)) listenPunkte1.setText("0  Ø: 0");
                    else if (listenName2.getText().toString().equals(spieler8.spielerName)) listenPunkte2.setText("0  Ø: 0");
                    else if (listenName3.getText().toString().equals(spieler8.spielerName)) listenPunkte3.setText("0  Ø: 0");
                    else if (listenName4.getText().toString().equals(spieler8.spielerName)) listenPunkte4.setText("0  Ø: 0");
                    else if (listenName5.getText().toString().equals(spieler8.spielerName)) listenPunkte5.setText("0  Ø: 0");
                    else if (listenName6.getText().toString().equals(spieler8.spielerName)) listenPunkte6.setText("0  Ø: 0");
                    else listenPunkte7.setText("0  Ø: 0");
                }
                break;
        }
        if (listenPunkte1.getText().toString().substring(0,1).equals("0")) {
            listenvorschlag1.setText("");
            listenvorschlag1.setVisibility(View.GONE);
        }
        if (listenPunkte2.getText().toString().substring(0,1).equals("0")) {
            listenvorschlag2.setText("");
            listenvorschlag2.setVisibility(View.GONE);
        }
        if (listenPunkte3.getText().toString().substring(0,1).equals("0")) {
            listenvorschlag3.setText("");
            listenvorschlag3.setVisibility(View.GONE);
        }
        if (listenPunkte4.getText().toString().substring(0,1).equals("0")) {
            listenvorschlag4.setText("");
            listenvorschlag4.setVisibility(View.GONE);
        }
        if (listenPunkte5.getText().toString().substring(0,1).equals("0")) {
            listenvorschlag5.setText("");
            listenvorschlag5.setVisibility(View.GONE);
        }
        if (listenPunkte6.getText().toString().substring(0,1).equals("0")) {
            listenvorschlag6.setText("");
            listenvorschlag6.setVisibility(View.GONE);
        }
        if (listenPunkte7.getText().toString().substring(0,1).equals("0")) {
            listenvorschlag7.setText("");
            listenvorschlag7.setVisibility(View.GONE);
        }
    };

    private final View.OnClickListener restdanebenclick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Button daneben=findViewById(R.id.daneben);
            switch (xdart) {
                case 0:
                    daneben.performClick();
                case 1:
                    daneben.performClick();
                case 2:
                    daneben.performClick();
            }
        }
    };


    //sichert die leg ergebnisse für die matchendstatistik
    private void zwischenspeichern(MainActivity.spieler quelle, MainActivity.spieler ziel) {
        ziel.AnzahlSpiele += quelle.AnzahlSpiele;
        ziel.anzahl180 += quelle.anzahl180;
        ziel.anzahluber60 += quelle.anzahluber60;
        ziel.anzahluber100 += quelle.anzahluber100;
        ziel.anzahluber140 += quelle.anzahluber140;
        ziel.geworfenePfeile += quelle.geworfenePfeile;
        ziel.durchschnitt = quelle.durchschnitt;
        if (ziel.besterWurf < quelle.besterWurf) ziel.besterWurf = quelle.besterWurf;
        if (ziel.sets != quelle.sets) ziel.sets = quelle.sets;
        if (quelle.checkoutmax > ziel.checkoutmax) ziel.checkoutmax = quelle.checkoutmax;
        ziel.matcheslost = quelle.matcheslost;
        ziel.matcheswon = quelle.matcheswon;
        ziel.legswon = quelle.legswon;
    }

    private void checkoutinit() {
        checkouts[170][1] = "T20";
        checkouts[170][2] = "T20";
        checkouts[170][3] = "D25";
        checkouts[167][1] = "T20";
        checkouts[167][2] = "T19";
        checkouts[167][3] = "D25";
        checkouts[164][1] = "T20";
        checkouts[164][2] = "T18";
        checkouts[164][3] = "D25";
        checkouts[161][1] = "T20";
        checkouts[161][2] = "T17";
        checkouts[161][3] = "D25";
        checkouts[160][1] = "T20";
        checkouts[160][2] = "T20";
        checkouts[160][3] = "D20";
        checkouts[158][1] = "T20";
        checkouts[158][2] = "T20";
        checkouts[158][3] = "D19";
        checkouts[157][1] = "T20";
        checkouts[157][2] = "T19";
        checkouts[157][3] = "D20";
        checkouts[156][1] = "T20";
        checkouts[156][2] = "T20";
        checkouts[156][3] = "D18";
        checkouts[155][1] = "T20";
        checkouts[155][2] = "T19";
        checkouts[155][3] = "D19";
        checkouts[154][1] = "T20";
        checkouts[154][2] = "T18";
        checkouts[154][3] = "D20";
        checkouts[153][1] = "T20";
        checkouts[153][2] = "T19";
        checkouts[153][3] = "D18";
        checkouts[152][1] = "T20";
        checkouts[152][2] = "T20";
        checkouts[152][3] = "D16";
        checkouts[151][1] = "T20";
        checkouts[151][2] = "T17";
        checkouts[151][3] = "D20";
        checkouts[150][1] = "T20";
        checkouts[150][2] = "T20";
        checkouts[150][3] = "D15";
        checkouts[149][1] = "T20";
        checkouts[149][2] = "T19";
        checkouts[149][3] = "D16";
        checkouts[148][1] = "T18";
        checkouts[148][2] = "T18";
        checkouts[148][3] = "D20";
        checkouts[147][1] = "T20";
        checkouts[147][2] = "T17";
        checkouts[147][3] = "D18";
        checkouts[146][1] = "T20";
        checkouts[146][2] = "T18";
        checkouts[146][3] = "D16";
        checkouts[145][1] = "T20";
        checkouts[145][2] = "T17";
        checkouts[145][3] = "D17";
        checkouts[144][1] = "T19";
        checkouts[144][2] = "T19";
        checkouts[144][3] = "D15";
        checkouts[143][1] = "T20";
        checkouts[143][2] = "T17";
        checkouts[143][3] = "D16";
        checkouts[142][1] = "T20";
        checkouts[142][2] = "T14";
        checkouts[142][3] = "D20";
        checkouts[141][1] = "T20";
        checkouts[141][2] = "T17";
        checkouts[141][3] = "D15";
        checkouts[140][1] = "T18";
        checkouts[140][2] = "T18";
        checkouts[140][3] = "D16";
        checkouts[139][1] = "T19";
        checkouts[139][2] = "D25";
        checkouts[139][3] = "D16";
        checkouts[138][1] = "T20";
        checkouts[138][2] = "D20";
        checkouts[138][3] = "D19";
        checkouts[137][1] = "T19";
        checkouts[137][2] = "D20";
        checkouts[137][3] = "D20";
        checkouts[136][1] = "T20";
        checkouts[136][2] = "D20";
        checkouts[136][3] = "D18";
        checkouts[135][1] = "T19";
        checkouts[135][2] = "D19";
        checkouts[135][3] = "D20";
        checkouts[134][1] = "T17";
        checkouts[134][2] = "T17";
        checkouts[134][3] = "D16";
        checkouts[133][1] = "T17";
        checkouts[133][2] = "D25";
        checkouts[133][3] = "D16";
        checkouts[132][1] = "T20";
        checkouts[132][2] = "D20";
        checkouts[132][3] = "D16";
        checkouts[131][1] = "T17";
        checkouts[131][2] = "T16";
        checkouts[131][3] = "D16";
        checkouts[130][1] = "T20";
        checkouts[130][2] = "D19";
        checkouts[130][3] = "D16";
        checkouts[129][1] = "T19";
        checkouts[129][2] = "D20";
        checkouts[129][3] = "D16";
        checkouts[128][1] = "T20";
        checkouts[128][2] = "D18";
        checkouts[128][3] = "D16";
        checkouts[127][1] = "T19";
        checkouts[127][2] = "D19";
        checkouts[127][3] = "D16";
        checkouts[126][1] = "T20";
        checkouts[126][2] = "D17";
        checkouts[126][3] = "D16";
        checkouts[125][1] = "T19";
        checkouts[125][2] = "D18";
        checkouts[125][3] = "D16";
        checkouts[124][1] = "T20";
        checkouts[124][2] = "D16";
        checkouts[124][3] = "D16";
        checkouts[123][1] = "T19";
        checkouts[123][2] = "D17";
        checkouts[123][3] = "D16";
        checkouts[122][1] = "T18";
        checkouts[122][2] = "D18";
        checkouts[122][3] = "D16";
        checkouts[121][1] = "T19";
        checkouts[121][2] = "D16";
        checkouts[121][3] = "D16";
        checkouts[120][1] = "T20";
        checkouts[120][2] = "20";
        checkouts[120][3] = "D20";
        checkouts[119][1] = "T19";
        checkouts[119][2] = "D15";
        checkouts[119][3] = "D16";
        checkouts[118][1] = "T20";
        checkouts[118][2] = "D13";
        checkouts[118][3] = "D16";
        checkouts[117][1] = "T20";
        checkouts[117][2] = "25";
        checkouts[117][3] = "D16";
        checkouts[116][1] = "T20";
        checkouts[116][2] = "16";
        checkouts[116][3] = "D20";
        checkouts[115][1] = "T20";
        checkouts[115][2] = "15";
        checkouts[115][3] = "D20";
        checkouts[114][1] = "T19";
        checkouts[114][2] = "25";
        checkouts[114][3] = "D16";
        checkouts[113][1] = "T20";
        checkouts[113][2] = "13";
        checkouts[113][3] = "D20";
        checkouts[112][1] = "T20";
        checkouts[112][2] = "20";
        checkouts[112][3] = "D16";
        checkouts[111][1] = "T20";
        checkouts[111][2] = "19";
        checkouts[111][3] = "D16";
        checkouts[110][1] = "T20";
        checkouts[110][2] = "18";
        checkouts[110][3] = "D16";
        checkouts[109][1] = "T20";
        checkouts[109][2] = "18";
        checkouts[109][3] = "D16";
        checkouts[109][1] = "T20";
        checkouts[109][2] = "17";
        checkouts[109][3] = "D16";
        checkouts[108][1] = "T20";
        checkouts[108][2] = "16";
        checkouts[108][3] = "D16";
        checkouts[107][1] = "T20";
        checkouts[107][2] = "15";
        checkouts[107][3] = "D16";
        checkouts[106][1] = "T20";
        checkouts[106][2] = "14";
        checkouts[106][3] = "D16";
        checkouts[105][1] = "T20";
        checkouts[105][2] = "13";
        checkouts[105][3] = "D16";
        checkouts[104][1] = "T20";
        checkouts[104][2] = "12";
        checkouts[104][3] = "D16";
        checkouts[103][1] = "T20";
        checkouts[103][2] = "11";
        checkouts[103][3] = "D16";
        checkouts[102][1] = "T20";
        checkouts[102][2] = "10";
        checkouts[102][3] = "D16";
        checkouts[101][1] = "T20";
        checkouts[101][2] = "9";
        checkouts[101][3] = "D16";
        checkouts[100][1] = "T20";
        checkouts[100][2] = "8";
        checkouts[100][3] = "D16";
        checkouts[99][1] = "T20";
        checkouts[99][2] = "7";
        checkouts[99][3] = "D16";
        checkouts[98][1] = "T20";
        checkouts[98][2] = "6";
        checkouts[98][3] = "D16";
        checkouts[97][1] = "T20";
        checkouts[97][2] = "5";
        checkouts[97][3] = "D16";
        checkouts[96][1] = "T20";
        checkouts[96][2] = "4";
        checkouts[96][3] = "D16";
        checkouts[95][1] = "T20";
        checkouts[95][2] = "3";
        checkouts[95][3] = "D16";
        checkouts[94][1] = "T20";
        checkouts[94][2] = "2";
        checkouts[94][3] = "D16";
        checkouts[93][1] = "T20";
        checkouts[93][2] = "1";
        checkouts[93][3] = "D16";
        checkouts[92][1] = "T20";
        checkouts[92][2] = "D16";
        checkouts[92][3] = "";
        checkouts[91][1] = "T19";
        checkouts[91][2] = "D17";
        checkouts[91][3] = "";
        checkouts[90][1] = "T18";
        checkouts[90][2] = "D18";
        checkouts[90][3] = "";
        checkouts[89][1] = "T19";
        checkouts[89][2] = "D16";
        checkouts[89][3] = "";
        checkouts[88][1] = "T18";
        checkouts[88][2] = "D17";
        checkouts[88][3] = "";
        checkouts[87][1] = "T19";
        checkouts[87][2] = "D15";
        checkouts[87][3] = "";
        checkouts[86][1] = "T18";
        checkouts[86][2] = "D16";
        checkouts[86][3] = "";
        checkouts[85][1] = "T15";
        checkouts[85][2] = "D20";
        checkouts[85][3] = "";
        checkouts[84][1] = "T18";
        checkouts[84][2] = "D15";
        checkouts[84][3] = "";
        checkouts[83][1] = "T17";
        checkouts[83][2] = "D16";
        checkouts[83][3] = "";
        checkouts[82][1] = "D25";
        checkouts[82][2] = "D16";
        checkouts[82][3] = "";
        checkouts[81][1] = "T19";
        checkouts[81][2] = "D12";
        checkouts[81][3] = "";
        checkouts[80][1] = "T16";
        checkouts[80][2] = "D16";
        checkouts[80][3] = "";
        checkouts[79][1] = "T13";
        checkouts[79][2] = "D20";
        checkouts[79][3] = "";
        checkouts[78][1] = "D19";
        checkouts[78][2] = "D20";
        checkouts[78][3] = "";
        checkouts[77][1] = "T15";
        checkouts[77][2] = "D16";
        checkouts[77][3] = "";
        checkouts[76][1] = "D19";
        checkouts[76][2] = "D19";
        checkouts[76][3] = "";
        checkouts[75][1] = "T15";
        checkouts[75][2] = "D15";
        checkouts[75][3] = "";
        checkouts[74][1] = "T14";
        checkouts[74][2] = "D16";
        checkouts[74][3] = "";
        checkouts[73][1] = "T19";
        checkouts[73][2] = "D8";
        checkouts[73][3] = "";
        checkouts[72][1] = "D20";
        checkouts[72][2] = "D16";
        checkouts[72][3] = "";
        checkouts[71][1] = "T13";
        checkouts[71][2] = "D16";
        checkouts[71][3] = "";
        checkouts[70][1] = "D19";
        checkouts[70][2] = "D16";
        checkouts[70][3] = "";
        checkouts[69][1] = "T11";
        checkouts[69][2] = "D18";
        checkouts[69][3] = "";
        checkouts[68][1] = "D18";
        checkouts[68][2] = "D16";
        checkouts[68][3] = "";
        checkouts[67][1] = "T17";
        checkouts[67][2] = "D8";
        checkouts[67][3] = "";
        checkouts[66][1] = "D17";
        checkouts[66][2] = "D16";
        checkouts[66][3] = "";
        checkouts[65][1] = "25";
        checkouts[65][2] = "D20";
        checkouts[65][3] = "";
        checkouts[64][1] = "D16";
        checkouts[64][2] = "D16";
        checkouts[64][3] = "";
        checkouts[63][1] = "25";
        checkouts[63][2] = "D19";
        checkouts[63][3] = "";
        checkouts[62][1] = "D15";
        checkouts[62][2] = "D16";
        checkouts[62][3] = "";
        checkouts[61][1] = "25";
        checkouts[61][2] = "D18";
        checkouts[61][3] = "";
        checkouts[60][1] = "20";
        checkouts[60][2] = "D20";
        checkouts[60][3] = "";
        checkouts[59][1] = "19";
        checkouts[59][2] = "D20";
        checkouts[59][3] = "";
        checkouts[58][1] = "20";
        checkouts[58][2] = "D19";
        checkouts[58][3] = "";
        checkouts[57][1] = "25";
        checkouts[57][2] = "D16";
        checkouts[57][3] = "";
        checkouts[56][1] = "18";
        checkouts[56][2] = "D19";
        checkouts[56][3] = "";
        checkouts[55][1] = "17";
        checkouts[55][2] = "D19";
        checkouts[55][3] = "";
        checkouts[54][1] = "18";
        checkouts[54][2] = "D18";
        checkouts[54][3] = "";
        checkouts[53][1] = "15";
        checkouts[53][2] = "D19";
        checkouts[53][3] = "";
        checkouts[52][1] = "20";
        checkouts[52][2] = "D16";
        checkouts[52][3] = "";
        checkouts[51][1] = "19";
        checkouts[51][2] = "D16";
        checkouts[51][3] = "";
        checkouts[50][1] = "18";
        checkouts[50][2] = "D16";
        checkouts[50][3] = "";
        checkouts[49][1] = "17";
        checkouts[49][2] = "D16";
        checkouts[49][3] = "";
        checkouts[48][1] = "16";
        checkouts[48][2] = "D16";
        checkouts[48][3] = "";
        checkouts[47][1] = "15";
        checkouts[47][2] = "D16";
        checkouts[47][3] = "";
        checkouts[46][1] = "14";
        checkouts[46][2] = "D16";
        checkouts[46][3] = "";
        checkouts[45][1] = "13";
        checkouts[45][2] = "D16";
        checkouts[45][3] = "";
        checkouts[44][1] = "12";
        checkouts[44][2] = "D16";
        checkouts[44][3] = "";
        checkouts[43][1] = "11";
        checkouts[43][2] = "D16";
        checkouts[43][3] = "";
        checkouts[42][1] = "10";
        checkouts[42][2] = "D16";
        checkouts[42][3] = "";
        checkouts[41][1] = "9";
        checkouts[41][2] = "D16";
        checkouts[41][3] = "";
        checkouts[40][1] = "D20";
        checkouts[40][2] = "";
        checkouts[40][3] = "";
        checkouts[39][1] = "7";
        checkouts[39][2] = "D16";
        checkouts[39][3] = "";
        checkouts[38][1] = "D19";
        checkouts[38][2] = "";
        checkouts[38][3] = "";
        checkouts[37][1] = "5";
        checkouts[37][2] = "D16";
        checkouts[37][3] = "";
        checkouts[36][1] = "D18";
        checkouts[36][2] = "";
        checkouts[36][3] = "";
        checkouts[35][1] = "3";
        checkouts[35][2] = "D16";
        checkouts[35][3] = "";
        checkouts[34][1] = "D17";
        checkouts[34][2] = "";
        checkouts[34][3] = "";
        checkouts[33][1] = "1";
        checkouts[33][2] = "D16";
        checkouts[33][3] = "";
        checkouts[32][1] = "D16";
        checkouts[32][2] = "";
        checkouts[32][3] = "";
        checkouts[31][1] = "15";
        checkouts[31][2] = "D8";
        checkouts[31][3] = "";
        checkouts[30][1] = "D15";
        checkouts[30][2] = "";
        checkouts[30][3] = "";
        checkouts[29][1] = "13";
        checkouts[29][2] = "D8";
        checkouts[29][3] = "";
        checkouts[28][1] = "D14";
        checkouts[28][2] = "";
        checkouts[28][3] = "";
        checkouts[27][1] = "11";
        checkouts[27][2] = "D8";
        checkouts[27][3] = "";
        checkouts[26][1] = "D13";
        checkouts[26][2] = "";
        checkouts[26][3] = "";
        checkouts[25][1] = "9";
        checkouts[25][2] = "D8";
        checkouts[25][3] = "";
        checkouts[24][1] = "D12";
        checkouts[24][2] = "";
        checkouts[24][3] = "";
        checkouts[23][1] = "7";
        checkouts[23][2] = "D8";
        checkouts[23][3] = "";
        checkouts[22][1] = "D11";
        checkouts[22][2] = "";
        checkouts[22][3] = "";
        checkouts[21][1] = "5";
        checkouts[21][2] = "D8";
        checkouts[21][3] = "";
        checkouts[20][1] = "D10";
        checkouts[20][2] = "";
        checkouts[20][3] = "";
        checkouts[19][1] = "3";
        checkouts[19][2] = "D8";
        checkouts[19][3] = "";
        checkouts[18][1] = "D9";
        checkouts[18][2] = "";
        checkouts[18][3] = "";
        checkouts[17][1] = "1";
        checkouts[17][2] = "D8";
        checkouts[17][3] = "";
        checkouts[16][1] = "D8";
        checkouts[16][2] = "";
        checkouts[16][3] = "";
        checkouts[15][1] = "7";
        checkouts[15][2] = "D4";
        checkouts[15][3] = "";
        checkouts[14][1] = "D7";
        checkouts[14][2] = "";
        checkouts[14][3] = "";
        checkouts[13][1] = "5";
        checkouts[13][2] = "D4";
        checkouts[13][3] = "";
        checkouts[12][1] = "D6";
        checkouts[12][2] = "";
        checkouts[12][3] = "";
        checkouts[11][1] = "3";
        checkouts[11][2] = "D4";
        checkouts[11][3] = "";
        checkouts[10][1] = "D5";
        checkouts[10][2] = "";
        checkouts[10][3] = "";
        checkouts[9][1] = "1";
        checkouts[9][2] = "D4";
        checkouts[9][3] = "";
        checkouts[8][1] = "D4";
        checkouts[8][2] = "";
        checkouts[8][3] = "";
        checkouts[7][1] = "3";
        checkouts[7][2] = "D2";
        checkouts[7][3] = "";
        checkouts[6][1] = "D3";
        checkouts[6][2] = "";
        checkouts[6][3] = "";
        checkouts[5][1] = "1";
        checkouts[5][2] = "D2";
        checkouts[5][3] = "";
        checkouts[4][1] = "D2";
        checkouts[4][2] = "";
        checkouts[4][3] = "";
        checkouts[3][1] = "1";
        checkouts[3][2] = "D1";
        checkouts[3][3] = "";
        checkouts[2][1] = "MaD_hOuSe";
        checkouts[2][2] = "";
        checkouts[2][3] = "";
        checkouts[1][1] = "What the ...!?";
        checkouts[1][2] = "";
        checkouts[1][3] = "";
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(updateBaseContextLocale(base));
    }

    private Context updateBaseContextLocale(Context context) {
        final SharedPreferences settings = context.getSharedPreferences("Einstellungen", 0);
        String language = Locale.getDefault().getLanguage();
        if (settings.contains("Sprache")) {
            language = settings.getString("Sprache", "en");
        }
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return updateResourcesLocale(context, locale);
        }

        return updateResourcesLocaleLegacy(context, locale);
    }

    @TargetApi(Build.VERSION_CODES.N_MR1)
    private Context updateResourcesLocale(Context context, Locale locale) {
        Configuration configuration = new Configuration(context.getResources().getConfiguration());
        configuration.setLocale(locale);
        return context.createConfigurationContext(configuration);
    }

    @SuppressWarnings("deprecation")
    private Context updateResourcesLocaleLegacy(Context context, Locale locale) {
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        return context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (MainActivity.themeauswahl) setTheme(R.style.AppTheme);
        else setTheme(R.style.Theme_AppCompat_Light_NoActionBar);
        setContentView(R.layout.activity_elimination);
        SharedPreferences settings = getSharedPreferences("Einstellungen", 0);

        // spielmodus und co übernehmen
        Intent intent = getIntent();
        CharSequence spieler1n = intent.getCharSequenceExtra("spieler1");
        CharSequence spieler2n = intent.getCharSequenceExtra("spieler2");
        CharSequence spieler3n = intent.getCharSequenceExtra("spieler3");
        CharSequence spieler4n = intent.getCharSequenceExtra("spieler4");
        CharSequence spieler5n = intent.getCharSequenceExtra("spieler5");
        CharSequence spieler6n = intent.getCharSequenceExtra("spieler6");
        CharSequence spieler7n = intent.getCharSequenceExtra("spieler7");
        CharSequence spieler8n = intent.getCharSequenceExtra("spieler8");
        spieler1.spielerName = spieler1n.toString();
        spieler2.spielerName = spieler2n.toString();
        spieler3.spielerName = spieler3n.toString();
        spieler4.spielerName = spieler4n.toString();
        spieler5.spielerName = spieler5n.toString();
        spieler6.spielerName = spieler6n.toString();
        spieler7.spielerName = spieler7n.toString();
        spieler8.spielerName = spieler8n.toString();

        score = findViewById(R.id.punktep1);
        d1 = findViewById(R.id.pfeil1);
        d2 = findViewById(R.id.pfeil2);
        d3 = findViewById(R.id.pfeil3);
        v1 = findViewById(R.id.vorschlag1);
        v2 = findViewById(R.id.vorschlag2);
        v3 = findViewById(R.id.vorschlag3);
        name = findViewById(R.id.p1name);
        durchschnitt = findViewById(R.id.durchschnitt);
        pfeile = findViewById(R.id.pfeilegeworfen);
        listenName1 = findViewById(R.id.listeSpieler1);
        listenName2 = findViewById(R.id.listeSpieler2);
        listenName3 = findViewById(R.id.listeSpieler3);
        listenName4 = findViewById(R.id.listeSpieler4);
        listenName5 = findViewById(R.id.listeSpieler5);
        listenName6 = findViewById(R.id.listeSpieler6);
        listenName7 = findViewById(R.id.listeSpieler7);
        listenPunkte1 = findViewById(R.id.listeSpieler1SS);
        listenPunkte2 = findViewById(R.id.listeSpieler2SS);
        listenPunkte3 = findViewById(R.id.listeSpieler3SS);
        listenPunkte4 = findViewById(R.id.listeSpieler4SS);
        listenPunkte5 = findViewById(R.id.listeSpieler5SS);
        listenPunkte6 = findViewById(R.id.listeSpieler6SS);
        listenPunkte7 = findViewById(R.id.listeSpieler7SS);
        listenvorschlag1 = findViewById(R.id.listeSpieler1v);
        listenvorschlag2 = findViewById(R.id.listeSpieler2v);
        listenvorschlag3 = findViewById(R.id.listeSpieler3v);
        listenvorschlag4 = findViewById(R.id.listeSpieler4v);
        listenvorschlag5 = findViewById(R.id.listeSpieler5v);
        listenvorschlag6 = findViewById(R.id.listeSpieler6v);
        listenvorschlag7 = findViewById(R.id.listeSpieler7v);
        scoretoeliminate = findViewById(R.id.scoretoeliminat);
        toeliminate = findViewById(R.id.toeliminatebeschriftun);
        listenvorschlag1.setVisibility(View.GONE);
        listenvorschlag2.setVisibility(View.GONE);
        listenvorschlag3.setVisibility(View.GONE);
        listenvorschlag4.setVisibility(View.GONE);
        listenvorschlag5.setVisibility(View.GONE);
        listenvorschlag6.setVisibility(View.GONE);
        listenvorschlag7.setVisibility(View.GONE);
        spieleranzahl = Integer.parseInt(intent.getStringExtra("spieleranzahl"));
        doubleout = intent.getBooleanExtra("doubleout", false);
        masterout = intent.getBooleanExtra("masterout", false);
        CharSequence spielmodus = intent.getCharSequenceExtra("spielmodus");
        maxPunkte = Integer.parseInt(spielmodus.toString());

            toeliminate.setVisibility(View.GONE);
            scoretoeliminate.setVisibility(View.GONE);


        score.setText("0");
        eliminationsvorschlagsetzen();
        name.setText(spieler1n);
        checkoutinit();
        vorschlagsetzen(maxPunkte);
        spieler1.score = 0;
        spieler2.score = 0;
        spieler3.score = 0;
        spieler4.score = 0;
        spieler5.score = 0;
        spieler6.score = 0;
        spieler7.score = 0;
        spieler8.score = 0;

        // initialisiere restliche spieler (in der anzeigeliste)
        switch (spieleranzahl) {
            case 1:
                listenName1.setVisibility(View.GONE);
                listenName2.setVisibility(View.GONE);
                listenName3.setVisibility(View.GONE);
                listenName4.setVisibility(View.GONE);
                listenName5.setVisibility(View.GONE);
                listenName6.setVisibility(View.GONE);
                listenName7.setVisibility(View.GONE);
                listenPunkte1.setVisibility(View.GONE);
                listenPunkte2.setVisibility(View.GONE);
                listenPunkte3.setVisibility(View.GONE);
                listenPunkte4.setVisibility(View.GONE);
                listenPunkte5.setVisibility(View.GONE);
                listenPunkte6.setVisibility(View.GONE);
                listenPunkte7.setVisibility(View.GONE);
                Guideline rechts = findViewById(R.id.rechtsoben);
                rechts.setGuidelinePercent(1);
                View senkrecht = findViewById(R.id.senkrecht);
                senkrecht.setVisibility(View.GONE);
                break;
            case 2:
                listenName1.setVisibility(View.VISIBLE);
                listenName1.setText(spieler2n);
                listenName2.setVisibility(View.GONE);
                listenName3.setVisibility(View.GONE);
                listenName4.setVisibility(View.GONE);
                listenName5.setVisibility(View.GONE);
                listenName6.setVisibility(View.GONE);
                listenName7.setVisibility(View.GONE);
                listenPunkte1.setVisibility(View.VISIBLE);
                listenPunkte1.setText("0  Ø: 0");
                listenPunkte2.setVisibility(View.GONE);
                listenPunkte3.setVisibility(View.GONE);
                listenPunkte4.setVisibility(View.GONE);
                listenPunkte5.setVisibility(View.GONE);
                listenPunkte6.setVisibility(View.GONE);
                listenPunkte7.setVisibility(View.GONE);
                break;
            case 3:
                listenName1.setVisibility(View.VISIBLE);
                listenName1.setText(spieler2n);
                listenName2.setVisibility(View.VISIBLE);
                listenName2.setText(spieler3n);
                listenName3.setVisibility(View.GONE);
                listenName4.setVisibility(View.GONE);
                listenName5.setVisibility(View.GONE);
                listenName6.setVisibility(View.GONE);
                listenName7.setVisibility(View.GONE);
                listenPunkte1.setVisibility(View.VISIBLE);
                listenPunkte1.setText("0  Ø: 0");
                listenPunkte2.setVisibility(View.VISIBLE);
                listenPunkte2.setText("0  Ø: 0");
                listenPunkte3.setVisibility(View.GONE);
                listenPunkte4.setVisibility(View.GONE);
                listenPunkte5.setVisibility(View.GONE);
                listenPunkte6.setVisibility(View.GONE);
                listenPunkte7.setVisibility(View.GONE);
                break;
            case 4:
                listenName1.setVisibility(View.VISIBLE);
                listenName1.setText(spieler2n);
                listenName2.setVisibility(View.VISIBLE);
                listenName2.setText(spieler3n);
                listenName3.setVisibility(View.VISIBLE);
                listenName3.setText(spieler4n);
                listenName4.setVisibility(View.GONE);
                listenName5.setVisibility(View.GONE);
                listenName6.setVisibility(View.GONE);
                listenName7.setVisibility(View.GONE);
                listenPunkte1.setVisibility(View.VISIBLE);
                listenPunkte1.setText("0  Ø: 0");
                listenPunkte2.setVisibility(View.VISIBLE);
                listenPunkte2.setText("0  Ø: 0");
                listenPunkte3.setVisibility(View.VISIBLE);
                listenPunkte3.setText("0  Ø: 0");
                listenPunkte4.setVisibility(View.GONE);
                listenPunkte5.setVisibility(View.GONE);
                listenPunkte6.setVisibility(View.GONE);
                listenPunkte7.setVisibility(View.GONE);
                break;
            case 5:
                listenName1.setVisibility(View.VISIBLE);
                listenName1.setText(spieler2n);
                listenName2.setVisibility(View.VISIBLE);
                listenName2.setText(spieler3n);
                listenName3.setVisibility(View.VISIBLE);
                listenName3.setText(spieler4n);
                listenName4.setVisibility(View.VISIBLE);
                listenName4.setText(spieler5n);
                listenName5.setVisibility(View.GONE);
                listenName6.setVisibility(View.GONE);
                listenName7.setVisibility(View.GONE);
                listenPunkte1.setVisibility(View.VISIBLE);
                listenPunkte1.setText("0  Ø: 0");
                listenPunkte2.setVisibility(View.VISIBLE);
                listenPunkte2.setText("0  Ø: 0");
                listenPunkte3.setVisibility(View.VISIBLE);
                listenPunkte3.setText("0  Ø: 0");
                listenPunkte4.setVisibility(View.VISIBLE);
                listenPunkte4.setText("0  Ø: 0");
                listenPunkte5.setVisibility(View.GONE);
                listenPunkte6.setVisibility(View.GONE);
                listenPunkte7.setVisibility(View.GONE);
                break;
            case 6:
                listenName1.setVisibility(View.VISIBLE);
                listenName1.setText(spieler2n);
                listenName2.setVisibility(View.VISIBLE);
                listenName2.setText(spieler3n);
                listenName3.setVisibility(View.VISIBLE);
                listenName3.setText(spieler4n);
                listenName4.setVisibility(View.VISIBLE);
                listenName4.setText(spieler5n);
                listenName5.setVisibility(View.VISIBLE);
                listenName5.setText(spieler6n);
                listenName6.setVisibility(View.GONE);
                listenName7.setVisibility(View.GONE);
                listenPunkte1.setVisibility(View.VISIBLE);
                listenPunkte1.setText("0  Ø: 0");
                listenPunkte2.setVisibility(View.VISIBLE);
                listenPunkte2.setText("0  Ø: 0");
                listenPunkte3.setVisibility(View.VISIBLE);
                listenPunkte3.setText("0  Ø: 0");
                listenPunkte4.setVisibility(View.VISIBLE);
                listenPunkte4.setText("0  Ø: 0");
                listenPunkte5.setVisibility(View.VISIBLE);
                listenPunkte5.setText("0  Ø: 0");
                listenPunkte6.setVisibility(View.GONE);
                listenPunkte7.setVisibility(View.GONE);
                break;
            case 7:
                listenName1.setVisibility(View.VISIBLE);
                listenName1.setText(spieler2n);
                listenName2.setVisibility(View.VISIBLE);
                listenName2.setText(spieler3n);
                listenName3.setVisibility(View.VISIBLE);
                listenName3.setText(spieler4n);
                listenName4.setVisibility(View.VISIBLE);
                listenName4.setText(spieler5n);
                listenName5.setVisibility(View.VISIBLE);
                listenName5.setText(spieler6n);
                listenName6.setVisibility(View.VISIBLE);
                listenName6.setText(spieler7n);
                listenName7.setVisibility(View.GONE);
                listenPunkte1.setVisibility(View.VISIBLE);
                listenPunkte1.setText("0  Ø: 0");
                listenPunkte2.setVisibility(View.VISIBLE);
                listenPunkte2.setText("0  Ø: 0");
                listenPunkte3.setVisibility(View.VISIBLE);
                listenPunkte3.setText("0  Ø: 0");
                listenPunkte4.setVisibility(View.VISIBLE);
                listenPunkte4.setText("0  Ø: 0");
                listenPunkte5.setVisibility(View.VISIBLE);
                listenPunkte5.setText("0  Ø: 0");
                listenPunkte6.setVisibility(View.VISIBLE);
                listenPunkte6.setText("0  Ø: 0");
                listenPunkte7.setVisibility(View.GONE);
                break;
            case 8:
                listenName1.setVisibility(View.VISIBLE);
                listenName1.setText(spieler2n);
                listenName2.setVisibility(View.VISIBLE);
                listenName2.setText(spieler3n);
                listenName3.setVisibility(View.VISIBLE);
                listenName3.setText(spieler4n);
                listenName4.setVisibility(View.VISIBLE);
                listenName4.setText(spieler5n);
                listenName5.setVisibility(View.VISIBLE);
                listenName5.setText(spieler6n);
                listenName6.setVisibility(View.VISIBLE);
                listenName6.setText(spieler7n);
                listenName7.setVisibility(View.VISIBLE);
                listenName7.setText(spieler8n);
                listenPunkte1.setVisibility(View.VISIBLE);
                listenPunkte1.setText("0  Ø: 0");
                listenPunkte2.setVisibility(View.VISIBLE);
                listenPunkte2.setText("0  Ø: 0");
                listenPunkte3.setVisibility(View.VISIBLE);
                listenPunkte3.setText("0  Ø: 0");
                listenPunkte4.setVisibility(View.VISIBLE);
                listenPunkte4.setText("0  Ø: 0");
                listenPunkte5.setVisibility(View.VISIBLE);
                listenPunkte5.setText("0  Ø: 0");
                listenPunkte6.setVisibility(View.VISIBLE);
                listenPunkte6.setText("0  Ø: 0");
                listenPunkte7.setVisibility(View.VISIBLE);
                listenPunkte7.setText("0  Ø: 0");
                break;

        }


        Button b1 = findViewById(R.id.b1);
        Button b2 = findViewById(R.id.b2);
        Button b3 = findViewById(R.id.b3);
        Button b4 = findViewById(R.id.b4);
        Button b5 = findViewById(R.id.b5);
        Button b6 = findViewById(R.id.b6);
        Button b7 = findViewById(R.id.b7);
        Button b8 = findViewById(R.id.b8);
        Button b9 = findViewById(R.id.b9);
        Button b10 = findViewById(R.id.b10);
        Button b11 = findViewById(R.id.b11);
        Button b12 = findViewById(R.id.b12);
        Button b13 = findViewById(R.id.b13);
        Button b14 = findViewById(R.id.b14);
        Button b15 = findViewById(R.id.b15);
        Button b16 = findViewById(R.id.b16);
        Button b17 = findViewById(R.id.b17);
        Button b18 = findViewById(R.id.b18);
        Button b19 = findViewById(R.id.b19);
        Button b20 = findViewById(R.id.b20);
        Button bull = findViewById(R.id.bull);
        Button bdouble = findViewById(R.id.doublebutton);
        Button btripel = findViewById(R.id.triple);
        Button daneben = findViewById(R.id.daneben);
        Button bundo = findViewById(R.id.undo);
        Button restdaneben = findViewById(R.id.restdaneben);
        TextView aufnahmetv = findViewById(R.id.aufnahmetv);
        b1.setOnClickListener(buttonclick);
        b2.setOnClickListener(buttonclick);
        b3.setOnClickListener(buttonclick);
        b4.setOnClickListener(buttonclick);
        b5.setOnClickListener(buttonclick);
        b6.setOnClickListener(buttonclick);
        b7.setOnClickListener(buttonclick);
        b8.setOnClickListener(buttonclick);
        b9.setOnClickListener(buttonclick);
        b10.setOnClickListener(buttonclick);
        b11.setOnClickListener(buttonclick);
        b12.setOnClickListener(buttonclick);
        b13.setOnClickListener(buttonclick);
        b14.setOnClickListener(buttonclick);
        b15.setOnClickListener(buttonclick);
        b16.setOnClickListener(buttonclick);
        b17.setOnClickListener(buttonclick);
        b18.setOnClickListener(buttonclick);
        b19.setOnClickListener(buttonclick);
        b20.setOnClickListener(buttonclick);
        daneben.setOnClickListener(buttonclick);
        restdaneben.setOnClickListener(restdanebenclick);
        bull.setOnClickListener(buttonclick);
        bdouble.setOnClickListener(doubletriple);
        btripel.setOnClickListener(doubletriple);
        bundo.setOnClickListener(undoclick);
        aufnahmetv.setOnClickListener(setleganzeigeclick);

        formater.setMaximumFractionDigits(2);
        formater.setMaximumIntegerDigits(3);
        formater.setMinimumIntegerDigits(1);
        formater.setMinimumFractionDigits(0);

        if (settings.contains("changetime")) changetime = settings.getInt("changetime", 1500);

        TypedValue outValue = new TypedValue();
        elimination.this.getTheme().resolveAttribute(R.attr.colorButtonNormal, outValue, true);
        bcolor = outValue.data;
        elimination.this.getTheme().resolveAttribute(R.attr.selectableItemBackground, outValue, true);
        bcolorn = outValue.data;
        if (settings.contains("suddendeath")) suddendeath = settings.getBoolean("suddendeath",false);
        int runden=0;
        if (settings.contains("sddarts")) runden = settings.getInt("sddarts",20);
        suddendeathdarts = runden*3;
        startTime=System.currentTimeMillis();

    }


    private void buttonfreeze(boolean freeze) {
        Button b1 = findViewById(R.id.b1);
        Button b2 = findViewById(R.id.b2);
        Button b3 = findViewById(R.id.b3);
        Button b4 = findViewById(R.id.b4);
        Button b5 = findViewById(R.id.b5);
        Button b6 = findViewById(R.id.b6);
        Button b7 = findViewById(R.id.b7);
        Button b8 = findViewById(R.id.b8);
        Button b9 = findViewById(R.id.b9);
        Button b10 = findViewById(R.id.b10);
        Button b11 = findViewById(R.id.b11);
        Button b12 = findViewById(R.id.b12);
        Button b13 = findViewById(R.id.b13);
        Button b14 = findViewById(R.id.b14);
        Button b15 = findViewById(R.id.b15);
        Button b16 = findViewById(R.id.b16);
        Button b17 = findViewById(R.id.b17);
        Button b18 = findViewById(R.id.b18);
        Button b19 = findViewById(R.id.b19);
        Button b20 = findViewById(R.id.b20);
        Button bull = findViewById(R.id.bull);
        Button bdouble = findViewById(R.id.doublebutton);
        Button btripel = findViewById(R.id.triple);
        Button daneben = findViewById(R.id.daneben);
        Button bundo = findViewById(R.id.undo);
        Button restdaneben = findViewById(R.id.restdaneben);

        freeze = !freeze;       //eine frage der logik ;-)

        bdouble.setEnabled(freeze);
        btripel.setEnabled(freeze);
        b1.setEnabled(freeze);
        b2.setEnabled(freeze);
        b3.setEnabled(freeze);
        b4.setEnabled(freeze);
        b5.setEnabled(freeze);
        b6.setEnabled(freeze);
        b7.setEnabled(freeze);
        b8.setEnabled(freeze);
        b9.setEnabled(freeze);
        b10.setEnabled(freeze);
        b11.setEnabled(freeze);
        b12.setEnabled(freeze);
        b13.setEnabled(freeze);
        b14.setEnabled(freeze);
        b15.setEnabled(freeze);
        b16.setEnabled(freeze);
        b17.setEnabled(freeze);
        b18.setEnabled(freeze);
        b19.setEnabled(freeze);
        b20.setEnabled(freeze);
        bull.setEnabled(freeze);
        daneben.setEnabled(freeze);
        bundo.setEnabled(freeze);
        restdaneben.setEnabled(freeze);
    }

    private int setAufnahme() {
        int auf;
        boolean add = true;
        auf = 0;

     /*alte version
        //wenn nicht überworfen etc., dann übernimm die aufnahme in die statistik
        setLast3zeiger(false);
        if (last3[last3zeiger + 2] == 111) {
            add = false;
            setLast3zeiger(true);
        } else {
            setLast3zeiger(false);
            if (last3[last3zeiger + 2] == 111) {
                add = false;
                setLast3zeiger(true);
                setLast3zeiger(true);
            } else {
                setLast3zeiger(false);
                if (last3[last3zeiger + 2] == 111) {
                    add = false;
                    setLast3zeiger(true);
                    setLast3zeiger(true);
                    setLast3zeiger(true);
                } else {
                    setLast3zeiger(true);
                    setLast3zeiger(true);
                    setLast3zeiger(true);
                }
            }
        }
      */

        if (add) {

            // letzten dart
         /* alte version
            setLast3zeiger(false);

            auf += last3[last3zeiger] * last3[last3zeiger + 1];
            setLast3zeiger(true);
         */
            auf += wuerfe.get(wuerfe.size()-1).addpunkte;
            switch (xdart) {        //aufnahme ist i.d.R. mit 3 pfeilen zuende, beim sieg auch mit 1 oder 2
                case 0:
                    break;
                case 1:
                /* setLast3zeiger(false);
                    setLast3zeiger(false);
                    auf += last3[last3zeiger] * last3[last3zeiger + 1];
                    setLast3zeiger(true);
                    setLast3zeiger(true);
                  */
                    auf += wuerfe.get(wuerfe.size()-2).addpunkte;
                    break;
                case 2:
                   /* setLast3zeiger(false);
                    setLast3zeiger(false);
                    auf += last3[last3zeiger] * last3[last3zeiger + 1];
                    setLast3zeiger(false);
                    auf += last3[last3zeiger] * last3[last3zeiger + 1];
                    setLast3zeiger(true);
                    setLast3zeiger(true);
                    setLast3zeiger(true);
                    */
                    auf += wuerfe.get(wuerfe.size()-2).addpunkte;
                    auf += wuerfe.get(wuerfe.size()-3).addpunkte;

                    break;
            }
        }
        return auf;
    }

    private void nextplayer_with_freeze() {
        buttonfreeze(true);
        final TextView aufnahmetv = findViewById(R.id.aufnahmetv);
        aufnahmetv.setText(String.valueOf(setAufnahme()));
        aufnahmetv.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                nextplayer();
                buttonfreeze(false);
                aufnahmetv.setVisibility(View.INVISIBLE);
            }
        }, changetime);

    }

    private void wurfstatistik(MainActivity.spieler player, boolean statistikerweitern) {
        int aufnahme = setAufnahme();
        if (statistikerweitern) {
                if (aufnahme == 180) player.anzahl180++;
                else if (aufnahme >= 140) player.anzahluber140++;
                else if (aufnahme >= 100) player.anzahluber100++;
                else if (aufnahme >= 60) player.anzahluber60++;
                if (aufnahme >= player.besterWurf) {
                    player.zweitbesterWurf = player.besterWurf;
                    player.besterWurf = aufnahme;
                }
                if (letzterwurf && (aufnahme > player.checkoutmax) && !suddendeatherreicht()) {
                    player.checkoutmax = aufnahme;
                }
                letzterwurf = false;


                //pfeile und durschnitt immer übernehmen, auch wenn score gleich bleibt, z.b. bei nullwurf
                player.score = Integer.parseInt(score.getText().toString());
                player.geworfenePfeile = Integer.parseInt(pfeile.getText().toString());
                player.durchschnitt = Float.valueOf(durchschnitt.getText().toString());

            }

        // undo! statistik evtl. von der letzten aufnahme bereinigen - wird nur bei xdart=2 bzw. 0-1 durch lastplayer() in undo() aufgerufen
        else {   //immer die letzten drei pfeile sind eine aufnahme, ausser es gab ein überworfen, dann gibts aber auch nichts zu bereinigen
            if ((aufnahme == 180) && player.anzahl180 > 0) player.anzahl180--;
            else if ((aufnahme >= 140) && player.anzahluber140 > 0) player.anzahluber140--;
            else if ((aufnahme >= 100) && player.anzahluber100 > 0) player.anzahluber100--;
            else if ((aufnahme >= 60) && player.anzahluber60 > 0) player.anzahluber60--;

            if (aufnahme == player.besterWurf) {
                player.besterWurf = player.zweitbesterWurf;
            }
        }
    }



    private void nextplayer()        // setzt die anzeigen so, dass der nächste spieler dran ist
    {
        rundenname = name.getText().toString();
        switch (spieleranzahl) {
            case 1: {
                // aktuelle anzeigewerte fungieren als speicher bis spielende
                wurfstatistik(spieler1, true);
                break;
            }
            case 2: {
                // anzeigewerte speichern
                listenPunkte1.setText(score.getText() + "  Ø: " + durchschnitt.getText());
                vorschlagsetzenliste(maxPunkte-Integer.parseInt(score.getText().toString()), listenvorschlag1);
                if (rundenname.equals(spieler1.spielerName)) {
                    wurfstatistik(spieler1, true);
                    //liste aktualisieren
                    listenName1.setText(spieler1.spielerName);
                    //aktueller spieler aktualisieren
                    name.setText(spieler2.spielerName);
                    score.setText(String.valueOf(spieler2.score));
                    pfeile.setText(String.valueOf(spieler2.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler2.durchschnitt));
                } else if (rundenname.equals(spieler2.spielerName)) {
                    wurfstatistik(spieler2, true);
                    //liste aktualisieren
                    listenName1.setText(spieler2.spielerName);
                    //aktueller spieler aktualisieren
                    name.setText(spieler1.spielerName);
                    score.setText(String.valueOf(spieler1.score));
                    pfeile.setText(String.valueOf(spieler1.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler1.durchschnitt));
                }
                break;
            }
            case 3: {
                if (!(listenvorschlag2.getText().equals(""))) {
                    listenvorschlag1.setText(listenvorschlag2.getText());
                    listenvorschlag1.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag1.setText("");
                    listenvorschlag1.setVisibility(View.GONE);
                }

                vorschlagsetzenliste(maxPunkte-Integer.parseInt(score.getText().toString()), listenvorschlag2);

                if (rundenname.equals(spieler1.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler1, true);
                    //liste aktualisieren
                    listenName1.setText(spieler3.spielerName);
                    listenPunkte1.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName2.setText(spieler1.spielerName);
                    listenPunkte2.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler2.spielerName);
                    score.setText(String.valueOf(spieler2.score));
                    pfeile.setText(String.valueOf(spieler2.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler2.durchschnitt));
                } else if (rundenname.equals(spieler2.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler2, true);
                    //liste aktualisieren
                    listenName1.setText(spieler1.spielerName);
                    listenPunkte1.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName2.setText(spieler2.spielerName);
                    listenPunkte2.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler3.spielerName);
                    score.setText(String.valueOf(spieler3.score));
                    pfeile.setText(String.valueOf(spieler3.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler3.durchschnitt));
                } else if (rundenname.equals(spieler3.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler3, true);
                    //liste aktualisieren
                    listenName1.setText(spieler2.spielerName);
                    listenPunkte1.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName2.setText(spieler3.spielerName);
                    listenPunkte2.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler1.spielerName);
                    score.setText(String.valueOf(spieler1.score));
                    pfeile.setText(String.valueOf(spieler1.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler1.durchschnitt));
                }
                break;
            }
            case 4: {
                if (!(listenvorschlag2.getText().equals(""))) {
                    listenvorschlag1.setText(listenvorschlag2.getText());
                    listenvorschlag1.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag1.setText("");
                    listenvorschlag1.setVisibility(View.GONE);
                }
                if (!(listenvorschlag3.getText().equals(""))) {
                    listenvorschlag2.setText(listenvorschlag3.getText());
                    listenvorschlag2.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag2.setText("");
                    listenvorschlag2.setVisibility(View.GONE);
                }

                vorschlagsetzenliste(maxPunkte-Integer.parseInt(score.getText().toString()), listenvorschlag3);


                if (rundenname.equals(spieler1.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler1, true);
                    //liste aktualisieren
                    listenName1.setText(spieler3.spielerName);
                    listenPunkte1.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName2.setText(spieler4.spielerName);
                    listenPunkte2.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName3.setText(spieler1.spielerName);
                    listenPunkte3.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler2.spielerName);
                    score.setText(String.valueOf(spieler2.score));
                    pfeile.setText(String.valueOf(spieler2.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler2.durchschnitt));
                } else if (rundenname.equals(spieler2.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler2, true);
                    //liste aktualisieren
                    listenName1.setText(spieler4.spielerName);
                    listenPunkte1.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName2.setText(spieler1.spielerName);
                    listenPunkte2.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName3.setText(spieler2.spielerName);
                    listenPunkte3.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler3.spielerName);
                    score.setText(String.valueOf(spieler3.score));
                    pfeile.setText(String.valueOf(spieler3.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler3.durchschnitt));
                } else if (rundenname.equals(spieler3.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler3, true);
                    //liste aktualisieren
                    listenName1.setText(spieler1.spielerName);
                    listenPunkte1.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName2.setText(spieler2.spielerName);
                    listenPunkte2.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName3.setText(spieler3.spielerName);
                    listenPunkte3.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler4.spielerName);
                    score.setText(String.valueOf(spieler4.score));
                    pfeile.setText(String.valueOf(spieler4.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler4.durchschnitt));
                } else if (rundenname.equals(spieler4.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler4, true);
                    //liste aktualisieren
                    listenName1.setText(spieler2.spielerName);
                    listenPunkte1.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName2.setText(spieler3.spielerName);
                    listenPunkte2.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName3.setText(spieler4.spielerName);
                    listenPunkte3.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler1.spielerName);
                    score.setText(String.valueOf(spieler1.score));
                    pfeile.setText(String.valueOf(spieler1.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler1.durchschnitt));
                }
                break;
            }
            case 5:
            {
                if (!(listenvorschlag2.getText().equals(""))) {
                    listenvorschlag1.setText(listenvorschlag2.getText());
                    listenvorschlag1.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag1.setText("");
                    listenvorschlag1.setVisibility(View.GONE);
                }
                if (!(listenvorschlag3.getText().equals(""))) {
                    listenvorschlag2.setText(listenvorschlag3.getText());
                    listenvorschlag2.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag2.setText("");
                    listenvorschlag2.setVisibility(View.GONE);
                }
                if (!(listenvorschlag4.getText().equals(""))) {
                    listenvorschlag3.setText(listenvorschlag4.getText());
                    listenvorschlag3.setVisibility(View.VISIBLE);
                } else {
                listenvorschlag3.setText("");
                listenvorschlag3.setVisibility(View.GONE);
                }

                vorschlagsetzenliste(maxPunkte-Integer.parseInt(score.getText().toString()), listenvorschlag4);

                if (rundenname.equals(spieler1.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler1, true);
                    //liste aktualisieren
                    listenName1.setText(spieler3.spielerName);
                    listenPunkte1.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName2.setText(spieler4.spielerName);
                    listenPunkte2.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName3.setText(spieler5.spielerName);
                    listenPunkte3.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName4.setText(spieler1.spielerName);
                    listenPunkte4.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler2.spielerName);
                    score.setText(String.valueOf(spieler2.score));
                    pfeile.setText(String.valueOf(spieler2.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler2.durchschnitt));
                } else if (rundenname.equals(spieler2.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler2, true);
                    //liste aktualisieren
                    listenName1.setText(spieler4.spielerName);
                    listenPunkte1.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName2.setText(spieler5.spielerName);
                    listenPunkte2.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName3.setText(spieler1.spielerName);
                    listenPunkte3.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName4.setText(spieler2.spielerName);
                    listenPunkte4.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler3.spielerName);
                    score.setText(String.valueOf(spieler3.score));
                    pfeile.setText(String.valueOf(spieler3.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler3.durchschnitt));
                } else if (rundenname.equals(spieler3.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler3, true);
                    //liste aktualisieren
                    listenName1.setText(spieler5.spielerName);
                    listenPunkte1.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName2.setText(spieler1.spielerName);
                    listenPunkte2.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName3.setText(spieler2.spielerName);
                    listenPunkte3.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName4.setText(spieler3.spielerName);
                    listenPunkte4.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler4.spielerName);
                    score.setText(String.valueOf(spieler4.score));
                    pfeile.setText(String.valueOf(spieler4.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler4.durchschnitt));
                } else if (rundenname.equals(spieler4.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler4, true);
                    //liste aktualisieren
                    listenName1.setText(spieler1.spielerName);
                    listenPunkte1.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName2.setText(spieler2.spielerName);
                    listenPunkte2.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName3.setText(spieler3.spielerName);
                    listenPunkte3.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName4.setText(spieler4.spielerName);
                    listenPunkte4.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler5.spielerName);
                    score.setText(String.valueOf(spieler5.score));
                    pfeile.setText(String.valueOf(spieler5.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler5.durchschnitt));
                } else if (rundenname.equals(spieler5.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler5, true);
                    //liste aktualisieren
                    listenName1.setText(spieler2.spielerName);
                    listenPunkte1.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName2.setText(spieler3.spielerName);
                    listenPunkte2.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName3.setText(spieler4.spielerName);
                    listenPunkte3.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName4.setText(spieler5.spielerName);
                    listenPunkte4.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler1.spielerName);
                    score.setText(String.valueOf(spieler1.score));
                    pfeile.setText(String.valueOf(spieler1.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler1.durchschnitt));
                }
                break;
            }
            case 6:
            {
                if (!(listenvorschlag2.getText().equals(""))) {
                    listenvorschlag1.setText(listenvorschlag2.getText());
                    listenvorschlag1.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag1.setText("");
                    listenvorschlag1.setVisibility(View.GONE);
                }
                if (!(listenvorschlag3.getText().equals(""))) {
                    listenvorschlag2.setText(listenvorschlag3.getText());
                    listenvorschlag2.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag2.setText("");
                    listenvorschlag2.setVisibility(View.GONE);
                }
                if (!(listenvorschlag4.getText().equals(""))) {
                    listenvorschlag3.setText(listenvorschlag4.getText());
                    listenvorschlag3.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag3.setText("");
                    listenvorschlag3.setVisibility(View.GONE);
                }
                if (!(listenvorschlag5.getText().equals(""))) {
                    listenvorschlag4.setText(listenvorschlag5.getText());
                    listenvorschlag4.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag4.setText("");
                    listenvorschlag4.setVisibility(View.GONE);
                }

                vorschlagsetzenliste(maxPunkte-Integer.parseInt(score.getText().toString()), listenvorschlag5);

                if (rundenname.equals(spieler1.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler1, true);
                    //liste aktualisieren
                    listenName1.setText(spieler3.spielerName);
                    listenPunkte1.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName2.setText(spieler4.spielerName);
                    listenPunkte2.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName3.setText(spieler5.spielerName);
                    listenPunkte3.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName4.setText(spieler6.spielerName);
                    listenPunkte4.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName5.setText(spieler1.spielerName);
                    listenPunkte5.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler2.spielerName);
                    score.setText(String.valueOf(spieler2.score));
                    pfeile.setText(String.valueOf(spieler2.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler2.durchschnitt));
                } else if (rundenname.equals(spieler2.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler2, true);
                    //liste aktualisieren
                    listenName1.setText(spieler4.spielerName);
                    listenPunkte1.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName2.setText(spieler5.spielerName);
                    listenPunkte2.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName3.setText(spieler6.spielerName);
                    listenPunkte3.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName4.setText(spieler1.spielerName);
                    listenPunkte4.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName5.setText(spieler2.spielerName);
                    listenPunkte5.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler3.spielerName);
                    score.setText(String.valueOf(spieler3.score));
                    pfeile.setText(String.valueOf(spieler3.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler3.durchschnitt));
                } else if (rundenname.equals(spieler3.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler3, true);
                    //liste aktualisieren
                    listenName1.setText(spieler5.spielerName);
                    listenPunkte1.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName2.setText(spieler6.spielerName);
                    listenPunkte2.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName3.setText(spieler1.spielerName);
                    listenPunkte3.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName4.setText(spieler2.spielerName);
                    listenPunkte4.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName5.setText(spieler3.spielerName);
                    listenPunkte5.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler4.spielerName);
                    score.setText(String.valueOf(spieler4.score));
                    pfeile.setText(String.valueOf(spieler4.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler4.durchschnitt));
                } else if (rundenname.equals(spieler4.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler4, true);
                    //liste aktualisieren
                    listenName1.setText(spieler6.spielerName);
                    listenPunkte1.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName2.setText(spieler1.spielerName);
                    listenPunkte2.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName3.setText(spieler2.spielerName);
                    listenPunkte3.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName4.setText(spieler3.spielerName);
                    listenPunkte4.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName5.setText(spieler4.spielerName);
                    listenPunkte5.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler5.spielerName);
                    score.setText(String.valueOf(spieler5.score));
                    pfeile.setText(String.valueOf(spieler5.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler5.durchschnitt));
                } else if (rundenname.equals(spieler5.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler5, true);
                    //liste aktualisieren
                    listenName1.setText(spieler1.spielerName);
                    listenPunkte1.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName2.setText(spieler2.spielerName);
                    listenPunkte2.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName3.setText(spieler3.spielerName);
                    listenPunkte3.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName4.setText(spieler4.spielerName);
                    listenPunkte4.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName5.setText(spieler5.spielerName);
                    listenPunkte5.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler6.spielerName);
                    score.setText(String.valueOf(spieler6.score));
                    pfeile.setText(String.valueOf(spieler6.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler6.durchschnitt));
                }
                else if (rundenname.equals(spieler6.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler6, true);
                    //liste aktualisieren
                    listenName1.setText(spieler2.spielerName);
                    listenPunkte1.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName2.setText(spieler3.spielerName);
                    listenPunkte2.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName3.setText(spieler4.spielerName);
                    listenPunkte3.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName4.setText(spieler5.spielerName);
                    listenPunkte4.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName5.setText(spieler6.spielerName);
                    listenPunkte5.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler1.spielerName);
                    score.setText(String.valueOf(spieler1.score));
                    pfeile.setText(String.valueOf(spieler1.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler1.durchschnitt));
                }
                break;
            }
            case 7:
            {
                if (!(listenvorschlag2.getText().equals(""))) {
                    listenvorschlag1.setText(listenvorschlag2.getText());
                    listenvorschlag1.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag1.setText("");
                    listenvorschlag1.setVisibility(View.GONE);
                }
                if (!(listenvorschlag3.getText().equals(""))) {
                    listenvorschlag2.setText(listenvorschlag3.getText());
                    listenvorschlag2.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag2.setText("");
                    listenvorschlag2.setVisibility(View.GONE);
                }
                if (!(listenvorschlag4.getText().equals(""))) {
                    listenvorschlag3.setText(listenvorschlag4.getText());
                    listenvorschlag3.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag3.setText("");
                    listenvorschlag3.setVisibility(View.GONE);
                }
                if (!(listenvorschlag5.getText().equals(""))) {
                    listenvorschlag4.setText(listenvorschlag5.getText());
                    listenvorschlag4.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag4.setText("");
                    listenvorschlag4.setVisibility(View.GONE);
                }
                if (!(listenvorschlag6.getText().equals(""))) {
                    listenvorschlag5.setText(listenvorschlag6.getText());
                    listenvorschlag5.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag5.setText("");
                    listenvorschlag5.setVisibility(View.GONE);
                }

                vorschlagsetzenliste(maxPunkte-Integer.parseInt(score.getText().toString()), listenvorschlag6);

                if (rundenname.equals(spieler1.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler1, true);
                    //liste aktualisieren
                    listenName1.setText(spieler3.spielerName);
                    listenPunkte1.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName2.setText(spieler4.spielerName);
                    listenPunkte2.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName3.setText(spieler5.spielerName);
                    listenPunkte3.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName4.setText(spieler6.spielerName);
                    listenPunkte4.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName5.setText(spieler7.spielerName);
                    listenPunkte5.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    listenName6.setText(spieler1.spielerName);
                    listenPunkte6.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler2.spielerName);
                    score.setText(String.valueOf(spieler2.score));
                    pfeile.setText(String.valueOf(spieler2.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler2.durchschnitt));
                } else if (rundenname.equals(spieler2.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler2, true);
                    //liste aktualisieren
                    listenName1.setText(spieler4.spielerName);
                    listenPunkte1.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName2.setText(spieler5.spielerName);
                    listenPunkte2.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName3.setText(spieler6.spielerName);
                    listenPunkte3.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName4.setText(spieler7.spielerName);
                    listenPunkte4.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    listenName5.setText(spieler1.spielerName);
                    listenPunkte5.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName6.setText(spieler2.spielerName);
                    listenPunkte6.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler3.spielerName);
                    score.setText(String.valueOf(spieler3.score));
                    pfeile.setText(String.valueOf(spieler3.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler3.durchschnitt));
                } else if (rundenname.equals(spieler3.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler3, true);
                    //liste aktualisieren
                    listenName1.setText(spieler5.spielerName);
                    listenPunkte1.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName2.setText(spieler6.spielerName);
                    listenPunkte2.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName3.setText(spieler7.spielerName);
                    listenPunkte3.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    listenName4.setText(spieler1.spielerName);
                    listenPunkte4.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName5.setText(spieler2.spielerName);
                    listenPunkte5.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName6.setText(spieler3.spielerName);
                    listenPunkte6.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler4.spielerName);
                    score.setText(String.valueOf(spieler4.score));
                    pfeile.setText(String.valueOf(spieler4.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler4.durchschnitt));
                } else if (rundenname.equals(spieler4.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler4, true);
                    //liste aktualisieren
                    listenName1.setText(spieler6.spielerName);
                    listenPunkte1.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName2.setText(spieler7.spielerName);
                    listenPunkte2.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    listenName3.setText(spieler1.spielerName);
                    listenPunkte3.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName4.setText(spieler2.spielerName);
                    listenPunkte4.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName5.setText(spieler3.spielerName);
                    listenPunkte5.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName6.setText(spieler4.spielerName);
                    listenPunkte6.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler5.spielerName);
                    score.setText(String.valueOf(spieler5.score));
                    pfeile.setText(String.valueOf(spieler5.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler5.durchschnitt));
                } else if (rundenname.equals(spieler5.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler5, true);
                    //liste aktualisieren
                    listenName1.setText(spieler7.spielerName);
                    listenPunkte1.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    listenName2.setText(spieler1.spielerName);
                    listenPunkte2.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName3.setText(spieler2.spielerName);
                    listenPunkte3.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName4.setText(spieler3.spielerName);
                    listenPunkte4.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName5.setText(spieler4.spielerName);
                    listenPunkte5.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName6.setText(spieler5.spielerName);
                    listenPunkte6.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler6.spielerName);
                    score.setText(String.valueOf(spieler6.score));
                    pfeile.setText(String.valueOf(spieler6.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler6.durchschnitt));
                }
                else if (rundenname.equals(spieler6.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler6, true);
                    //liste aktualisieren
                    listenName1.setText(spieler1.spielerName);
                    listenPunkte1.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName2.setText(spieler2.spielerName);
                    listenPunkte2.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName3.setText(spieler3.spielerName);
                    listenPunkte3.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName4.setText(spieler4.spielerName);
                    listenPunkte4.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName5.setText(spieler5.spielerName);
                    listenPunkte5.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName6.setText(spieler6.spielerName);
                    listenPunkte6.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler7.spielerName);
                    score.setText(String.valueOf(spieler7.score));
                    pfeile.setText(String.valueOf(spieler7.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler7.durchschnitt));
                }
                else if (rundenname.equals(spieler7.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler7, true);
                    //liste aktualisieren
                    listenName1.setText(spieler2.spielerName);
                    listenPunkte1.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName2.setText(spieler3.spielerName);
                    listenPunkte2.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName3.setText(spieler4.spielerName);
                    listenPunkte3.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName4.setText(spieler5.spielerName);
                    listenPunkte4.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName5.setText(spieler6.spielerName);
                    listenPunkte5.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName6.setText(spieler7.spielerName);
                    listenPunkte6.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler1.spielerName);
                    score.setText(String.valueOf(spieler1.score));
                    pfeile.setText(String.valueOf(spieler1.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler1.durchschnitt));
                }
                break;
            }
            case 8:
            {
                if (!(listenvorschlag2.getText().equals(""))) {
                    listenvorschlag1.setText(listenvorschlag2.getText());
                    listenvorschlag1.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag1.setText("");
                    listenvorschlag1.setVisibility(View.GONE);
                }
                if (!(listenvorschlag3.getText().equals(""))) {
                    listenvorschlag2.setText(listenvorschlag3.getText());
                    listenvorschlag2.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag2.setText("");
                    listenvorschlag2.setVisibility(View.GONE);
                }
                if (!(listenvorschlag4.getText().equals(""))) {
                    listenvorschlag3.setText(listenvorschlag4.getText());
                    listenvorschlag3.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag3.setText("");
                    listenvorschlag3.setVisibility(View.GONE);
                }
                if (!(listenvorschlag5.getText().equals(""))) {
                    listenvorschlag4.setText(listenvorschlag5.getText());
                    listenvorschlag4.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag4.setText("");
                    listenvorschlag4.setVisibility(View.GONE);
                }
                if (!(listenvorschlag6.getText().equals(""))) {
                    listenvorschlag5.setText(listenvorschlag6.getText());
                    listenvorschlag5.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag5.setText("");
                    listenvorschlag5.setVisibility(View.GONE);
                }
                if (!(listenvorschlag7.getText().equals(""))) {
                    listenvorschlag6.setText(listenvorschlag7.getText());
                    listenvorschlag6.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag6.setText("");
                    listenvorschlag6.setVisibility(View.GONE);
                }

                vorschlagsetzenliste(maxPunkte-Integer.parseInt(score.getText().toString()), listenvorschlag7);

                if (rundenname.equals(spieler1.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler1, true);
                    //liste aktualisieren
                    listenName1.setText(spieler3.spielerName);
                    listenPunkte1.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName2.setText(spieler4.spielerName);
                    listenPunkte2.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName3.setText(spieler5.spielerName);
                    listenPunkte3.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName4.setText(spieler6.spielerName);
                    listenPunkte4.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName5.setText(spieler7.spielerName);
                    listenPunkte5.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    listenName6.setText(spieler8.spielerName);
                    listenPunkte6.setText(spieler8.score + "  Ø: " + spieler8.durchschnitt);
                    listenName7.setText(spieler1.spielerName);
                    listenPunkte7.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler2.spielerName);
                    score.setText(String.valueOf(spieler2.score));
                    pfeile.setText(String.valueOf(spieler2.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler2.durchschnitt));
                } else if (rundenname.equals(spieler2.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler2, true);
                    //liste aktualisieren
                    listenName1.setText(spieler4.spielerName);
                    listenPunkte1.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName2.setText(spieler5.spielerName);
                    listenPunkte2.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName3.setText(spieler6.spielerName);
                    listenPunkte3.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName4.setText(spieler7.spielerName);
                    listenPunkte4.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    listenName5.setText(spieler8.spielerName);
                    listenPunkte5.setText(spieler8.score + "  Ø: " + spieler8.durchschnitt);
                    listenName6.setText(spieler1.spielerName);
                    listenPunkte6.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName7.setText(spieler2.spielerName);
                    listenPunkte7.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler3.spielerName);
                    score.setText(String.valueOf(spieler3.score));
                    pfeile.setText(String.valueOf(spieler3.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler3.durchschnitt));
                } else if (rundenname.equals(spieler3.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler3, true);
                    //liste aktualisieren
                    listenName1.setText(spieler5.spielerName);
                    listenPunkte1.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName2.setText(spieler6.spielerName);
                    listenPunkte2.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName3.setText(spieler7.spielerName);
                    listenPunkte3.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    listenName4.setText(spieler8.spielerName);
                    listenPunkte4.setText(spieler8.score + "  Ø: " + spieler8.durchschnitt);
                    listenName5.setText(spieler1.spielerName);
                    listenPunkte5.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName6.setText(spieler2.spielerName);
                    listenPunkte6.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName7.setText(spieler3.spielerName);
                    listenPunkte7.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler4.spielerName);
                    score.setText(String.valueOf(spieler4.score));
                    pfeile.setText(String.valueOf(spieler4.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler4.durchschnitt));
                } else if (rundenname.equals(spieler4.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler4, true);
                    //liste aktualisieren
                    listenName1.setText(spieler6.spielerName);
                    listenPunkte1.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName2.setText(spieler7.spielerName);
                    listenPunkte2.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    listenName3.setText(spieler8.spielerName);
                    listenPunkte3.setText(spieler8.score + "  Ø: " + spieler8.durchschnitt);
                    listenName4.setText(spieler1.spielerName);
                    listenPunkte4.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName5.setText(spieler2.spielerName);
                    listenPunkte5.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName6.setText(spieler3.spielerName);
                    listenPunkte6.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName7.setText(spieler4.spielerName);
                    listenPunkte7.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler5.spielerName);
                    score.setText(String.valueOf(spieler5.score));
                    pfeile.setText(String.valueOf(spieler5.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler5.durchschnitt));
                } else if (rundenname.equals(spieler5.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler5, true);
                    //liste aktualisieren
                    listenName1.setText(spieler7.spielerName);
                    listenPunkte1.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    listenName2.setText(spieler8.spielerName);
                    listenPunkte2.setText(spieler8.score + "  Ø: " + spieler8.durchschnitt);
                    listenName3.setText(spieler1.spielerName);
                    listenPunkte3.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName4.setText(spieler2.spielerName);
                    listenPunkte4.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName5.setText(spieler3.spielerName);
                    listenPunkte5.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName6.setText(spieler4.spielerName);
                    listenPunkte6.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName7.setText(spieler5.spielerName);
                    listenPunkte7.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler6.spielerName);
                    score.setText(String.valueOf(spieler6.score));
                    pfeile.setText(String.valueOf(spieler6.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler6.durchschnitt));
                }
                else if (rundenname.equals(spieler6.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler6, true);
                    //liste aktualisieren
                    listenName1.setText(spieler8.spielerName);
                    listenPunkte1.setText(spieler8.score + "  Ø: " + spieler8.durchschnitt);
                    listenName2.setText(spieler1.spielerName);
                    listenPunkte2.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName3.setText(spieler2.spielerName);
                    listenPunkte3.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName4.setText(spieler3.spielerName);
                    listenPunkte4.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName5.setText(spieler4.spielerName);
                    listenPunkte5.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName6.setText(spieler5.spielerName);
                    listenPunkte6.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName7.setText(spieler6.spielerName);
                    listenPunkte7.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler7.spielerName);
                    score.setText(String.valueOf(spieler7.score));
                    pfeile.setText(String.valueOf(spieler7.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler7.durchschnitt));
                }
                else if (rundenname.equals(spieler7.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler7, true);
                    //liste aktualisieren
                    listenName1.setText(spieler1.spielerName);
                    listenPunkte1.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName2.setText(spieler2.spielerName);
                    listenPunkte2.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName3.setText(spieler3.spielerName);
                    listenPunkte3.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName4.setText(spieler4.spielerName);
                    listenPunkte4.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName5.setText(spieler5.spielerName);
                    listenPunkte5.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName6.setText(spieler6.spielerName);
                    listenPunkte6.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName7.setText(spieler7.spielerName);
                    listenPunkte7.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler8.spielerName);
                    score.setText(String.valueOf(spieler8.score));
                    pfeile.setText(String.valueOf(spieler8.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler8.durchschnitt));
                }
                else if (rundenname.equals(spieler8.spielerName)) {
                    //aktueller spieler - daten speichern
                    wurfstatistik(spieler8, true);
                    //liste aktualisieren
                    listenName1.setText(spieler2.spielerName);
                    listenPunkte1.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName2.setText(spieler3.spielerName);
                    listenPunkte2.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName3.setText(spieler4.spielerName);
                    listenPunkte3.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName4.setText(spieler5.spielerName);
                    listenPunkte4.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName5.setText(spieler6.spielerName);
                    listenPunkte5.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName6.setText(spieler7.spielerName);
                    listenPunkte6.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    listenName7.setText(spieler8.spielerName);
                    listenPunkte7.setText(spieler8.score + "  Ø: " + spieler8.durchschnitt);
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler1.spielerName);
                    score.setText(String.valueOf(spieler1.score));
                    pfeile.setText(String.valueOf(spieler1.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler1.durchschnitt));
                }
                break;
            }

        }

        //vorschlag "berechnen" und geworfene Pfeilwerte zurücksetzen
        eliminationsvorschlagsetzen();
        dummy2 = maxPunkte-Integer.parseInt(score.getText().toString());
        vorschlagsetzen(dummy2);
        d1.setText("-");
        d2.setText("-");
        d3.setText("-");
        xdart = 0;

        if (aktiverSpieler < spieleranzahl) aktiverSpieler++;
        else aktiverSpieler = 1;
    }

    private void lastplayer()    // setzt die anzeigen so, dass der letzte spieler wieder dran ist
    {
        rundenname = name.getText().toString();
        //nur ausführen wenn der letzte spieler schon wenigstens einmal geworfen hat.(pfeileanzahl)!!!

        switch (spieleranzahl) {
            case 1: { // nothing to do, es gibt keinen anderen spieler
                if (Integer.parseInt(pfeile.getText().toString())<=0) return;
                wurfstatistik(spieler1, false);
                break;
            }
            case 2: {
                // anzeigewerte laden
                if (rundenname.equals(spieler1.spielerName)) {
                    if (spieler2.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler1.spielerName);
                    listenPunkte1.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    dummy3 = spieler1.score;
                    //aktueller spieler aktualisieren
                    name.setText(spieler2.spielerName);
                    score.setText(String.valueOf(spieler2.score));
                    pfeile.setText(String.valueOf(spieler2.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler2.durchschnitt));
                    wurfstatistik(spieler2, false);
                } else if (rundenname.equals(spieler2.spielerName)) {
                    if (spieler1.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler2.spielerName);
                    listenPunkte1.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    dummy3 = spieler2.score;
                    //aktueller spieler aktualisieren
                    name.setText(spieler1.spielerName);
                    score.setText(String.valueOf(spieler1.score));
                    pfeile.setText(String.valueOf(spieler1.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler1.durchschnitt));
                    wurfstatistik(spieler1, false);
                }
                vorschlagsetzenliste(maxPunkte-dummy3, listenvorschlag1);
                break;
            }
            case 3: {
                if (!(listenvorschlag1.getText().equals(""))) {
                    listenvorschlag2.setText(listenvorschlag1.getText());
                    listenvorschlag2.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag2.setText("");
                    listenvorschlag2.setVisibility(View.GONE);
                }
                if (rundenname.equals(spieler1.spielerName)) {
                    if (spieler3.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler1.spielerName);
                    listenPunkte1.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName2.setText(spieler2.spielerName);
                    listenPunkte2.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    dummy3 = spieler1.score;
                    //vorherigen spieler auf aktuelle-anzeige setzen
                    name.setText(spieler3.spielerName);
                    score.setText(String.valueOf(spieler3.score));
                    pfeile.setText(String.valueOf(spieler3.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler3.durchschnitt));
                    wurfstatistik(spieler3, false);
                } else if (rundenname.equals(spieler2.spielerName)) {
                    if (spieler1.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler2.spielerName);
                    listenPunkte1.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName2.setText(spieler3.spielerName);
                    listenPunkte2.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    dummy3 = spieler2.score;
                    //vorherigen spieler auf aktuelle-anzeige setzen
                    name.setText(spieler1.spielerName);
                    score.setText(String.valueOf(spieler1.score));
                    pfeile.setText(String.valueOf(spieler1.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler1.durchschnitt));
                    wurfstatistik(spieler1, false);
                } else if (rundenname.equals(spieler3.spielerName)) {
                    if (spieler2.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler3.spielerName);
                    listenPunkte1.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName2.setText(spieler1.spielerName);
                    listenPunkte2.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    dummy3 = spieler3.score;
                    //vorherigen spieler auf aktuelle-anzeige setzen
                    name.setText(spieler2.spielerName);
                    score.setText(String.valueOf(spieler2.score));
                    pfeile.setText(String.valueOf(spieler2.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler2.durchschnitt));
                    wurfstatistik(spieler2, false);
                }
                vorschlagsetzenliste(maxPunkte-dummy3, listenvorschlag1);
                break;
            }
            case 4: {
                if (!(listenvorschlag2.getText().equals(""))) {
                    listenvorschlag3.setText(listenvorschlag2.getText());
                    listenvorschlag3.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag3.setText("");
                    listenvorschlag3.setVisibility(View.GONE);
                }

                if (!(listenvorschlag1.getText().equals(""))) {
                    listenvorschlag2.setText(listenvorschlag1.getText());
                    listenvorschlag2.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag2.setText("");
                    listenvorschlag2.setVisibility(View.GONE);
                }
                if (rundenname.equals(spieler1.spielerName)) {
                    //liste aktualisieren
                    if (spieler4.geworfenePfeile == 0) return;
                    listenName1.setText(spieler1.spielerName);
                    listenPunkte1.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName2.setText(spieler2.spielerName);
                    listenPunkte2.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName3.setText(spieler3.spielerName);
                    listenPunkte3.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    dummy3 = spieler1.score;
                    //vorherigen spieler auf aktuelle-anzeige setzen
                    name.setText(spieler4.spielerName);
                    score.setText(String.valueOf(spieler4.score));
                    pfeile.setText(String.valueOf(spieler4.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler4.durchschnitt));
                    wurfstatistik(spieler4, false);
                } else if (rundenname.equals(spieler2.spielerName)) {
                    if (spieler1.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler2.spielerName);
                    listenPunkte1.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName2.setText(spieler3.spielerName);
                    listenPunkte2.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName3.setText(spieler4.spielerName);
                    listenPunkte3.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    dummy3 = spieler2.score;
                    //vorherigen spieler auf aktuelle-anzeige setzen
                    name.setText(spieler1.spielerName);
                    score.setText(String.valueOf(spieler1.score));
                    pfeile.setText(String.valueOf(spieler1.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler1.durchschnitt));
                    wurfstatistik(spieler1, false);
                } else if (rundenname.equals(spieler3.spielerName)) {
                    if (spieler2.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler3.spielerName);
                    listenPunkte1.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName2.setText(spieler4.spielerName);
                    listenPunkte2.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName3.setText(spieler1.spielerName);
                    listenPunkte3.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    dummy3 = spieler3.score;
                    //vorherigen spieler auf aktuelle-anzeige setzen
                    name.setText(spieler2.spielerName);
                    score.setText(String.valueOf(spieler2.score));
                    pfeile.setText(String.valueOf(spieler2.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler2.durchschnitt));
                    wurfstatistik(spieler2, false);
                } else if (rundenname.equals(spieler4.spielerName)) {
                    if (spieler3.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler4.spielerName);
                    listenPunkte1.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName2.setText(spieler1.spielerName);
                    listenPunkte2.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName3.setText(spieler2.spielerName);
                    listenPunkte3.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    dummy3 = spieler4.score;
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler3.spielerName);
                    score.setText(String.valueOf(spieler3.score));
                    pfeile.setText(String.valueOf(spieler3.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler3.durchschnitt));
                    wurfstatistik(spieler3, false);
                }
                vorschlagsetzenliste(maxPunkte-dummy3, listenvorschlag1);
                break;
            }
            case 5:
            {
                if (!(listenvorschlag3.getText().equals(""))) {
                    listenvorschlag4.setText(listenvorschlag3.getText());
                    listenvorschlag4.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag4.setText("");
                    listenvorschlag4.setVisibility(View.GONE);
                }

                if (!(listenvorschlag2.getText().equals(""))) {
                    listenvorschlag3.setText(listenvorschlag2.getText());
                    listenvorschlag3.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag3.setText("");
                    listenvorschlag3.setVisibility(View.GONE);
                }

                if (!(listenvorschlag1.getText().equals(""))) {
                    listenvorschlag2.setText(listenvorschlag1.getText());
                    listenvorschlag2.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag2.setText("");
                    listenvorschlag2.setVisibility(View.GONE);
                }

                if (rundenname.equals(spieler1.spielerName)) {
                    //liste aktualisieren
                    if (spieler5.geworfenePfeile == 0) return;
                    listenName1.setText(spieler1.spielerName);
                    listenPunkte1.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName2.setText(spieler2.spielerName);
                    listenPunkte2.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName3.setText(spieler3.spielerName);
                    listenPunkte3.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName4.setText(spieler4.spielerName);
                    listenPunkte4.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    dummy3 = spieler1.score;
                    //vorherigen spieler auf aktuelle-anzeige setzen
                    name.setText(spieler5.spielerName);
                    score.setText(String.valueOf(spieler5.score));
                    pfeile.setText(String.valueOf(spieler5.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler5.durchschnitt));
                    wurfstatistik(spieler5, false);
                } else if (rundenname.equals(spieler2.spielerName)) {
                    if (spieler1.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler2.spielerName);
                    listenPunkte1.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName2.setText(spieler3.spielerName);
                    listenPunkte2.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName3.setText(spieler4.spielerName);
                    listenPunkte3.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName4.setText(spieler5.spielerName);
                    listenPunkte4.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    dummy3 = spieler2.score;
                    //vorherigen spieler auf aktuelle-anzeige setzen
                    name.setText(spieler1.spielerName);
                    score.setText(String.valueOf(spieler1.score));
                    pfeile.setText(String.valueOf(spieler1.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler1.durchschnitt));
                    wurfstatistik(spieler1, false);
                } else if (rundenname.equals(spieler3.spielerName)) {
                    if (spieler2.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler3.spielerName);
                    listenPunkte1.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName2.setText(spieler4.spielerName);
                    listenPunkte2.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName3.setText(spieler5.spielerName);
                    listenPunkte3.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName4.setText(spieler1.spielerName);
                    listenPunkte4.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    dummy3 = spieler3.score;
                    //vorherigen spieler auf aktuelle-anzeige setzen
                    name.setText(spieler2.spielerName);
                    score.setText(String.valueOf(spieler2.score));
                    pfeile.setText(String.valueOf(spieler2.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler2.durchschnitt));
                    wurfstatistik(spieler2, false);
                } else if (rundenname.equals(spieler4.spielerName)) {
                    if (spieler3.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler4.spielerName);
                    listenPunkte1.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName2.setText(spieler5.spielerName);
                    listenPunkte2.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName3.setText(spieler1.spielerName);
                    listenPunkte3.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName4.setText(spieler2.spielerName);
                    listenPunkte4.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    dummy3 = spieler4.score;
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler3.spielerName);
                    score.setText(String.valueOf(spieler3.score));
                    pfeile.setText(String.valueOf(spieler3.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler3.durchschnitt));
                    wurfstatistik(spieler3, false);
                }
                else if (rundenname.equals(spieler5.spielerName)) {
                    if (spieler4.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler5.spielerName);
                    listenPunkte1.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName2.setText(spieler1.spielerName);
                    listenPunkte2.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName3.setText(spieler2.spielerName);
                    listenPunkte3.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName4.setText(spieler3.spielerName);
                    listenPunkte4.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    dummy3 = spieler5.score;
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler4.spielerName);
                    score.setText(String.valueOf(spieler4.score));
                    pfeile.setText(String.valueOf(spieler4.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler4.durchschnitt));
                    wurfstatistik(spieler4, false);
                }
                vorschlagsetzenliste(maxPunkte-dummy3, listenvorschlag1);
                break;
            }
            case 6:
            {
                if (!(listenvorschlag4.getText().equals(""))) {
                    listenvorschlag5.setText(listenvorschlag4.getText());
                    listenvorschlag5.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag5.setText("");
                    listenvorschlag5.setVisibility(View.GONE);
                }
                if (!(listenvorschlag3.getText().equals(""))) {
                    listenvorschlag4.setText(listenvorschlag3.getText());
                    listenvorschlag4.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag4.setText("");
                    listenvorschlag4.setVisibility(View.GONE);
                }

                if (!(listenvorschlag2.getText().equals(""))) {
                    listenvorschlag3.setText(listenvorschlag2.getText());
                    listenvorschlag3.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag3.setText("");
                    listenvorschlag3.setVisibility(View.GONE);
                }

                if (!(listenvorschlag1.getText().equals(""))) {
                    listenvorschlag2.setText(listenvorschlag1.getText());
                    listenvorschlag2.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag2.setText("");
                    listenvorschlag2.setVisibility(View.GONE);
                }

                if (rundenname.equals(spieler1.spielerName)) {
                    //liste aktualisieren
                    if (spieler6.geworfenePfeile == 0) return;
                    listenName1.setText(spieler1.spielerName);
                    listenPunkte1.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName2.setText(spieler2.spielerName);
                    listenPunkte2.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName3.setText(spieler3.spielerName);
                    listenPunkte3.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName4.setText(spieler4.spielerName);
                    listenPunkte4.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName5.setText(spieler5.spielerName);
                    listenPunkte5.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    dummy3 = spieler1.score;
                    //vorherigen spieler auf aktuelle-anzeige setzen
                    name.setText(spieler6.spielerName);
                    score.setText(String.valueOf(spieler6.score));
                    pfeile.setText(String.valueOf(spieler6.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler6.durchschnitt));
                    wurfstatistik(spieler6, false);
                } else if (rundenname.equals(spieler2.spielerName)) {
                    if (spieler1.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler2.spielerName);
                    listenPunkte1.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName2.setText(spieler3.spielerName);
                    listenPunkte2.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName3.setText(spieler4.spielerName);
                    listenPunkte3.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName4.setText(spieler5.spielerName);
                    listenPunkte4.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName5.setText(spieler6.spielerName);
                    listenPunkte5.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    dummy3 = spieler2.score;
                    //vorherigen spieler auf aktuelle-anzeige setzen
                    name.setText(spieler1.spielerName);
                    score.setText(String.valueOf(spieler1.score));
                    pfeile.setText(String.valueOf(spieler1.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler1.durchschnitt));
                    wurfstatistik(spieler1, false);
                } else if (rundenname.equals(spieler3.spielerName)) {
                    if (spieler2.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler3.spielerName);
                    listenPunkte1.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName2.setText(spieler4.spielerName);
                    listenPunkte2.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName3.setText(spieler5.spielerName);
                    listenPunkte3.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName4.setText(spieler6.spielerName);
                    listenPunkte4.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName5.setText(spieler1.spielerName);
                    listenPunkte5.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    dummy3 = spieler3.score;
                    //vorherigen spieler auf aktuelle-anzeige setzen
                    name.setText(spieler2.spielerName);
                    score.setText(String.valueOf(spieler2.score));
                    pfeile.setText(String.valueOf(spieler2.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler2.durchschnitt));
                    wurfstatistik(spieler2, false);
                } else if (rundenname.equals(spieler4.spielerName)) {
                    if (spieler3.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler4.spielerName);
                    listenPunkte1.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName2.setText(spieler5.spielerName);
                    listenPunkte2.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName3.setText(spieler6.spielerName);
                    listenPunkte3.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName4.setText(spieler1.spielerName);
                    listenPunkte4.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName5.setText(spieler2.spielerName);
                    listenPunkte5.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    dummy3 = spieler4.score;
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler3.spielerName);
                    score.setText(String.valueOf(spieler3.score));
                    pfeile.setText(String.valueOf(spieler3.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler3.durchschnitt));
                    wurfstatistik(spieler3, false);
                }
                else if (rundenname.equals(spieler5.spielerName)) {
                    if (spieler4.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler5.spielerName);
                    listenPunkte1.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName2.setText(spieler6.spielerName);
                    listenPunkte2.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName3.setText(spieler1.spielerName);
                    listenPunkte3.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName4.setText(spieler2.spielerName);
                    listenPunkte4.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName5.setText(spieler3.spielerName);
                    listenPunkte5.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    dummy3 = spieler5.score;
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler4.spielerName);
                    score.setText(String.valueOf(spieler4.score));
                    pfeile.setText(String.valueOf(spieler4.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler4.durchschnitt));
                    wurfstatistik(spieler4, false);
                }
                else if (rundenname.equals(spieler6.spielerName)) {
                if (spieler5.geworfenePfeile == 0) return;
                //liste aktualisieren
                listenName1.setText(spieler6.spielerName);
                listenPunkte1.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                listenName2.setText(spieler1.spielerName);
                listenPunkte2.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                listenName3.setText(spieler2.spielerName);
                listenPunkte3.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                listenName4.setText(spieler3.spielerName);
                listenPunkte4.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                listenName5.setText(spieler4.spielerName);
                listenPunkte5.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                dummy3 = spieler6.score;
                //nächsten spieler auf aktuelle-anzeige setzen
                name.setText(spieler5.spielerName);
                score.setText(String.valueOf(spieler5.score));
                pfeile.setText(String.valueOf(spieler5.geworfenePfeile));
                durchschnitt.setText(Float.toString(spieler5.durchschnitt));
                wurfstatistik(spieler5, false);
            }
                vorschlagsetzenliste(maxPunkte-dummy3, listenvorschlag1);
                break;
            }
            case 7:
            {
                if (!(listenvorschlag5.getText().equals(""))) {
                    listenvorschlag6.setText(listenvorschlag5.getText());
                    listenvorschlag6.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag6.setText("");
                    listenvorschlag6.setVisibility(View.GONE);
                }
                if (!(listenvorschlag4.getText().equals(""))) {
                    listenvorschlag5.setText(listenvorschlag4.getText());
                    listenvorschlag5.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag5.setText("");
                    listenvorschlag5.setVisibility(View.GONE);
                }
                if (!(listenvorschlag3.getText().equals(""))) {
                    listenvorschlag4.setText(listenvorschlag3.getText());
                    listenvorschlag4.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag4.setText("");
                    listenvorschlag4.setVisibility(View.GONE);
                }

                if (!(listenvorschlag2.getText().equals(""))) {
                    listenvorschlag3.setText(listenvorschlag2.getText());
                    listenvorschlag3.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag3.setText("");
                    listenvorschlag3.setVisibility(View.GONE);
                }

                if (!(listenvorschlag1.getText().equals(""))) {
                    listenvorschlag2.setText(listenvorschlag1.getText());
                    listenvorschlag2.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag2.setText("");
                    listenvorschlag2.setVisibility(View.GONE);
                }

                if (rundenname.equals(spieler1.spielerName)) {
                    //liste aktualisieren
                    if (spieler7.geworfenePfeile == 0) return;
                    listenName1.setText(spieler1.spielerName);
                    listenPunkte1.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName2.setText(spieler2.spielerName);
                    listenPunkte2.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName3.setText(spieler3.spielerName);
                    listenPunkte3.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName4.setText(spieler4.spielerName);
                    listenPunkte4.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName5.setText(spieler5.spielerName);
                    listenPunkte5.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName6.setText(spieler6.spielerName);
                    listenPunkte6.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    dummy3 = spieler1.score;
                    //vorherigen spieler auf aktuelle-anzeige setzen
                    name.setText(spieler7.spielerName);
                    score.setText(String.valueOf(spieler7.score));
                    pfeile.setText(String.valueOf(spieler7.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler7.durchschnitt));
                    wurfstatistik(spieler7, false);
                } else if (rundenname.equals(spieler2.spielerName)) {
                    if (spieler1.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler2.spielerName);
                    listenPunkte1.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName2.setText(spieler3.spielerName);
                    listenPunkte2.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName3.setText(spieler4.spielerName);
                    listenPunkte3.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName4.setText(spieler5.spielerName);
                    listenPunkte4.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName5.setText(spieler6.spielerName);
                    listenPunkte5.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName6.setText(spieler7.spielerName);
                    listenPunkte6.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    dummy3 = spieler2.score;
                    //vorherigen spieler auf aktuelle-anzeige setzen
                    name.setText(spieler1.spielerName);
                    score.setText(String.valueOf(spieler1.score));
                    pfeile.setText(String.valueOf(spieler1.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler1.durchschnitt));
                    wurfstatistik(spieler1, false);
                } else if (rundenname.equals(spieler3.spielerName)) {
                    if (spieler2.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler3.spielerName);
                    listenPunkte1.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName2.setText(spieler4.spielerName);
                    listenPunkte2.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName3.setText(spieler5.spielerName);
                    listenPunkte3.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName4.setText(spieler6.spielerName);
                    listenPunkte4.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName5.setText(spieler7.spielerName);
                    listenPunkte5.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    listenName6.setText(spieler1.spielerName);
                    listenPunkte6.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    dummy3 = spieler3.score;
                    //vorherigen spieler auf aktuelle-anzeige setzen
                    name.setText(spieler2.spielerName);
                    score.setText(String.valueOf(spieler2.score));
                    pfeile.setText(String.valueOf(spieler2.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler2.durchschnitt));
                    wurfstatistik(spieler2, false);
                } else if (rundenname.equals(spieler4.spielerName)) {
                    if (spieler3.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler4.spielerName);
                    listenPunkte1.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName2.setText(spieler5.spielerName);
                    listenPunkte2.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName3.setText(spieler6.spielerName);
                    listenPunkte3.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName4.setText(spieler7.spielerName);
                    listenPunkte4.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    listenName5.setText(spieler1.spielerName);
                    listenPunkte5.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName6.setText(spieler2.spielerName);
                    listenPunkte6.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    dummy3 = spieler4.score;
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler3.spielerName);
                    score.setText(String.valueOf(spieler3.score));
                    pfeile.setText(String.valueOf(spieler3.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler3.durchschnitt));
                    wurfstatistik(spieler3, false);
                }
                else if (rundenname.equals(spieler5.spielerName)) {
                    if (spieler4.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler5.spielerName);
                    listenPunkte1.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName2.setText(spieler6.spielerName);
                    listenPunkte2.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName3.setText(spieler7.spielerName);
                    listenPunkte3.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    listenName4.setText(spieler1.spielerName);
                    listenPunkte4.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName5.setText(spieler2.spielerName);
                    listenPunkte5.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName6.setText(spieler3.spielerName);
                    listenPunkte6.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    dummy3 = spieler5.score;
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler4.spielerName);
                    score.setText(String.valueOf(spieler4.score));
                    pfeile.setText(String.valueOf(spieler4.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler4.durchschnitt));
                    wurfstatistik(spieler4, false);
                }
                else if (rundenname.equals(spieler6.spielerName)) {
                    if (spieler5.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler6.spielerName);
                    listenPunkte1.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName2.setText(spieler7.spielerName);
                    listenPunkte2.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    listenName3.setText(spieler1.spielerName);
                    listenPunkte3.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName4.setText(spieler2.spielerName);
                    listenPunkte4.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName5.setText(spieler3.spielerName);
                    listenPunkte5.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName6.setText(spieler4.spielerName);
                    listenPunkte6.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    dummy3 = spieler6.score;
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler5.spielerName);
                    score.setText(String.valueOf(spieler5.score));
                    pfeile.setText(String.valueOf(spieler5.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler5.durchschnitt));
                    wurfstatistik(spieler5, false);
                }
                else if (rundenname.equals(spieler7.spielerName)) {
                    if (spieler6.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler7.spielerName);
                    listenPunkte1.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    listenName2.setText(spieler1.spielerName);
                    listenPunkte2.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName3.setText(spieler2.spielerName);
                    listenPunkte3.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName4.setText(spieler3.spielerName);
                    listenPunkte4.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName5.setText(spieler4.spielerName);
                    listenPunkte5.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName6.setText(spieler5.spielerName);
                    listenPunkte6.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    dummy3 = spieler7.score;
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler6.spielerName);
                    score.setText(String.valueOf(spieler6.score));
                    pfeile.setText(String.valueOf(spieler6.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler6.durchschnitt));
                    wurfstatistik(spieler6, false);
                }
                vorschlagsetzenliste(maxPunkte-dummy3, listenvorschlag1);
                break;
            }
            case 8:
            {
                if (!(listenvorschlag6.getText().equals(""))) {
                    listenvorschlag7.setText(listenvorschlag6.getText());
                    listenvorschlag7.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag7.setText("");
                    listenvorschlag7.setVisibility(View.GONE);
                }
                if (!(listenvorschlag5.getText().equals(""))) {
                    listenvorschlag6.setText(listenvorschlag5.getText());
                    listenvorschlag6.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag6.setText("");
                    listenvorschlag6.setVisibility(View.GONE);
                }
                if (!(listenvorschlag4.getText().equals(""))) {
                    listenvorschlag5.setText(listenvorschlag4.getText());
                    listenvorschlag5.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag5.setText("");
                    listenvorschlag5.setVisibility(View.GONE);
                }
                if (!(listenvorschlag3.getText().equals(""))) {
                    listenvorschlag4.setText(listenvorschlag3.getText());
                    listenvorschlag4.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag4.setText("");
                    listenvorschlag4.setVisibility(View.GONE);
                }

                if (!(listenvorschlag2.getText().equals(""))) {
                    listenvorschlag3.setText(listenvorschlag2.getText());
                    listenvorschlag3.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag3.setText("");
                    listenvorschlag3.setVisibility(View.GONE);
                }

                if (!(listenvorschlag1.getText().equals(""))) {
                    listenvorschlag2.setText(listenvorschlag1.getText());
                    listenvorschlag2.setVisibility(View.VISIBLE);
                } else {
                    listenvorschlag2.setText("");
                    listenvorschlag2.setVisibility(View.GONE);
                }

                if (rundenname.equals(spieler1.spielerName)) {
                    //liste aktualisieren
                    if (spieler8.geworfenePfeile == 0) return;
                    listenName1.setText(spieler1.spielerName);
                    listenPunkte1.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName2.setText(spieler2.spielerName);
                    listenPunkte2.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName3.setText(spieler3.spielerName);
                    listenPunkte3.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName4.setText(spieler4.spielerName);
                    listenPunkte4.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName5.setText(spieler5.spielerName);
                    listenPunkte5.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName6.setText(spieler6.spielerName);
                    listenPunkte6.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName7.setText(spieler7.spielerName);
                    listenPunkte7.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    dummy3 = spieler1.score;
                    //vorherigen spieler auf aktuelle-anzeige setzen
                    name.setText(spieler8.spielerName);
                    score.setText(String.valueOf(spieler8.score));
                    pfeile.setText(String.valueOf(spieler8.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler8.durchschnitt));
                    wurfstatistik(spieler8, false);
                } else if (rundenname.equals(spieler2.spielerName)) {
                    if (spieler1.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler2.spielerName);
                    listenPunkte1.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName2.setText(spieler3.spielerName);
                    listenPunkte2.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName3.setText(spieler4.spielerName);
                    listenPunkte3.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName4.setText(spieler5.spielerName);
                    listenPunkte4.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName5.setText(spieler6.spielerName);
                    listenPunkte5.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName6.setText(spieler7.spielerName);
                    listenPunkte6.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    listenName7.setText(spieler8.spielerName);
                    listenPunkte7.setText(spieler8.score + "  Ø: " + spieler8.durchschnitt);
                    dummy3 = spieler2.score;
                    //vorherigen spieler auf aktuelle-anzeige setzen
                    name.setText(spieler1.spielerName);
                    score.setText(String.valueOf(spieler1.score));
                    pfeile.setText(String.valueOf(spieler1.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler1.durchschnitt));
                    wurfstatistik(spieler1, false);
                } else if (rundenname.equals(spieler3.spielerName)) {
                    if (spieler2.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler3.spielerName);
                    listenPunkte1.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName2.setText(spieler4.spielerName);
                    listenPunkte2.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName3.setText(spieler5.spielerName);
                    listenPunkte3.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName4.setText(spieler6.spielerName);
                    listenPunkte4.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName5.setText(spieler7.spielerName);
                    listenPunkte5.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    listenName6.setText(spieler8.spielerName);
                    listenPunkte6.setText(spieler8.score + "  Ø: " + spieler8.durchschnitt);
                    listenName7.setText(spieler1.spielerName);
                    listenPunkte7.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    dummy3 = spieler3.score;
                    //vorherigen spieler auf aktuelle-anzeige setzen
                    name.setText(spieler2.spielerName);
                    score.setText(String.valueOf(spieler2.score));
                    pfeile.setText(String.valueOf(spieler2.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler2.durchschnitt));
                    wurfstatistik(spieler2, false);
                } else if (rundenname.equals(spieler4.spielerName)) {
                    if (spieler3.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler4.spielerName);
                    listenPunkte1.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName2.setText(spieler5.spielerName);
                    listenPunkte2.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName3.setText(spieler6.spielerName);
                    listenPunkte3.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName4.setText(spieler7.spielerName);
                    listenPunkte4.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    listenName5.setText(spieler8.spielerName);
                    listenPunkte5.setText(spieler8.score + "  Ø: " + spieler8.durchschnitt);
                    listenName6.setText(spieler1.spielerName);
                    listenPunkte6.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName7.setText(spieler2.spielerName);
                    listenPunkte7.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    dummy3 = spieler4.score;
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler3.spielerName);
                    score.setText(String.valueOf(spieler3.score));
                    pfeile.setText(String.valueOf(spieler3.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler3.durchschnitt));
                    wurfstatistik(spieler3, false);
                }
                else if (rundenname.equals(spieler5.spielerName)) {
                    if (spieler4.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler5.spielerName);
                    listenPunkte1.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName2.setText(spieler6.spielerName);
                    listenPunkte2.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName3.setText(spieler7.spielerName);
                    listenPunkte3.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    listenName4.setText(spieler8.spielerName);
                    listenPunkte4.setText(spieler8.score + "  Ø: " + spieler8.durchschnitt);
                    listenName5.setText(spieler1.spielerName);
                    listenPunkte5.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName6.setText(spieler2.spielerName);
                    listenPunkte6.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName7.setText(spieler3.spielerName);
                    listenPunkte7.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    dummy3 = spieler5.score;
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler4.spielerName);
                    score.setText(String.valueOf(spieler4.score));
                    pfeile.setText(String.valueOf(spieler4.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler4.durchschnitt));
                    wurfstatistik(spieler4, false);
                }
                else if (rundenname.equals(spieler6.spielerName)) {
                    if (spieler5.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler6.spielerName);
                    listenPunkte1.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    listenName2.setText(spieler7.spielerName);
                    listenPunkte2.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    listenName3.setText(spieler8.spielerName);
                    listenPunkte3.setText(spieler8.score + "  Ø: " + spieler8.durchschnitt);
                    listenName4.setText(spieler1.spielerName);
                    listenPunkte4.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName5.setText(spieler2.spielerName);
                    listenPunkte5.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName6.setText(spieler3.spielerName);
                    listenPunkte6.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName7.setText(spieler4.spielerName);
                    listenPunkte7.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    dummy3 = spieler6.score;
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler5.spielerName);
                    score.setText(String.valueOf(spieler5.score));
                    pfeile.setText(String.valueOf(spieler5.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler5.durchschnitt));
                    wurfstatistik(spieler5, false);
                }
                else if (rundenname.equals(spieler7.spielerName)) {
                    if (spieler6.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler7.spielerName);
                    listenPunkte1.setText(spieler7.score + "  Ø: " + spieler7.durchschnitt);
                    listenName2.setText(spieler8.spielerName);
                    listenPunkte2.setText(spieler8.score + "  Ø: " + spieler8.durchschnitt);
                    listenName3.setText(spieler1.spielerName);
                    listenPunkte3.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName4.setText(spieler2.spielerName);
                    listenPunkte4.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName5.setText(spieler3.spielerName);
                    listenPunkte5.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName6.setText(spieler4.spielerName);
                    listenPunkte6.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName7.setText(spieler5.spielerName);
                    listenPunkte7.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    dummy3 = spieler7.score;
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler6.spielerName);
                    score.setText(String.valueOf(spieler6.score));
                    pfeile.setText(String.valueOf(spieler6.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler6.durchschnitt));
                    wurfstatistik(spieler6, false);
                }
                else if (rundenname.equals(spieler8.spielerName)) {
                    if (spieler7.geworfenePfeile == 0) return;
                    //liste aktualisieren
                    listenName1.setText(spieler8.spielerName);
                    listenPunkte1.setText(spieler8.score + "  Ø: " + spieler8.durchschnitt);
                    listenName2.setText(spieler1.spielerName);
                    listenPunkte2.setText(spieler1.score + "  Ø: " + spieler1.durchschnitt);
                    listenName3.setText(spieler2.spielerName);
                    listenPunkte3.setText(spieler2.score + "  Ø: " + spieler2.durchschnitt);
                    listenName4.setText(spieler3.spielerName);
                    listenPunkte4.setText(spieler3.score + "  Ø: " + spieler3.durchschnitt);
                    listenName5.setText(spieler4.spielerName);
                    listenPunkte5.setText(spieler4.score + "  Ø: " + spieler4.durchschnitt);
                    listenName6.setText(spieler5.spielerName);
                    listenPunkte6.setText(spieler5.score + "  Ø: " + spieler5.durchschnitt);
                    listenName7.setText(spieler6.spielerName);
                    listenPunkte7.setText(spieler6.score + "  Ø: " + spieler6.durchschnitt);
                    dummy3 = spieler8.score;
                    //nächsten spieler auf aktuelle-anzeige setzen
                    name.setText(spieler7.spielerName);
                    score.setText(String.valueOf(spieler7.score));
                    pfeile.setText(String.valueOf(spieler7.geworfenePfeile));
                    durchschnitt.setText(Float.toString(spieler7.durchschnitt));
                    wurfstatistik(spieler7, false);
                }
                eliminationsvorschlagsetzen();
                vorschlagsetzenliste(maxPunkte-dummy3, listenvorschlag1);
                break;
            }

            
        }

        //vorschlag "berechnen" und geworfene Pfeilwerte zurücksetzen
        dummy2 = maxPunkte-Integer.parseInt(score.getText().toString());
        vorschlagsetzen(dummy2);
        // laden der zuletzt geworfenen 3 darts (inkl. D u T anzeige)
    //    setLast3zeiger(false);
       //alte version
        // switch (last3[last3zeiger]) {
        switch (wuerfe.get(wuerfe.size()-1).faktor){

        case 1: {
                dot = "";
                break;
            }
            case 2: {
                dot = "D";
                break;
            }
            case 3: {
                dot = "T";
                break;
            }
        }
      /*  d3.setText(dot + last3[last3zeiger + 1]);
        setLast3zeiger(false);
        switch (last3[last3zeiger]) {
        */
        d3.setText(dot+wuerfe.get(wuerfe.size()-1).zahl);
        switch (wuerfe.get(wuerfe.size()-2).faktor){
            case 1: {
                dot = "";
                break;
            }
            case 2: {
                dot = "D";
                break;
            }
            case 3: {
                dot = "T";
                break;
            }
        }
        d2.setText(dot+wuerfe.get(wuerfe.size()-2).zahl);
/*
        d2.setText(dot + last3[last3zeiger + 1]);
        setLast3zeiger(false);
        switch (last3[last3zeiger]) {
  */
        switch (wuerfe.get(wuerfe.size()-3).faktor){

            case 1: {
                dot = "";
                break;
            }
            case 2: {
                dot = "D";
                break;
            }
            case 3: {
                dot = "T";
                break;
            }
        }
        d1.setText(dot+wuerfe.get(wuerfe.size()-3).zahl);

     /*   d1.setText(dot + last3[last3zeiger + 1]);
        setLast3zeiger(true);
        setLast3zeiger(true);
        setLast3zeiger(true);
       */
        xdart = 3;

        rundenname = name.getText().toString();

        if (aktiverSpieler > 1) aktiverSpieler--;
        else aktiverSpieler = spieleranzahl;
    }


    private void spielerwertespeichern() {

        rundenname = name.getText().toString();
        switch (spieleranzahl) {
            case 1: {
                // aktuelle anzeigewerte zwischenspeichern
                wurfstatistik(spieler1, true);
                break;
            }
            case 2: {
                // anzeigewerte speichern
                if (rundenname.equals(spieler1.spielerName)) {
                    // aktuelle anzeigewerte zwischenspeichern
                    wurfstatistik(spieler1, true);



                } else if (rundenname.equals(spieler2.spielerName)) {
                    wurfstatistik(spieler2, true);

                }
                break;
            }
            case 3: {
                if (rundenname.equals(spieler1.spielerName)) {
                    wurfstatistik(spieler1, true);

                } else if (rundenname.equals(spieler2.spielerName)) {
                    wurfstatistik(spieler2, true);

                } else if (rundenname.equals(spieler3.spielerName)) {
                    wurfstatistik(spieler3, true);

                }
                break;
            }
            case 4: {
                if (rundenname.equals(spieler1.spielerName)) {
                    wurfstatistik(spieler1, true);

                } else if (rundenname.equals(spieler2.spielerName)) {
                    wurfstatistik(spieler2, true);

                } else if (rundenname.equals(spieler3.spielerName)) {
                    wurfstatistik(spieler3, true);

                } else if (rundenname.equals(spieler4.spielerName)) {
                    wurfstatistik(spieler4, true);

                }
                break;
            }
            case 5: {
                if (rundenname.equals(spieler1.spielerName)) {
                    wurfstatistik(spieler1, true);

                } else if (rundenname.equals(spieler2.spielerName)) {
                    wurfstatistik(spieler2, true);

                } else if (rundenname.equals(spieler3.spielerName)) {
                    wurfstatistik(spieler3, true);

                } else if (rundenname.equals(spieler4.spielerName)) {
                    wurfstatistik(spieler4, true);

                } else if (rundenname.equals(spieler5.spielerName)) {
                    wurfstatistik(spieler5, true);

                }
                break;
            }
            case 6: {
                if (rundenname.equals(spieler1.spielerName)) {
                    wurfstatistik(spieler1, true);

                } else if (rundenname.equals(spieler2.spielerName)) {
                    wurfstatistik(spieler2, true);

                } else if (rundenname.equals(spieler3.spielerName)) {
                    wurfstatistik(spieler3, true);

                } else if (rundenname.equals(spieler4.spielerName)) {
                    wurfstatistik(spieler4, true);

                } else if (rundenname.equals(spieler5.spielerName)) {
                    wurfstatistik(spieler5, true);

                } else if (rundenname.equals(spieler6.spielerName)) {
                    wurfstatistik(spieler6, true);

                }
                break;
            }
            case 7: {
                if (rundenname.equals(spieler1.spielerName)) {
                    wurfstatistik(spieler1, true);

                } else if (rundenname.equals(spieler2.spielerName)) {
                    wurfstatistik(spieler2, true);

                } else if (rundenname.equals(spieler3.spielerName)) {
                    wurfstatistik(spieler3, true);

                } else if (rundenname.equals(spieler4.spielerName)) {
                    wurfstatistik(spieler4, true);

                } else if (rundenname.equals(spieler5.spielerName)) {
                    wurfstatistik(spieler5, true);

                } else if (rundenname.equals(spieler6.spielerName)) {
                    wurfstatistik(spieler6, true);

                } else if (rundenname.equals(spieler7.spielerName)) {
                    wurfstatistik(spieler7, true);

                }
                break;
            }
            case 8: {
                if (rundenname.equals(spieler1.spielerName)) {
                    wurfstatistik(spieler1, true);

                } else if (rundenname.equals(spieler2.spielerName)) {
                    wurfstatistik(spieler2, true);

                } else if (rundenname.equals(spieler3.spielerName)) {
                    wurfstatistik(spieler3, true);

                } else if (rundenname.equals(spieler4.spielerName)) {
                    wurfstatistik(spieler4, true);

                } else if (rundenname.equals(spieler5.spielerName)) {
                    wurfstatistik(spieler5, true);

                } else if (rundenname.equals(spieler6.spielerName)) {
                    wurfstatistik(spieler6, true);

                } else if (rundenname.equals(spieler7.spielerName)) {
                    wurfstatistik(spieler7, true);

                } else if (rundenname.equals(spieler8.spielerName)) {
                    wurfstatistik(spieler8, true);

                }
                break;
            }
        }

    }

    // speicherzeiger einen dart vor oder zurücksetzen
    private void setLast3zeiger(boolean plusminus)  //true=add, false=sub
    {
        if (plusminus) {
            if (last3zeiger + 3 > 6 * maxundo) last3zeiger = 1;
            else {
                last3zeiger++;
                last3zeiger++;
                last3zeiger++;
            }
        } else {
            if (last3zeiger - 3 < 1) last3zeiger = ((6 * maxundo) - 2);
            else {
                last3zeiger--;
                last3zeiger--;
                last3zeiger--;
            }
        }
    }

    public void onBackPressed() {

        AlertDialog alertDialog = new AlertDialog.Builder(elimination.this).create();
        alertDialog.setTitle(getResources().getString(R.string.achtung));
        alertDialog.setMessage(getResources().getString(R.string.willstduverlassen));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.jaichw),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.zuruck), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
        alertDialog.show();
    }

    private void vorschlagsetzen(int mydummy) {
        if (mydummy > 0 && mydummy <= 170 && mydummy != 169 && mydummy != 168 && mydummy != 166 && mydummy != 165 && mydummy != 163 && mydummy != 162 && mydummy != 159) {
            if (checkouts[mydummy][2].isEmpty()) {
                v1.setText("( " + checkouts[mydummy][1] + " )");
                v2.setText(checkouts[mydummy][2]);
                v3.setText(checkouts[mydummy][3]);
                v1.setVisibility(View.VISIBLE);
                v2.setVisibility(View.GONE);
                v3.setVisibility(View.GONE);
            } else {
                v1.setText("( " + checkouts[mydummy][1]);
                if (checkouts[mydummy][3].isEmpty()) {
                    v2.setText(checkouts[mydummy][2] + " )");
                    v3.setText(checkouts[mydummy][3]);
                    v1.setVisibility(View.VISIBLE);
                    v2.setVisibility(View.VISIBLE);
                    v3.setVisibility(View.GONE);
                } else {
                    v2.setText(checkouts[mydummy][2]);
                    v3.setText(checkouts[mydummy][3] + " )");
                    v1.setVisibility(View.VISIBLE);
                    v2.setVisibility(View.VISIBLE);
                    v3.setVisibility(View.VISIBLE);
                }
            }
        } else {
            v1.setText("-");
            v2.setText("-");
            v3.setText("-");
            v1.setVisibility(View.INVISIBLE);
            v2.setVisibility(View.INVISIBLE);
            v3.setVisibility(View.INVISIBLE);
        }
    }

    private void eliminationsvorschlagsetzen() {
        //elimination distance errechnen und setzen
        int maxdiff = 0,
                aktuellerscore= Integer.parseInt(score.getText().toString());
        int diff=30000;

        switch (spieleranzahl) {
            case 8:
                if (spieler8.score > aktuellerscore) {
                    maxdiff = spieler8.score - aktuellerscore;
                    diff = maxdiff;
                }
            case 7:
                if (spieler7.score > aktuellerscore)
                {
                    maxdiff = spieler7.score - aktuellerscore;
                    if (maxdiff <  diff) diff = maxdiff;
                }
            case 6:
                if (spieler6.score > aktuellerscore) {
                    maxdiff = spieler6.score - aktuellerscore;
                    if (maxdiff <  diff) diff = maxdiff;
                }
            case 5:
                if (spieler5.score > aktuellerscore) {
                    maxdiff = spieler5.score - aktuellerscore;
                    if (maxdiff <  diff) diff = maxdiff;
                }
            case 4:
                if (spieler4.score > aktuellerscore)
                {
                    maxdiff = spieler4.score - aktuellerscore;
                    if (maxdiff <  diff) diff = maxdiff;
                }
            case 3:
                if (spieler3.score > aktuellerscore)
                {
                    maxdiff = spieler3.score - aktuellerscore;
                    if (maxdiff <  diff) diff = maxdiff;
                }
            case 2:
                if (spieler2.score > aktuellerscore)
                {
                    maxdiff = spieler2.score - aktuellerscore;
                    if (maxdiff <  diff) diff = maxdiff;
                }
            case 1:
                if (spieler1.score > aktuellerscore)
                {
                    maxdiff = spieler1.score - aktuellerscore;
                    if (maxdiff <  diff) diff = maxdiff;
                }
        }
        if ((diff > 0) && (diff != 30000)) {
            scoretoeliminate.setText(Integer.toString(diff));
            scoretoeliminate.setVisibility(View.VISIBLE);
            toeliminate.setVisibility(View.VISIBLE);
        } else {
            scoretoeliminate.setVisibility(View.GONE);
            toeliminate.setVisibility(View.GONE);
        }
    }

    private void vorschlagsetzenliste(int mydummy, TextView vorschlag) {
        if (mydummy > 0 && mydummy <= 170 && mydummy != 169 && mydummy != 168 && mydummy != 166 && mydummy != 165 && mydummy != 163 && mydummy != 162 && mydummy != 159) {
            if (checkouts[mydummy][2].isEmpty()) {
                vorschlag.setText("( " + checkouts[mydummy][1] + " )");
            } else {
                if (checkouts[mydummy][3].isEmpty()) {
                    vorschlag.setText("( " + checkouts[mydummy][1] + " " + checkouts[mydummy][2] + " )");

                } else {
                    vorschlag.setText("( " + checkouts[mydummy][1] + " " + checkouts[mydummy][2] + " " + checkouts[mydummy][3] + " )");
                }
            }

            vorschlag.setVisibility(View.VISIBLE);

        } else {
            vorschlag.setText("");
            vorschlag.setVisibility(View.GONE);

        }
    }

    private boolean suddendeatherreicht() {
        if (!suddendeath) return false;
        switch (spieleranzahl) {
            case 8:
                if (!((spieler8.geworfenePfeile>=suddendeathdarts) && (spieler8.geworfenePfeile==spieler7.geworfenePfeile)
                        && (spieler8.geworfenePfeile==spieler6.geworfenePfeile) && (spieler8.geworfenePfeile==spieler5.geworfenePfeile) && (spieler8.geworfenePfeile==spieler4.geworfenePfeile)
                        && (spieler8.geworfenePfeile==spieler3.geworfenePfeile) && (spieler8.geworfenePfeile==spieler2.geworfenePfeile) && (spieler8.geworfenePfeile==spieler1.geworfenePfeile) ))
                    return false;
                break;
            case 7:
                if (!((spieler7.geworfenePfeile>=suddendeathdarts) && (spieler7.geworfenePfeile==spieler6.geworfenePfeile)
                        && (spieler7.geworfenePfeile==spieler5.geworfenePfeile) && (spieler7.geworfenePfeile==spieler4.geworfenePfeile) && (spieler7.geworfenePfeile==spieler3.geworfenePfeile)
                        && (spieler7.geworfenePfeile==spieler2.geworfenePfeile) && (spieler7.geworfenePfeile==spieler1.geworfenePfeile)  ))
                    return false;
                break;
            case 6:
                if (!((spieler6.geworfenePfeile>=suddendeathdarts) && (spieler6.geworfenePfeile==spieler5.geworfenePfeile)
                        && (spieler6.geworfenePfeile==spieler4.geworfenePfeile) && (spieler6.geworfenePfeile==spieler3.geworfenePfeile) && (spieler6.geworfenePfeile==spieler2.geworfenePfeile)
                        && (spieler6.geworfenePfeile==spieler1.geworfenePfeile)  ))
                    return false;
                break;
            case 5:
                if (!((spieler5.geworfenePfeile>=suddendeathdarts) && (spieler5.geworfenePfeile==spieler4.geworfenePfeile)
                        && (spieler5.geworfenePfeile==spieler3.geworfenePfeile) && (spieler5.geworfenePfeile==spieler2.geworfenePfeile) && (spieler5.geworfenePfeile==spieler1.geworfenePfeile) ))
                    return false;
                break;
            case 4:
                if (!((spieler4.geworfenePfeile>=suddendeathdarts) && (spieler4.geworfenePfeile==spieler3.geworfenePfeile)
                        && (spieler4.geworfenePfeile==spieler2.geworfenePfeile) && (spieler4.geworfenePfeile==spieler1.geworfenePfeile)))
                    return false;
                break;
            case 3:
                if (!((spieler3.geworfenePfeile>=suddendeathdarts) && (spieler3.geworfenePfeile==spieler2.geworfenePfeile)
                        && (spieler3.geworfenePfeile==spieler1.geworfenePfeile)))
                    return false;
                break;
            case 2:
                if (!((spieler2.geworfenePfeile>=suddendeathdarts) && (spieler2.geworfenePfeile==spieler1.geworfenePfeile)))
                    return false;
                break;
            case 1:
                if (!(spieler1.geworfenePfeile>=suddendeathdarts))
                    return false;
                break;
        }
        return true;
    }
}




