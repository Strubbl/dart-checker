hinzugefügt:
- Einstellungen: Sprachauswahl
- X01/Cricket: Sicherheitsfrage bei Klick auf "Undo"
- X01: verbessertes layout für größere Bildschirme und Querformat - variable Textgröße der Punkteanzeige

repariert:
- SET/LEG: Überblick am Spielende: falsche Textüberblendungen waren sichtbar
- Hauptmenü: Layout der Spielernamen, speziell für kleine Bildschirme und viele Spieler
- Hilfe: das helle Design wurde nicht angewendet