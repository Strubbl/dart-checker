hinzugefügt:
- Einstellungen: Startzahl des Crazy Cricket Spielbereiches

repariert:
- cricket: App friert am Spielende ein, wenn die Einstellung "das Spiel geht weiter, wenn der erste Spieler gewonnen hat" aktiviert ist
- cricket: falsche Anzeigen, wenn die Sicherheitsfrage am Spielende verneint wird
