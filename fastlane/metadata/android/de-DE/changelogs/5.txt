- Layout Verbesserungen im Cricket Spiel

hinzugefügt:
- neuer Eintrag unter Einstellungen: FIRST TO oder BEST OF
- Set/Leg Statusmeldung nachdem ein Leg beendet wurde

repariert:
- Fehler in Anzeige von "höchster checkout" nach einem Set/Leg Spiel
- Fehler in Cricket: (Re)aktivierung der Zahlenbuttons, obwohl die Zahlen geschlossen waren
