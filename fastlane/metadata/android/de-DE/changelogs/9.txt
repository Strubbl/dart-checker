hinzugefügt:
- Cricket Variante: cut throat
- master out
- Einstellungen: alternativer Eingabemodus für nicht-Cricket-Spiele
- Einstellungen: Sudden Death Modus für nicht-Cricket-Spiele

repariert:
- X01: undo Fehlfunktion bei einem Spieler und keinem Wurf
- Cricket: kein Standby
