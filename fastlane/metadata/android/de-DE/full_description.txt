Dart Checker kümmert sich um die Punkte, während du allein oder mit Freunden Dart spielst.
Es ist die bessere Form einer Punktetafel. Es werden keine händischen Berechnungen mehr benötigt.
Tippe deine Treffer einfach so ein, wie du sie jemandem sagen würdest.

    Features

    * einzelnes X01 (301, 501,...), SET/LEG und FREE Trainingmodus (frei wählbare Punkteanzahl), CRICKET
    * 1-8 Spieler (ausgenommen SET/LEG Modus)
    * master, double und single out
    * checkout-Vorschläge
    * mehrfache Rücknahme in Folge von fehlerfhaften Eingaben
    * Statistiken (über beliebig viele Spiele und Spieler)
    * zwei Eingabevarianten (Dart für Dart oder gesamte Aufnahme)
    * energiesparendes dunkles Design oder ein helles
    * Sprache: Deutsch, Spanisch oder Englisch
    * offline - es wird absolut keine Internetverbindung benötigt oder genutzt, von dem eigentlich App-Download abgesehen ;-)